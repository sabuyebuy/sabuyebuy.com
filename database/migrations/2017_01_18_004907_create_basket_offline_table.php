<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasketOfflineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baskets_offline', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_detail');
            $table->integer('quantity');
            $table->double('price');
            $table->string('note');
            $table->integer('user_id');
            $table->smallInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('baskets_offline');
    }
}
