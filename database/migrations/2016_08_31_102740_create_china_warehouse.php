<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChinaWarehouse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('china_warehouse', function (Blueprint $table) {
            $table->increments('id');
            $table->string('warehouse_name');
            $table->string('receiver_name');
            $table->string('receiver_address');
            $table->string('receiver_province');
            $table->string('receiver_zip_code');
            $table->string('receiver_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('china_warehouse');
    }
}
