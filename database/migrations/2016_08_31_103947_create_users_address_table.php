<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_address', function (Blueprint $table) {
            $table->increments('id');
            $table->string('receiver_name');
            $table->string('receiver_address');
            $table->string('receiver_district');
            $table->string('receiver_province');
            $table->string('receiver_zip_code');
            $table->string('receiver_tel');
            $table->smallInteger('default_address')->default(0);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_address');
    }
}
