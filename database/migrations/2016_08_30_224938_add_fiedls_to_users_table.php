<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiedlsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('user_image',512);
            $table->string('user_name');
            $table->string('user_code');
            $table->float('user_amount_balance');
            $table->smallInteger('gender');
            $table->date('birthday_date')->default('1980-01-01');
            $table->string('owner_shop');
            $table->string('contact_phone1');
            $table->string('contact_phone2');
            $table->string('contact_line');
            $table->string('user_level');
            $table->string('user_group');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('user_image');
            $table->dropColumn('user_name');
            $table->dropColumn('user_code');
            $table->dropColumn('user_amount_balance');
            $table->dropColumn('gender');
            $table->dropColumn('birthday_date');
            $table->dropColumn('owner_shop');
            $table->dropColumn('contact_phone1');
            $table->dropColumn('contact_phone2');
            $table->dropColumn('contact_line');
            $table->dropColumn('user_level');
            $table->dropColumn('user_group');
        });
    }
}
