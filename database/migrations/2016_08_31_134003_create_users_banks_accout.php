<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersBanksAccout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_bank_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bank_id');
            $table->string('bank_code');
            $table->integer('account_number');
            $table->string('account_name');
            $table->string('account_branch');
            $table->string('note');
            $table->smallInteger('default')->default(0);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_bank_accounts');
    }
}
