<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsPostsMultilangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en');
            $table->string('title_th');
            $table->string('title_cn');
            $table->longText('body_en');
            $table->longText('body_th');
            $table->longText('body_cn');
            $table->string('slug_en');
            $table->string('slug_th');
            $table->string('slug_cn');
            $table->string('post_type');
            $table->string('post_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cms_posts');
    }
}
