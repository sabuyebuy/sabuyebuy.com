<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoteToUsersWithdrawal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_withdrawal', function (Blueprint $table) {
            $table->text('note')->after('staff_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_withdrawal', function (Blueprint $table) {
            $table->dropColumn('note');
        });
    }
}
