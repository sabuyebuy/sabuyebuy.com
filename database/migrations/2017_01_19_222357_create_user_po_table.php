<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_po', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('po_lock');
            $table->string('po_id');
            $table->string('po_type');
            $table->string('po_status');
            $table->string('po_step');
            $table->double('exchange_rate');
            $table->integer('operator_id');
            $table->string('shipping_cn_to_th_by');
            $table->string('shipping_th_to_user_by');
            $table->smallInteger('wooden_box');
    
            $table->double('payment_add_ship_in_cn');
            $table->double('payment_add_ship_in_th');
            $table->double('payment_add_buy_fee');
            $table->double('payment_add_buy_for_other');
            
            
            $table->double('discount');
    
            $table->string('shipping_cn_tracking');
            $table->string('shipping_th_tracking');
    
            $table->text('note');
    
            $table->integer('user_id');
            $table->integer('cart_id');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_po');
    }
}
