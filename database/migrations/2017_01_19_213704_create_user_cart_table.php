<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_cart', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_title');
            $table->string('product_url');
            $table->string('product_image_url');
            $table->string('product_shop');
            $table->integer('quantity');
            $table->double('price');
            $table->string('note');
            $table->integer('user_id');
            $table->integer('po_id');
            $table->smallInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_cart');
    }
}
