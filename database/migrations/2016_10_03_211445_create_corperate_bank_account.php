<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorperateBankAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporate_bank_account', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bank_code');
            $table->string('bank_name');
            $table->string('bank_account');
            $table->string('bank_account_type');
            $table->string('bank_branch');
            $table->string('bank_note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('corporate_bank_account');
    }
}
