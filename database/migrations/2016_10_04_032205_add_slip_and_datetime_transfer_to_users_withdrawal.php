<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlipAndDatetimeTransferToUsersWithdrawal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_withdrawal', function (Blueprint $table) {
            $table->string('slip')->after('status');
            $table->dateTime('transfer_date')->after('slip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_withdrawal', function (Blueprint $table) {
            $table->dropColumn('slip');
            $table->dropColumn('transfer_date');
        });
    }
}
