<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersDeposit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_deposit', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bank');
            $table->float('amount');
            $table->string('slip');
            $table->dateTime('date_time');
            $table->smallInteger('user_id');
            $table->string('status');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_deposit');
    }
}
