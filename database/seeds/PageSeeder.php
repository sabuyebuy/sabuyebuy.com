<?php
    
    use App\CMSPosts;
    use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * About
         */
        CMSPosts::create([
            'title_en' => 'Who we are ?',
            'title_th' => 'เราคือใคร ?',
            'title_cn' => '我們是誰',
            'body_en'  => '',
            'body_th'  => '',
            'body_cn'  => '',
            'slug_en'  => '',
            'slug_th'  => '',
            'slug_cn'  => '',
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
        
        CMSPosts::create([
            'title_en' => 'Vision',
            'title_th' => 'วิสัยทัศน์',
            'title_cn' => '視力',
            'body_en'  => '',
            'body_th'  => '',
            'body_cn'  => '',
            'slug_en'  => '',
            'slug_th'  => '',
            'slug_cn'  => '',
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
        
        CMSPosts::create([
            'title_en' => 'Mission',
            'title_th' => 'เป้าหมาย',
            'title_cn' => '任務',
            'body_en'  => '',
            'body_th'  => '',
            'body_cn'  => '',
            'slug_en'  => '',
            'slug_th'  => '',
            'slug_cn'  => '',
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
        
        CMSPosts::create([
            'title_en' => 'Core competency',
            'title_th' => 'ความสามารถหลัก',
            'title_cn' => '核心競爭力',
            'body_en'  => '',
            'body_th'  => '',
            'body_cn'  => '',
            'slug_en'  => '',
            'slug_th'  => '',
            'slug_cn'  => '',
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
        
        CMSPosts::create([
            'title_en' => 'Our services',
            'title_th' => 'บริการของเรา',
            'title_cn' => '我們的服務',
            'body_en'  => '',
            'body_th'  => '',
            'body_cn'  => '',
            'slug_en'  => '',
            'slug_th'  => '',
            'slug_cn'  => '',
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
    
        /**
         * How to order
         */
        CMSPosts::create([
            'title_en' => 'How to order',
            'title_th' => 'วิธีการสั่งซื้อ',
            'title_cn' => '如何訂購',
            'body_en'  => '',
            'body_th'  => '',
            'body_cn'  => '',
            'slug_en'  => '',
            'slug_th'  => '',
            'slug_cn'  => '',
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
    
        /**
         * Imports
         */
        CMSPosts::create([
            'title_en' => 'By Truck',
            'title_th' => 'ทางรถ',
            'title_cn' => '卡車',
            'body_en'  => '',
            'body_th'  => '',
            'body_cn'  => '',
            'slug_en'  => '',
            'slug_th'  => '',
            'slug_cn'  => '',
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
        
        CMSPosts::create([
            'title_en' => 'By ship',
            'title_th' => 'ทางเรือ',
            'title_cn' => '坐船',
            'body_en'  => '',
            'body_th'  => '',
            'body_cn'  => '',
            'slug_en'  => '',
            'slug_th'  => '',
            'slug_cn'  => '',
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
        
        
        CMSPosts::create([
            'title_en' => 'By Air flight',
            'title_th' => 'เราคือใคร',
            'title_cn' => '我們是誰',
            'body_en'  => '',
            'body_th'  => '',
            'body_cn'  => '',
            'slug_en'  => '',
            'slug_th'  => '',
            'slug_cn'  => '',
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
        
        CMSPosts::create([
            'title_en' => 'Domestic fee',
            'title_th' => 'ค่าธรรมเนียมภายในประเทศ',
            'title_cn' => '國產費',
            'body_en'  => '',
            'body_th'  => '',
            'body_cn'  => '',
            'slug_en'  => '',
            'slug_th'  => '',
            'slug_cn'  => '',
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
    
    
        /**
         * Export
         */
        
        CMSPosts::create([
            'title_en' => 'Information',
            'title_th' => 'รายละเอียด',
            'title_cn' => '信息',
            'body_en'  => '',
            'body_th'  => '',
            'body_cn'  => '',
            'slug_en'  => '',
            'slug_th'  => '',
            'slug_cn'  => '',
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
        
        CMSPosts::create([
            'title_en' => 'Transportation fee',
            'title_th' => 'ค่าธรรมเนียมขนส่ง',
            'title_cn' => '交通費',
            'body_en'  => '',
            'body_th'  => '',
            'body_cn'  => '',
            'slug_en'  => '',
            'slug_th'  => '',
            'slug_cn'  => '',
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
        
        CMSPosts::create([
            'title_en' => 'Payment',
            'title_th' => 'การชำระเงิน',
            'title_cn' => '付款',
            'body_en'  => '',
            'body_th'  => '',
            'body_cn'  => '',
            'slug_en'  => '',
            'slug_th'  => '',
            'slug_cn'  => '',
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
    
        /**
         * Contact
         */
        CMSPosts::create([
            'title_en' => 'Contact',
            'title_th' => 'ติดต่อเรา',
            'title_cn' => '聯繫',
            'body_en'  => '',
            'body_th'  => '',
            'body_cn'  => '',
            'slug_en'  => '',
            'slug_th'  => '',
            'slug_cn'  => '',
            'post_type' => 'page',
            'post_status' => 'publish'
        ]);
    }
}
