<?php
    
    use App\ProductsCategory;
    use Illuminate\Database\Seeder;

class ProductCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * About
         */
        ProductsCategory::create([
            'category_name' => 'แม่และเด็ก',
            'slug' => 'แม่และเด็ก',
        ]);
    }
}
