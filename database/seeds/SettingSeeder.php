<?php
    
    use App\Setting;
    use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'lang' => 'en',
            'name' => 'English',
            'active' => 1
        ]);
        
        Setting::create([
            'lang' => 'th',
            'name' => 'ไทย',
            'active' => 0
        ]);
        
        Setting::create([
            'lang' => 'cn',
            'name' => '中国',
            'active' => 0
        ]);
    }
}
