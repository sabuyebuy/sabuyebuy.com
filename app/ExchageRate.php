<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchageRate extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'exchange_rate';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
}
