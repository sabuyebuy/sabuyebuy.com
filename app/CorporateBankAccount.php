<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorporateBankAccount extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'corporate_bank_account';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
}
