<?php
    
    namespace App;
    
    use Illuminate\Database\Eloquent\Model;
    
    class UsersFaveritesShop extends Model
    {
        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'users_favorites_shop';
        
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $guarded = ['id'];
    }
