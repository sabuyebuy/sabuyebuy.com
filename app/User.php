<?php
    
    namespace App;
    
    use App;
    use Carbon\Carbon;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Illuminate\Support\Facades\Auth;
    
    class User extends Authenticatable
    {

        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'users';
    
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $guarded = ['id'];
    
        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden = [
            'password', 'remember_token',
        ];
    
        /**
         * Current User logged in
         * @return mixed
         */
        public function currentUser()
        {
            if(Auth::check()){
                return User::find(Auth::user()->id);
            }else{
                return redirect('/login');
            }
        
        }
    
        public function userIDMaker()
        {
            $date = Carbon::now();
            $user_code = "SB".substr($date->year,2).$date->month. str_pad(count(User::all())+1, 5, '0', STR_PAD_LEFT);
            return $user_code;
        }
    
        public function userRole(){
            if(Auth::check()){
                return User::find(Auth::user()->role);
            }else{
                return redirect('/login');
            }
        }

    }
