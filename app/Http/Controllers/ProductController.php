<?php

namespace App\Http\Controllers;

use App\Products;
use App\ProductsCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    
    /**
     * List all products
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $products = Products::all();
    
        return view('ui-backend/admin/products/index', compact('products'));
    }
    
    public function create()
    {
        $category = ProductsCategory::all();
        return view('ui-backend/admin/products/create', compact('category'));
    }
    
    /**
     * Edit
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $product = Products::find($id);
        $category = ProductsCategory::all();
        return view('ui-backend/admin/products/edit', compact('product','category'));
    }
    
    public function store(Request $request)
    {
    //return $request->all();
        Products::create([
            'title' => $request->get('title'),
            'image' => $request->get('image'),
            'category' => $request->get('category'),
            'full_price' => $request->get('full_price'),
            'price' => $request->get('price'),
            'external_link' => $request->get('external_link'),
            'shop' => $request->get('shop'),
        ]);
    
        Session::flash('status', "Created");
        return redirect()->back();
    }
    
    public function update($id,Request $request)
    {
        $product = Products::find($id);
        $product->title = $request->get('title');
        $product->image = $request->get('image');
        $product->category = $request->get('category');
        $product->full_price = $request->get('full_price');
        $product->price = $request->get('price');
        $product->external_link = $request->get('external_link');
        $product->shop = $request->get('shop');
        
        $product->save();
    
        Session::flash('status', "Updated");
        return redirect()->back();
    }
    
    public function destroy($id)
    {
        $product = Products::find($id);
        $product->delete();
        
        Session::flash('status', "Deleted");
        return redirect()->back();
    }
}
