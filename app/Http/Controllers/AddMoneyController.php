<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\UserDeposit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
class AddMoneyController extends HomeController
{
	private $_api_context;
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		/** setup PayPal api context **/
		$paypal_conf = \Config::get('paypal');
		$this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
		$this->_api_context->setConfig($paypal_conf['settings']);
	}
	/**
	 * Show the application paywith paypalpage.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function payWithPaypal()
	{
		return \redirect('/users/wallet/deposit');
	}
	/**
	 * Store a details of payment with paypal.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postPaymentWithpaypal(Request $request)
	{
		$payer = new Payer();
		$payer->setPaymentMethod('paypal');
		$item_1 = new Item();
		$item_1->setName('Refill account for '.Auth::user()->name) /** item name **/
		->setCurrency('THB')
			->setQuantity(1)
			->setPrice($request->get('amount')); /** unit price **/
		$item_list = new ItemList();
		$item_list->setItems(array($item_1));
		$amount = new Amount();
		$amount->setCurrency('THB')
			->setTotal($request->get('amount'));
		$transaction = new Transaction();
		$transaction->setAmount($amount)
			->setItemList($item_list)
			->setDescription('SabuyEbuy refill balance');
		$redirect_urls = new RedirectUrls();
		$redirect_urls->setReturnUrl(URL::route('payment.status')) /** Specify return URL **/
		->setCancelUrl(URL::route('payment.status'));
		$payment = new Payment();
		$payment->setIntent('Sale')
			->setPayer($payer)
			->setRedirectUrls($redirect_urls)
			->setTransactions(array($transaction));
		/** dd($payment->create($this->_api_context));exit; **/
		try {
			$payment->create($this->_api_context);
		} catch (\PayPal\Exception\PPConnectionException $ex) {
			if (\Config::get('app.debug')) {
				\Session::put('error','Connection timeout');
				return Redirect::route('addmoney.paywithpaypal');
				/** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
				/** $err_data = json_decode($ex->getData(), true); **/
				/** exit; **/
			} else {
				\Session::put('error','Some error occur, sorry for inconvenient');
				return Redirect::route('addmoney.paywithpaypal');
				/** die('Some error occur, sorry for inconvenient'); **/
			}
		}
		foreach($payment->getLinks() as $link) {
			if($link->getRel() == 'approval_url') {
				$redirect_url = $link->getHref();
				break;
			}
		}
		/** add payment ID to session **/
		Session::put('paypal_payment_id', $payment->getId());
		if(isset($redirect_url)) {
			/** redirect to paypal **/
			return Redirect::away($redirect_url);
		}
		\Session::put('error','Unknown error occurred');
		return Redirect::route('addmoney.paywithpaypal');
	}
	public function getPaymentStatus(Request $request)
	{
		/** Get the payment ID before session clear **/
		$payment_id = Session::get('paypal_payment_id');
		/** clear the session payment ID **/
		Session::forget('paypal_payment_id');
		if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
			\Session::put('error','Payment failed');
			return Redirect::route('addmoney.paywithpaypal');
		}
		$payment = Payment::get($payment_id, $this->_api_context);
		/** PaymentExecution object includes information necessary **/
		/** to execute a PayPal account payment. **/
		/** The payer_id is added to the request query parameters **/
		/** when the user is redirected from paypal back to your site **/
		$execution = new PaymentExecution();
		$execution->setPayerId(Input::get('PayerID'));
		/**Execute the payment **/
		$result = $payment->execute($execution, $this->_api_context);
		/** dd($result);exit; /** DEBUG RESULT, remove it later **/
		//print_r($result);
		if ($result->getState() == 'approved') {

			//return $result->getTransactions();

			/** it's all right **/
			/** Here Write your database logic like that insert record or value in database if you want **/

			UserDeposit::create([
				'user_id' => Auth::user()->id,
				'bank' => 'PayPal',
				'amount' => $result->transactions[0]->amount->total,
				'slip' => '',
				'date_time' => Carbon::now(),
				'status' => 'ตรวจสอบแล้ว',
				'note' => '',
			]);

			\Session::put('status','Payment success');
			return Redirect::route('addmoney.paywithpaypal');
		}
		\Session::put('error','Payment failed');
		return Redirect::route('addmoney.paywithpaypal');
	}
}
