<?php

namespace App\Http\Controllers;

use App\ExchageRate;
use App\Languages;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AdministratorController extends Controller
{
    
    
    /**
     * UsersFavoriteShop constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->middleware('auth');
        $this->user =  $user->currentUser();
        
        $lang = Session::get ('lang');
        if ($lang != null) {
            \App::setLocale($lang);
        }else{
            Session::put('lang', 'th');
            \App::setLocale('th');
        }
    }
    
    /**
     * Administrator Home
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        $user = $this->user;
        return view('ui-backend/admin/home',compact('user'));
    }
    
    /**
     * Languages view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function languagesView()
    {
        $user = $this->user;
        $langData =  DB::select("SELECT * FROM `languages`");
        return view('ui-backend/admin/languages',compact('user','langData'));
    }
    
    /**
     * Update languages
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function languagesUpdate(Request $request)
    {
        $all = $request->except(['_token']);
    
        $id = 1;
        foreach ($all as $value){
            //echo  $value[0]." => " .$value[1]."<br/>";
            $langID  = Languages::find($id);
            $langID->value_th = $value[0];
            $langID->value_en = $value[1];
        
            $langID->save();
        
            $id++;
        }
        Session::flash('status', "Updated");
    
        return redirect()->back();
    }
    
    
    /**
     * Exchange rate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function exchangeRate(){
    
        $exchangeRate = ExchageRate::find(1);
        return view('ui-backend/admin/exchange-rate',compact('exchangeRate'));
        
    }
    
    /**
     * Exchange rate update
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exchangeRateUpdate(Request $request){
    
        $exchangeRate = ExchageRate::find(1);
        $exchangeRate->thai_baht = (double)$request->get('thai_baht');
        $exchangeRate->start_date = $request->get('start_date');
        $exchangeRate->save();
    
        Session::flash('status', "Updated");
        return redirect()->back();
        
    }
}
