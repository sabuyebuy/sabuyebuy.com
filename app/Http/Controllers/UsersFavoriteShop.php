<?php
    
    namespace App\Http\Controllers;
    
    use App\User;
    use App\UsersFaveritesShop;
    use Illuminate\Http\Request;
    
    use App\Http\Requests;
    use Illuminate\Support\Facades\Session;
    
    class UsersFavoriteShop extends Controller
    {
    
        /**
         * UsersFavoriteShop constructor.
         * @param User $user
         */
        public function __construct(User $user)
        {
            $this->middleware('auth');
            $this->user =  $user->currentUser();
    
            $lang = Session::get ('lang');
            if ($lang != null) {
                \App::setLocale($lang);
            }else{
                Session::put('lang', 'th');
                \App::setLocale('th');
            }
        }
    
        public function index()
        {
    
            
            $user = $this->user;
                    
            $favorites = UsersFaveritesShop::where('user_id','=',$user->id)->get();
            
            return view('ui-backend/users/favorites',compact('favorites','user'));
        }
        
        
        /**
         * Store favorites shop
         * @param Request $request
         * @param User $user
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
         */
        public function store(Request $request,User $user)
        {
            $this->validate($request,[
                'shop_name' => 'required',
                'shop_url' => 'required|url',
            ]);
            
            UsersFaveritesShop::create([
                'shop_name' => $request->get('shop_name'),
                'shop_url' => $request->get('shop_url'),
                'note' => $request->get('note'),
                'user_id' => $user->currentUser()->id,
            ]);
            Session::flash('status', "ข้อมูลถูกเพิ่มแล้ว");
            return redirect('/users/favorites');
        }
        
        public function destroy($id){
            $fav =  UsersFaveritesShop::find($id);
            
            $fav->delete();
            Session::flash('status', "ข้อมูลถูกลบแล้ว");
            return redirect('/users/profile');
            
        }
    }
