<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class APIsController extends APIBaseController
{
    /**
     * Update lang
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function langSetting(Request $request){
        
        
        DB::table('setting')->update(['active' => 0]);
        
        Setting::where('lang','=',$request->get('lang'))
            ->update(['active' => 1]);
        
        return $this->setStatusCode(200)->respond("Language change success full.");
    }
}
