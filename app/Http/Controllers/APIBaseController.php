<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;

use App\Http\Requests;

class APIBaseController extends Controller
{
    /**
     * status code
     * @var int
     */
    protected $statusCode = 200;
    
    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
    
    /**
     * @param mixed $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }
    
    /**
     * @param string $message
     * @return mixed
     */
    public function responseNotFound($message = 'Not Found !',$header = [])
    {
        $header = [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Headers' => 'Origin, Content-Type'
        ];
        
        return $this->setStatusCode(404)->respondWithError($message, $header);
        
    }
    
    /**
     * @param string $message
     * @return mixed
     */
    public function responseInternalError($message = 'Internal Error')
    {
        return $this->setStatusCode(500)->respondWithError($message);
        
    }
    
    /**
     * @param $message
     * @return mixed
     */
    public function respondWithError($message)
    {
        return $this->respond([
            
            'error'       => $message,
            'status_code' => $this->getStatusCode(),
        
        ]);
    }
    
    /**
     * @param $data
     * @param array $header
     * @return mixed
     */
    public function respond($data, $header = [])
    {
        $header = [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Headers' => 'Origin, Content-Type'
        ];
        
        return [$data, $this->getStatusCode(), $header];
    }
}
