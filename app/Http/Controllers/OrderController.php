<?php
    
    namespace App\Http\Controllers;
    
    use App\ExchageRate;
    use App\Products;
    use App\User;
    use App\UsersCart;
    use App\UsersPO;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    
    use App\Http\Requests;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Session;
    
    class OrderController extends Controller
    {
        /**
         * PagesContentController constructor.
         * @param User $user
         */
        public function __construct(User $user)
        {
            $this->middleware('auth');
            
            $lang = Session::get ('lang');
            if ($lang != null) {
                \App::setLocale($lang);
            }else{
                Session::put('lang', 'th');
                \App::setLocale('th');
            }
            
            $this->lang = $lang;
            $this->user = $user->currentUser();
            
        }
        
        /**
         * List product in cart
         * @return mixed
         */
        public function cartList()
        {
            $products = UsersCart::where('user_id','=',$this->user->id)
                ->where('status','=',0)
                ->get();
            $user = $this->user->id;
            $exchangeRage = ExchageRate::find(1)->thai_baht;
            $total = 0;
            foreach ($products as $product){
                $total += $product->price * $product->quantity;
            }
            
            $thaiTotal = $total*$exchangeRage;
            
            return view('ui-frontend/orders/cart',compact('products','user','total','thaiTotal'));
        }
        
        public function removeFromCart($id)
        {
            $userCart = UsersCart::find($id);
            
            $userCart->delete();
            
            Session::flash('status', "ลบสินค้าจากตระกร้าแล้ว");
            return redirect('/cart');
        }
        
        /**
         * Create PO
         * @param Request $request
         * @return array
         */
        public function createPO(Request $request)
        {
            //return $request->all();
            $user = $this->user->id;
            
            $ids = join(",",$request->get('product_id'));
            
            //print_r($ids);
            $products = DB::select("SELECT * FROM `users_cart` WHERE `id` IN ($ids)");
            
            $exchangeRage = ExchageRate::find(1)->thai_baht;
    
            $total = 0;
            foreach ($products as $product){
                $total += $product->price * $product->quantity;
            }
    
            $thaiTotal = $total*$exchangeRage;
            
            return view('ui-frontend/orders/create-po',compact('products','user','total','thaiTotal'));
        }
        
        public function createPOConfirm(Request $request)
        {
    
            $agree = $request->get('assent_buy');
            
            if(isset($agree)) {
    
                $user = $this->user;
                $data = Carbon::now()->format('dmyHis');
    
                $poID = $user->user_code . "_" . $data;
    
                $exchangeRage = ExchageRate::find(1)->thai_baht;
    
    
                // Update cart
                DB::table('users_cart')->whereIn('id', $request->get('products_id'))->update(array(
                    'status' => 1,
                    'po_id' => $poID
                ));
    
    
                UsersPO::create([
                    'po_status' => 'Open',
                    'po_step' => 1,
                    'exchange_rate' => $exchangeRage,
                    'shipping_cn_to_th_by' => $request->get('shipping_cn_to_th_by'),
                    'shipping_th_to_user_by' => $request->get('shipping_th_to_user_by'),
                    'wooden_box' => $request->get('wooden_box'),
                    'po_id' => $poID,
                    'user_id' => $user->id,
                ]);
    
                Session::flash('status', "การสั่งซื้อเสร็จสิ้น");
    
                return redirect('/');
            }else{
                Session::flash('error', "กรุณาคลิกยอมรับเงื่อนไขการให้บริการ");
                return redirect('/cart');
            }
        }
        
        /**
         * Add product to cart
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse
         */
        public function addToCart(Request $request)
        {
            //return $request->all();
            
            UsersCart::create($request->except('_token'));
            
            Session::flash('status', "เพิ่มสินค้าลงตระกร้าแล้ว");
            return redirect('/cart');
        }
        
        public function productView($id)
        {
            $product = Products::find($id);
            $user = $this->user->id;
            
            return view('ui-frontend/products/product-detail',compact('product','user'));
        }
    }
