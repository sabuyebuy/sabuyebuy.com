<?php
    
    namespace App\Http\Controllers;
    
    use App\User;
    use App\UserDeposit;
    use App\UsersAdress;
    use App\UsersBankAccounts;
    use App\UsersCart;
    use App\UsersFaveritesShop;
    use App\UsersPO;
    use App\UserWithdrawal;
    use Illuminate\Http\Request;
    
    use App\Http\Requests;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Session;
    
    class UsersController extends Controller
    {
        /**
         * UsersController constructor.
         * @param User $user
         */
        public function __construct(User $user)
        {
            $this->middleware('auth',['except' => ['langSwitcher']]);
            
            $this->user =  $user->currentUser();
    
            $lang = Session::get ('lang');
            if ($lang != null) {
                \App::setLocale($lang);
            }else{
                Session::put('lang', 'th');
                \App::setLocale('th');
            }
            
        }
    
        /**
         * User dashboard
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function dashboard()
        {
            $user =  $this->user;
            $orders = UsersPO::where('user_id','=',$this->user->id)
                ->orderby('id')
                ->get();
    
            return view('ui-backend/users/po', compact('orders','user'));
        }
    
    
        public function showPO($po_id)
        {
            $user =  $this->user;
            $po = UsersPO::where('po_id','=',$po_id)->first();
            $products = UsersCart::where('po_id','=',$po_id)->get();
            
            $total = 0;
            foreach ($products as $product){
                $total += $product->price * $product->quantity;
            }
            //$total = $total - $product->discount;
            $thaiTotal = $total*$po->exchange_rate;
            
            $orderFee = ($total*10)/100;
            
            $dataSum = [
                'productTotal' => count($products),
                'cn_ship' => $po->payment_add_ship_in_cn,
                'order_fee' => $orderFee,
                'totalCost' => $orderFee+$total+$po->payment_add_ship_in_cn*$po->exchange_rate,
                'totalCostWithRate' => ($orderFee+$total+$po->payment_add_ship_in_cn)*$po->exchange_rate,
            ];
    
            return view('ui-backend/users/po-show', compact('po','user','products','total','thaiTotal','dataSum'));
        }
    
        /**
         * List all user
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function listAll()
        {
            $users = User::all();
    
            return view('ui-backend/admin/users/list-all',compact('users'));
        }
    
        public function userEdit($id)
        {
            $user = User::find($id);
    
            return view('ui-backend/admin/users/user-edit',compact('user'));
        }
        
        public function userUpdate($id,Request $request)
        {
            
            //return $request->all();
            $user = User::find($id);
            $user->user_code = $request->get('user_code');
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->user_amount_balance = $request->get('user_amount_balance');
            $user->role = $request->get('role');

            if($request->get('password') != ''){
                $user->password = bcrypt($request->get('password'));
            }else{
                $user->password = $user->password;
            }
            
    
            $user->save();
    
            Session::flash('status', "Updated");
            return redirect()->back();
        }
    
        /**
         * Delete user
         * @param $id
         * @return \Illuminate\Http\RedirectResponse
         */
        public function userDelete($id)
        {
            $users = User::find($id);
            $users->delete();
            
            Session::flash('status', "Deleted");
            return redirect()->back();
        }
    
    
        /**
         * User profile dashboard
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function profile()
        {
            $user = $this->user;
            
            if($user->role == 'user') {
                $address = UsersAdress::where('user_id', '=', $user->id)->get();
                $banks = UsersBankAccounts::where('user_id', '=', $user->id)->get();
                
                //$favorites = UsersFaveritesShop::where('user_id','=',$user->id)->get();
                
                return view('ui-backend/users/profile', compact('user', 'address', 'banks', 'favorites'));
            }
            
            if($user->role == 'accounting') {
                
                return redirect('/admin/accounting');

//                $deposit = UserDeposit::where('status','=','รอการตรวจสอบ')->get();
//                $withdrawal = UserWithdrawal::where('status','=','รอการตรวจสอบ')->get();
//                return view('ui-backend/accounting/index', compact('user','deposit','withdrawal'));
            }
        }
    
    
        /**
         * User update profile
         * @param $id
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
         */
        public function updateProfile($id,Request $request)
        {
            //return $request->all();
            
            $user = User::find($id);
            
            if ($request->file('user_image') != '') {
                
                $avatar      = $request->file('user_image');
                $filename = $avatar->getClientOriginalName();
                
                $avatar->move('uploads/images/', $filename);
                
                $user->user_image = $filename;
            }
            
            $user->name = $request->get('name');
            $user->gender = $request->get('gender');
            $user->birthday_date = $request->get('birthday_date');
            $user->owner_shop = $request->get('owner_shop');
            $user->contact_phone1 = $request->get('contact_phone1');
            $user->contact_phone2 = $request->get('contact_phone2');
            $user->contact_line = $request->get('contact_line');
            
            
            $user->save();
            
            Session::flash('status', "ข้อมูลถูกบันทึกแล้ว");
            return redirect('/users/profile');
        }
    
    
        /**
         * Lang switcher
         * @param $lang
         * @return \Illuminate\Http\RedirectResponse
         */
        public function langSwitcher($lang)
        {
            Session::put('lang', $lang);
            return redirect()->back();
        }
    }
