<?php
    
    namespace App\Http\Controllers;
    
    use App\ExchageRate;
    use App\Http\Requests;
    use App\Products;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Session;
    
    class HomeController extends Controller
    {
        /**
         * Create a new controller instance.
         *
         */
        public function __construct()
        {
            $lang = Session::get ('lang');
            if ($lang != null) {
                \App::setLocale($lang);
            }else{
                Session::put('lang', 'th');
                \App::setLocale('th');
            }
        }
    
        /**
         * Show the application dashboard.
         * @return \Illuminate\Http\Response
         * @internal param Request $request
         */
        public function index()
        {
            $products = Products::paginate(12);
            $exchangeRate = ExchageRate::find(1);
           
            $data = [
                'rate' => $exchangeRate->thai_baht,
                'start_date' => Carbon::parse($exchangeRate->start_date)->format('d M Y')
            ];
            return view('ui-frontend/home',compact('products','data'));
        }
    }
