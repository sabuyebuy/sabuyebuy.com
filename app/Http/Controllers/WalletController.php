<?php
    
    namespace App\Http\Controllers;
    
    use App\CorporateBankAccount;
    use App\User;
    use App\UserDeposit;
    use App\UsersBankAccounts;
    use App\UserWithdrawal;
    use Carbon\Carbon;
    use Illuminate\Contracts\Validation\Validator;
    use Illuminate\Http\Request;
    
    use App\Http\Requests;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Session;
    
    class WalletController extends Controller
    {
        /**
         * WalletController constructor.
         * @param User $user
         */
        public function __construct(User $user)
        {
            $this->middleware('auth');
            $this->user =  $user->currentUser();
    
            $lang = Session::get ('lang');
            if ($lang != null) {
                \App::setLocale($lang);
            }else{
                Session::put('lang', 'th');
                \App::setLocale('th');
            }
        }
        
        /**
         * index view
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function depositIndex()
        {
            $user = $this->user;
            $cop_banks = CorporateBankAccount::all();
            $deposit = UserDeposit::where('user_id','=',$user->id)->get();
            
            $totalDeposit = 0;
            foreach ($deposit as $dep){
                $totalDeposit += $dep->amount;
            }
            
            $totalDeposit;
            
            return view('ui-backend/wallet/deposit',compact('user','cop_banks','deposit','totalDeposit'));
        }
        
        /**
         * Show
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function depositShow($id)
        {
            $user = $this->user;
            $deposit = UserDeposit::find($id);
            return view('ui-backend/wallet/deposit-show',compact('user','deposit'));
        }
        
        /**
         * Save
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse
         */
        public function depositStore(Request $request)
        {
            //return $request->all();
            $this->validate($request, [
                'bank' => 'required',
                'amount' => 'required|numeric',
                'slip' => 'required',
                'date_time' => 'required',
            ]);
            
//            $userAccountBalance =Auth::user()->user_amount_balance;
//            if($request->get('amount') < $userAccountBalance) {
                
                $path_image = "";
                if ($request->file('slip')) {
                    $image = $request->file('slip');
                    $file = $image->getClientOriginalName();
                    $path_image = $image->move('uploads/deposit/', $file);
                }
                
                UserDeposit::create([
                    'user_id' => Auth::user()->id,
                    'bank' => $request->get('bank'),
                    'amount' => $request->get('amount'),
                    'slip' => $path_image,
                    'date_time' => $request->get('date_time'),
                    'status' => 'รอการตรวจสอบ',
                    'note' => $request->get('note'),
                ]);
                
                Session::flash('status', "แจ้งโอนสำเร็จ");
//            }else{
//
//            }
            
            return redirect()->back();
            
        }
        
        /**
         * Update
         * @param $id
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse
         */
        public function depositUpdate($id,Request $request)
        {
            $deposit = UserDeposit::find($id);
            $deposit->bank = $request->get('bank');
            $deposit->amount = $request->get('amount');
            $deposit->slip = $request->get('slip');
            $deposit->date_time = $request->get('date_time');
            $deposit->slip = $request->get('slip');
            $deposit->note = $request->get('note');
            
            $deposit->save();
            
            Session::flash('status', "ลบรายการเรียบร้อย");
            
            return redirect()->back();
        }
        
        /**
         * Delete
         * @param $id
         * @return \Illuminate\Http\RedirectResponse
         */
        public function depositDestroy($id)
        {
            $deposit = UserDeposit::find($id);
            $deposit->delete();
            
            Session::flash('status', "ลบรายการเรียบร้อย");
            
            return redirect()->back();
        }
        
        
        /**
         * index view
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function withdrawalIndex()
        {
            $user = $this->user;
            $banks = UsersBankAccounts::where('user_id','=',$user->id)->get();
            $withdrawal = UserWithdrawal::where('user_id','=',$user->id)->get();
    
            $totalWithdrawal = 0;
            
            foreach ($withdrawal as $withdraw){
                $totalWithdrawal += $withdraw->amount;
            }
            
            //return $totalWithdrawal;
            
            return view('ui-backend/wallet/withdrawal',compact('user','banks','withdrawal','totalWithdrawal'));
        }
        
        public function withdrawalShow($id)
        {
            $user = $this->user;
            $withdrawal = UserWithdrawal::find($id);
            return view('ui-backend/wallet/withdrawal-show',compact('user','withdrawal'));
        }
        
        /**
         * Save
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse
         */
        public function withdrawalStore(Request $request)
        {
            $user = Auth::user();
            
            $this->validate($request, [
                'bank' => 'required',
                'amount' => 'required',
            ]);
            
            $userAccountBalance = $user->user_amount_balance;
            
            if($request->get('amount') < $userAccountBalance){
                UserWithdrawal::create([
                    'user_id'      => $user->id,
                    'bank'         => $request->get('bank'),
                    'amount'       => $request->get('amount'),
                    'date_time'    => Carbon::now(),
                    'status'       => 'รอการตรวจสอบ',
                ]);
    
                Session::flash('status', "แจ้งโอนสำเร็จ");
            }else{
                Session::flash('error', "ยอดเงินไม่เพียงพอ");
            }
            
          
            return redirect()->back();
            
        }
        
        /**
         * Update
         * @param $id
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse
         */
        public function withdrawalUpdate($id,Request $request)
        {
            $deposit = UserWithdrawal::find($id);
            $deposit->bank = $request->get('bank');
            $deposit->amount = $request->get('amount');
            $deposit->save();
            
            Session::flash('status', "ลบรายการเรียบร้อย");
            return redirect()->back();
        }
        
        /**
         * Delete
         * @param $id
         * @return \Illuminate\Http\RedirectResponse
         */
        public function withdrawalDestroy($id)
        {
            $deposit = UserWithdrawal::find($id);
            $deposit->delete();
            
            Session::flash('status', "ลบรายการเรียบร้อย");
            return redirect()->back();
        }
    }
