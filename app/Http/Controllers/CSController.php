<?php
    
    namespace App\Http\Controllers;
    
    use App\POStep;
    use App\User;
    use App\UsersCart;
    use App\UsersPO;
    use Illuminate\Http\Request;
    
    use App\Http\Requests;
    use Illuminate\Support\Facades\Auth;

    class CSController extends Controller
    {
        public function __construct(User $user)
        {
            $this->user =  $user->currentUser();
        }
        
        /**
         * List all order
         * @param Request $request
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function ordersList(Request $request)
        {
            
            $ordersData = UsersPO::where('operator_id','=',0)->get();
            $orders = [];
            foreach ($ordersData as $value){
                $orders[] = [
                    'id' => $value->id,
                    'po_id' => $value->po_id,
                    'user_code' => User::find($value->user_id)->user_code,
                    'po_step' => POStep::find($value->po_step)->name,
                    'exchange_rate' => $value->exchange_rate,
                ];
            }
            
            //return $orders;
            return view('ui-backend/cs/index', compact('orders'));
        }
    
        /**
         * Response order
         * @param $order_id
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
         */
        public function responseOrder($order_id){
            $order = UsersPO::find($order_id);
            $order->operator_id = Auth::user()->id;
            $order->save();
            
            return redirect('/cs/manage-orders/'.$order->po_id);
        }
    
        public function orderManagementStaff(Request $request)
        {
            $ordersData = UsersPO::where('operator_id','=',Auth::user()->id)->get();
            $orders = [];
            foreach ($ordersData as $value){
                $orders[] = [
                    'id' => $value->id,
                    'po_id' => $value->po_id,
                    'user_code' => User::find($value->user_id)->user_code,
                    'po_step' => POStep::find($value->po_step)->name,
                    'exchange_rate' => $value->exchange_rate,
                ];
            }
    
            //return $orders;
            return view('ui-backend/cs/index-response-orders', compact('orders'));
        }
        
        /**
         * @param $order_id
         * @return string
         */
        public function orderManagement($order_id)
        {
            
            $po = UsersPO::where('po_id','=',$order_id)->first();
            $products = UsersCart::where('po_id','=',$order_id)->get();
    
            $user =  User::find($po->user_id);
    
            $total = 0;
            foreach ($products as $product){
                $total += $product->price * $product->quantity;
            }
    
            $thaiTotal = $total*$po->exchange_rate;
    
            $orderFee = ($total*10)/100;
    
            $dataSum = [
                'productTotal' => count($products),
                'order_fee' => $orderFee,
                'totalCost' => $orderFee+$total,
                'totalCostWithRate' => ($orderFee+$total)*$po->exchange_rate,
            ];
    
            $totalAddPay = ($po->payment_add_ship_in_cn+$po->payment_add_ship_in_th+$po->payment_add_buy_fee+$po->payment_add_buy_for_other) - $po->discount;
    
            return view('ui-backend/cs/order-detail', compact('po','user','products','total','thaiTotal','dataSum','totalAddPay'));
        }
    }
