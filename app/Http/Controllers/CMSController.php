<?php
    
    namespace App\Http\Controllers;
    
    use App\CMSPosts;
    use App\User;
    use Illuminate\Http\Request;
    
    use App\Http\Requests;
    use Illuminate\Support\Facades\Session;

    class CMSController extends Controller
    {
        /**
         * CMSController constructor.
         * @param User $user
         */
        public function __construct(User $user)
        {
//            $this->middleware('auth');
            $this->user =  $user->currentUser();
            
            $lang = Session::get ('lang');
            if ($lang != null) {
                \App::setLocale($lang);
            }else{
                Session::put('lang', 'th');
                \App::setLocale('th');
            }
        }
    
        public function postContent($id)
        {
            $user = $this->user;
            $content = CMSPosts::find($id);
            return view('ui-backend/admin/cms/post-content',compact('user','content'));
        }
    
        public function storeContent(Request $request)
        {
             return $request->all();
        }
    
        public function updateContent($id,Request $request)
        {
           //return $request->all();
           
            $page = CMSPosts::find($id);
            
            $page->body_en = $request->get('body_en');
            $page->body_th = $request->get('body_th');
            $page->body_cn = $request->get('body_cn');
            
            $page->save();
    
            Session::flash('status', "ข้อมูลถูกแก้ไขแล้ว");
            return redirect()->back();
        }
    }
