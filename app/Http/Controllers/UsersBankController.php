<?php

namespace App\Http\Controllers;

use App\User;
use App\UsersAdress;
use App\UsersBankAccounts;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class UsersBankController extends Controller
{
    /**
     * UsersBankController constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->middleware('auth');
        $this->user =  $user->currentUser();
    
        $lang = Session::get ('lang');
        if ($lang != null) {
            \App::setLocale($lang);
        }else{
            Session::put('lang', 'th');
            \App::setLocale('th');
        }
    }
    
    public function index()
    {
        $user = $this->user;
        $banks = UsersBankAccounts::where('user_id','=',$user->id)->get();
        
        return view('ui-backend/users/banks',compact('banks','user'));
    }
    
    
    
    /**
     * Add bank account
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addBank(Request $request,User $user)
    {
        $this->validate($request,[
            'account_number' => 'required|numeric',
            'account_name' => 'required',
            'bank_code' => 'required',
        ]);
        
        UsersBankAccounts::create([
            'bank_code' => $request->get('bank_code'),
            'bank_name' => $request->get('bank_name'),
            'account_number' => $request->get('account_number'),
            'account_name' => $request->get('account_name'),
            'account_branch' => $request->get('account_branch'),
            'note' => $request->get('note'),
            'user_id' => $user->currentUser()->id,
        ]);
    
        Session::flash('status', "ข้อมูลถูกบันทึกแล้ว");
        return redirect('/users/profile');
    }
    
    public function destroy($id)
    {
        $userBank = UsersBankAccounts::find($id);
        $userBank->delete();
        
        return redirect('/users/banks');
    }
    
    
    

}
