<?php

namespace App\Http\Controllers;

use App\User;
use App\UsersAdress;
use Faker\Provider\ar_JO\Address;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class UsersAddressController extends Controller
{
    
    /**
     * UsersAddressController constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->middleware('auth');
        $this->user =  $user->currentUser();
    
        $lang = Session::get ('lang');
        if ($lang != null) {
            \App::setLocale($lang);
        }else{
            Session::put('lang', 'th');
            \App::setLocale('th');
        }
    }
    
    public function index()
    {
        $user = $this->user;
        $address = UsersAdress::where('user_id','=',$user->id)->get();
        return view('ui-backend/users/address',compact('user','address'));
    }
    
    /**
     * Add address
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addAddress(Request $request, User $user)
    {
        $this->validate($request,[
            'receiver_name'  => 'required',
            'receiver_address'  => 'required',
            'receiver_district'  => 'required',
            'receiver_province'  => 'required',
            'receiver_zip_code'  => 'required|numeric',
            'receiver_tel'  => 'required|numeric',
            'default_address',
        ]);
        
        UsersAdress::create([
            'receiver_name'  => $request->get('receiver_name'),
            'receiver_address'  => $request->get('receiver_address'),
            'receiver_district'  => $request->get('receiver_district'),
            'receiver_province'  => $request->get('receiver_province'),
            'receiver_zip_code'  => $request->get('receiver_zip_code'),
            'receiver_tel'  => $request->get('receiver_tel'),
            //'default_address'=> $request->get('default_address'),
            'user_id' => $user->currentUser()->id,
        ]);
    
        Session::flash('status', "ข้อมูลถูกบันทึกแล้ว");
        return redirect('/users/profile');
    }
    
    
    public function destroy($id){
        $fav =  UsersAdress::find($id);
        
        $fav->delete();
        Session::flash('status', "ข้อมูลถูกลบแล้ว");
        return redirect('/users/address');
        
    }
}
