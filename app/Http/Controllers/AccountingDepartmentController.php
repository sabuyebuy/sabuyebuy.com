<?php
    
    namespace App\Http\Controllers;
    
    use App\User;
    use App\UserDeposit;
    use App\UserWithdrawal;
    use Illuminate\Http\Request;
    
    use App\Http\Requests;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Session;
    
    class AccountingDepartmentController extends Controller
    {
        /**
         * AccountingDepartmentController constructor.
         * @param User $user
         */
        public function __construct(User $user)
        {
            $this->middleware('auth');
            $this->user = Auth::user();
    
            $lang = Session::get ('lang');
            if ($lang != null) {
                \App::setLocale($lang);
            }else{
                Session::put('lang', 'th');
                \App::setLocale('th');
            }
        }
        
        /**
         * index view
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index()
        {
            $deposit = UserDeposit::where('status','=','รอการตรวจสอบ')->limit(5)->get();
            $withdrawal = UserWithdrawal::where('status','=','รอการตรวจสอบ')->limit(5)->get();
            return view('ui-backend/accounting/index', compact('user','deposit','withdrawal'));
        }
        
        /**
         * All Deposit
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function allDeposit()
        {
            //$deposit = UserDeposit::where('status','=','รอการตรวจสอบ')->paginate(25);
            $depositData = UserDeposit::all();
            
            $deposit = [];
            foreach ($depositData as $dep){
                $deposit[] = [
                    'id' => $dep->id,
                    'user_code' => User::find($dep->id)->user_code,
                    'bank' => $dep->bank,
                    'amount' => $dep->amount,
                    'date_time' => $dep->date_time,
                    'note' => $dep->note,
                    'status' => $dep->status,
                ];
            }
            //return $deposit;
            return view('ui-backend/accounting/deposit', compact('user','deposit'));
        }
        
        public function depositVerify($id){
            
            if($this->user->role  == 'accounting'){
                $deposit = UserDeposit::find($id);
                return view('ui-backend/accounting/deposit-verify',compact('deposit'));
            }else{
                abort(401, 'Unauthorized.');
            }
        }
        
        /**
         * Approve deposit (Accounting)
         * @param $id
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
         */
        public function depositVerifyApprove($id){
            
            if($this->user->role  == 'accounting'){
                
                $deposit = UserDeposit::find($id);
                $deposit->status = 'ตรวจสอบแล้ว';
                $deposit->staff_name = $this->user->name;
                $deposit->save();
                
                
                /**
                 * Update balance
                 */
                $updateBalance = User::find($deposit->user_id);
                $updateBalance->user_amount_balance = $updateBalance->user_amount_balance + $deposit->amount;
                $updateBalance->save();
                
                
                Session::flash('status', "ยีนยันข้อมูลแล้ว");
                return redirect('/accounting/home');
                
            }else{
                abort(401, 'Unauthorized.');
            }
        }
        
        
        public function allWithdrawal(){
            //$withdrawal = UserWithdrawal::where('status','=','รอการตรวจสอบ')->paginate(25);
            $withdrawalData = UserWithdrawal::all();
    
            $withdrawal = [];
            foreach ($withdrawalData as $wd){
                $withdrawal[] = [
                    'id' => $wd->id,
                    'user_code' => User::find($wd->id)->user_code,
                    'bank' => $wd->bank,
                    'amount' => $wd->amount,
                    'date_time' => $wd->date_time,
                    'note' => $wd->note,
                    'status' => $wd->status,
                    'transfer_date' => $wd->transfer_date,
                ];
            }
            
            return view('ui-backend/accounting/withdrawal', compact('user','withdrawal'));
        }
        
        public function withdrawalVerify($id){
            
            if($this->user->role  == 'accounting'){
                $withdrawal = UserWithdrawal::find($id);
                $userBalance = User::find($withdrawal->user_id)->user_amount_balance;
                return view('ui-backend/accounting/withdrawal-verify',compact('withdrawal','userBalance'));
            }else{
                abort(401, 'Unauthorized.');
            }
        }
        
        
        /**
         * Approve deposit (Accounting)
         * @param $id
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
         */
        public function withdrawalVerifyApprove($id,Request $request){
    
            $this->validate($request, [
                'amount' => 'required|numeric'
            ]);
            
            if($this->user->role  == 'accounting'){
                
                $withdrawalAmount = $request->get('amount');
                
                $path_image = "";
                if ($request->file('slip')) {
                    $image      = $request->file('slip');
                    $file = $image->getClientOriginalName();
                    $path_image = $image->move('uploads/withdrawal/', $file);
                }
                
                $withdrawal = UserWithdrawal::find($id);
                $withdrawal->status = $request->get('status');
                $withdrawal->amount = $request->get('amount');
                $withdrawal->slip = $path_image;
                $withdrawal->transfer_date = $request->get('transfer_date');
                $withdrawal->note = $request->get('note');
                $withdrawal->staff_name = $this->user->name;
                
                /**
                 * Update balance
                 */
                $userBalance = User::find($withdrawal->user_id);
                
                
                if($withdrawalAmount < $userBalance->user_amount_balance){
                    
                    $userBalance->user_amount_balance = $userBalance->user_amount_balance - $withdrawalAmount;
                   
                    $withdrawal->save();
                    $userBalance->save();
                    Session::flash('status', "ยีนยันข้อมูลแล้ว");
                    return redirect('/accounting/withdrawal');
                }else{
                    $withdrawal->save();
                    Session::flash('error', "ยอดเงินคงเหลือน้อยกว่ายอดถอน");
                    return redirect('/accounting/home');
                }
                
                
                
                
                
            }else{
                abort(401, 'Unauthorized.');
            }
        }
    
    
        public function depositCancel($id)
        {
            $deposit = UserDeposit::find($id);
            $deposit->status = 'ยกเลิก';
            $deposit->save();
            Session::flash('status', "ยกเลิกรายการแล้ว");
            return redirect('/accounting/deposit');
        }
    
    
        public function withdrawalCancel($id)
        {
            $withdrawal = UserWithdrawal::find($id);
            $withdrawal->status = 'ยกเลิก';
            $withdrawal->save();
            Session::flash('status', "ยกเลิกรายการแล้ว");
            return redirect('/accounting/withdrawal');
        }
    }
