<?php
    
    namespace App\Http\Controllers;
    
    use App\CMSPosts;
    use Illuminate\Http\Request;
    
    use App\Http\Requests;
    use Illuminate\Support\Facades\Session;
    
    class PagesContentController extends Controller
    {
        /**
         * PagesContentController constructor.
         */
        public function __construct()
        {
            $lang = Session::get ('lang');
            if ($lang != null) {
                \App::setLocale($lang);
            }else{
                Session::put('lang', 'th');
                \App::setLocale('th');
            }
    
            $this->lang = $lang;
            
        }
        
        /**
         * About us page
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function aboutUS()
        {
            return view('ui-frontend/pages/pages-content');
        }
        
        /**
         * Who we are page
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function whoWeAre()
        {
            $content = CMSPosts::find(1);
            
            if($this->lang == 'th'){
                $title = $content->title_th;
                $body = $content->body_th;
            }elseif ($this->lang == 'cn'){
                $title = $content->title_cn;
                $body = $content->body_cn;
            }else{
                $title = $content->title_en;
                $body = $content->body_en;
            }
            
            if($body == ''){
                $body = '<h1>Page under construction</h1>';
            }
            
            return view('ui-frontend/pages/pages-content',compact('title','body'));
        }
        
        /**
         * Vision page
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function vision()
        {
            $content = CMSPosts::find(2);
            
            if($this->lang == 'th'){
                $title = $content->title_th;
                $body = $content->body_th;
            }elseif ($this->lang == 'cn'){
                $title = $content->title_cn;
                $body = $content->body_cn;
            }else{
                $title = $content->title_en;
                $body = $content->body_en;
            }
    
            if($body == ''){
                $body = '<h1>Page under construction</h1>';
            }
    
            return view('ui-frontend/pages/pages-content',compact('title','body'));
        }
        
        /**
         * Mission page
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function mission()
        {
            $content = CMSPosts::find(3);
            
            if($this->lang == 'th'){
                $title = $content->title_th;
                $body = $content->body_th;
            }elseif ($this->lang == 'cn'){
                $title = $content->title_cn;
                $body = $content->body_cn;
            }else{
                $title = $content->title_en;
                $body = $content->body_en;
            }
    
            if($body == ''){
                $body = '<h1>Page under construction</h1>';
            }
    
            return view('ui-frontend/pages/pages-content',compact('title','body'));
        }
        
        /**
         * Core Competency page
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function coreCompetency()
        {
            $content = CMSPosts::find(4);
    
            if($this->lang == 'th'){
                $title = $content->title_th;
                $body = $content->body_th;
            }elseif ($this->lang == 'cn'){
                $title = $content->title_cn;
                $body = $content->body_cn;
            }else{
                $title = $content->title_en;
                $body = $content->body_en;
            }
    
            if($body == ''){
                $body = '<h1>Page under construction</h1>';
            }
    
            return view('ui-frontend/pages/pages-content',compact('title','body'));
        }
        
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function ourService()
        {
            $content = CMSPosts::find(5);
    
            if($this->lang == 'th'){
                $title = $content->title_th;
                $body = $content->body_th;
            }elseif ($this->lang == 'cn'){
                $title = $content->title_cn;
                $body = $content->body_cn;
            }else{
                $title = $content->title_en;
                $body = $content->body_en;
            }
    
            if($body == ''){
                $body = '<h1>Page under construction</h1>';
            }
    
            return view('ui-frontend/pages/pages-content',compact('title','body'));
        }
        
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function howToOrder()
        {
            $content = CMSPosts::find(6);
    
            if($this->lang == 'th'){
                $title = $content->title_th;
                $body = $content->body_th;
            }elseif ($this->lang == 'cn'){
                $title = $content->title_cn;
                $body = $content->body_cn;
            }else{
                $title = $content->title_en;
                $body = $content->body_en;
            }
    
            if($body == ''){
                $body = '<h1>Page under construction</h1>';
            }
    
            return view('ui-frontend/pages/pages-content',compact('title','body'));
        }
        
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function byTruck()
        {
            $content = CMSPosts::find(7);
    
            if($this->lang == 'th'){
                $title = $content->title_th;
                $body = $content->body_th;
            }elseif ($this->lang == 'cn'){
                $title = $content->title_cn;
                $body = $content->body_cn;
            }else{
                $title = $content->title_en;
                $body = $content->body_en;
            }
    
            if($body == ''){
                $body = '<h1>Page under construction</h1>';
            }
    
            return view('ui-frontend/pages/pages-content',compact('title','body'));
        }
        
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function byShip()
        {
            $content = CMSPosts::find(8);
    
            if($this->lang == 'th'){
                $title = $content->title_th;
                $body = $content->body_th;
            }elseif ($this->lang == 'cn'){
                $title = $content->title_cn;
                $body = $content->body_cn;
            }else{
                $title = $content->title_en;
                $body = $content->body_en;
            }
    
            if($body == ''){
                $body = '<h1>Page under construction</h1>';
            }
    
            return view('ui-frontend/pages/pages-content',compact('title','body'));
        }
        
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function byAirFlight()
        {
            $content = CMSPosts::find(9);
    
            if($this->lang == 'th'){
                $title = $content->title_th;
                $body = $content->body_th;
            }elseif ($this->lang == 'cn'){
                $title = $content->title_cn;
                $body = $content->body_cn;
            }else{
                $title = $content->title_en;
                $body = $content->body_en;
            }
    
            if($body == ''){
                $body = '<h1>Page under construction</h1>';
            }
    
            return view('ui-frontend/pages/pages-content',compact('title','body'));
        }
        
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function domesticFee()
        {
            $content = CMSPosts::find(10);
    
            if($this->lang == 'th'){
                $title = $content->title_th;
                $body = $content->body_th;
            }elseif ($this->lang == 'cn'){
                $title = $content->title_cn;
                $body = $content->body_cn;
            }else{
                $title = $content->title_en;
                $body = $content->body_en;
            }
    
            if($body == ''){
                $body = '<h1>Page under construction</h1>';
            }
    
            return view('ui-frontend/pages/pages-content',compact('title','body'));
        }
        
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function exportInfo()
        {
            $content = CMSPosts::find(11);
    
            if($this->lang == 'th'){
                $title = $content->title_th;
                $body = $content->body_th;
            }elseif ($this->lang == 'cn'){
                $title = $content->title_cn;
                $body = $content->body_cn;
            }else{
                $title = $content->title_en;
                $body = $content->body_en;
            }
    
            if($body == ''){
                $body = '<h1>Page under construction</h1>';
            }
    
            return view('ui-frontend/pages/pages-content',compact('title','body'));
        }
        
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function transportationFee()
        {
            $content = CMSPosts::find(12);
    
            if($this->lang == 'th'){
                $title = $content->title_th;
                $body = $content->body_th;
            }elseif ($this->lang == 'cn'){
                $title = $content->title_cn;
                $body = $content->body_cn;
            }else{
                $title = $content->title_en;
                $body = $content->body_en;
            }
    
            if($body == ''){
                $body = '<h1>Page under construction</h1>';
            }
    
            return view('ui-frontend/pages/pages-content',compact('title','body'));
        }
        
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function payment()
        {
            $content = CMSPosts::find(13);
    
            if($this->lang == 'th'){
                $title = $content->title_th;
                $body = $content->body_th;
            }elseif ($this->lang == 'cn'){
                $title = $content->title_cn;
                $body = $content->body_cn;
            }else{
                $title = $content->title_en;
                $body = $content->body_en;
            }
    
            if($body == ''){
                $body = '<h1>Page under construction</h1>';
            }
    
            return view('ui-frontend/pages/pages-content',compact('title','body'));
        }
        
        /**
         * Contact page
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function contact()
        {
            $content = CMSPosts::find(14);
    
            if($this->lang == 'th'){
                $title = $content->title_th;
                $body = $content->body_th;
            }elseif ($this->lang == 'cn'){
                $title = $content->title_cn;
                $body = $content->body_cn;
            }else{
                $title = $content->title_en;
                $body = $content->body_en;
            }
    
            if($body == ''){
                $body = '<h1>Page under construction</h1>';
            }
            
            return view('ui-frontend/pages/pages-content',compact('title','body'));
        }
    }
