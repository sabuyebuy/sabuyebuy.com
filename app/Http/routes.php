<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

use App\Http\Controllers\UsersFavoriteShop;
use App\Http\Requests\Request;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Sunra\PhpSimple\HtmlDomParser;

Route::get('debug', function () {

	//return  $balane = 0.00/5.19;


//        for ($i = 1; $i <= 20; $i++) {
//           echo  rand(5001,5900)."<br>";
//        }
//
//
	$url = 'https://www.amazon.cn/dp/B01MR5B9G2/ref=cngwdyfloorv2_recs_0/460-6742389-3065446?pf_rd_m=A1AJ19PSB66TGU&pf_rd_s=desktop-2&pf_rd_r=4S6BY13QEKF6FDPHN386&pf_rd_r=4S6BY13QEKF6FDPHN386&pf_rd_t=36701&pf_rd_p=78d99717-ba38-48ca-89aa-e501d77b022f&pf_rd_p=78d99717-ba38-48ca-89aa-e501d77b022f&pf_rd_i=desktop';

	$dom = HtmlDomParser::file_get_html( $url );

	return  $elems = $dom->find('priceblock_ourprice');

});

/**
 * Language switcher
 */
Route::get('lang-switch/{lang}','UsersController@langSwitcher');


Route::auth();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

Route::get('/cart', 'OrderController@cartList');
Route::get('/remove-from-cart/{product_id}', 'OrderController@removeFromCart');
Route::post('/cart-add', 'OrderController@addToCart');
Route::post('/create-po', 'OrderController@createPO');
Route::post('/create-po-confirm', 'OrderController@createPOConfirm');

Route::get('/product-view/{product_id}', 'OrderController@productView');




/**
 * User profile
 */

Route::group(['prefix' => 'users'], function () {

	/**
	 * Get requests
	 */
	Route::get('dashboard','UsersController@dashboard');

	Route::get('profile','UsersController@profile');
	Route::get('favorites','UsersFavoriteShop@index');
	Route::get('address','UsersAddressController@index');
	Route::get('banks','UsersBankController@index');
	Route::get('wallet/deposit','WalletController@depositIndex');
	Route::get('wallet/deposit/{id}','WalletController@depositShow');
	Route::get('wallet/withdrawal','WalletController@withdrawalIndex');
	Route::get('wallet/withdrawal/{id}','WalletController@withdrawalShow');

	Route::get('po/show/{po_id}','UsersController@showPO');



	/**
	 * Post requests
	 */
	Route::post('add-fav-shop','UsersFavoriteShop@store');
	Route::post('add-bank','UsersBankController@addBank');
	Route::post('add-address','UsersAddressController@addAddress');
	Route::post('wallet/deposit','WalletController@depositStore');
	Route::post('wallet/withdrawal','WalletController@withdrawalStore');


	/**
	 * Put & Patch requests
	 */
	Route::put('update-profile/{id}','UsersController@updateProfile');
	Route::put('wallet/deposit/edit/{id}','WalletController@depositUpdate');
	Route::put('wallet/withdrawal/edit/{id}','WalletController@withdrawalUpdate');

	/**
	 * Delete requests
	 */
	Route::delete('delete-fav-shop/{id}','UsersFavoriteShop@destroy');
	Route::delete('delete-address/{id}','UsersAddressController@destroy');
	Route::delete('delete-bank/{id}','UsersBankController@destroy');
	Route::delete('wallet/deposit/{id}','WalletController@depositDestroy');
	Route::delete('wallet/withdrawal/{id}','WalletController@withdrawalDestroy');



});

Route::get('paywithpaypal', array('as' => 'addmoney.paywithpaypal','uses' => 'AddMoneyController@payWithPaypal',));
Route::post('paypal', array('as' => 'addmoney.paypal','uses' => 'AddMoneyController@postPaymentWithpaypal',));
Route::get('paypal', array('as' => 'payment.status','uses' => 'AddMoneyController@getPaymentStatus',));


/**
 * Administrator
 */

Route::group(['prefix' => 'admin'], function () {

	/**
	 * Admin Home
	 */
	Route::get('home','AdministratorController@home');
	Route::get('languages','AdministratorController@languagesView');
	Route::post('languages','AdministratorController@languagesUpdate');

	/**
	 * Get request
	 */
	Route::get('exchange-rate','AdministratorController@exchangeRate');

	Route::get('cms/page/{id}','CMSController@postContent');

	Route::get('cms/page/{id}','CMSController@postContent');

	Route::get('users','UsersController@listAll');
	Route::get('users/edit/{user_id}','UsersController@userEdit');


	Route::get('products','ProductController@index');
	Route::get('products/create','ProductController@create');
	Route::get('products/edit/{product_id}','ProductController@edit');




	/**
	 * Post request
	 */
	Route::put('cms/{id}','CMSController@updateContent');

	Route::post('exchange-rate','AdministratorController@exchangeRateUpdate');

	Route::post('users/edit/{user_id}','UsersController@userUpdate');
	Route::post('users/delete/{user_id}','UsersController@userDelete');

	Route::post('products','ProductController@store');
	Route::post('products/edit/{product_id}','ProductController@update');
	Route::post('products/delete/{product_id}','ProductController@destroy');



});

/**
 * Accounting
 */

Route::group(['prefix' => 'accounting'], function () {

	/**
	 * Get request
	 */

	Route::get('home','AccountingDepartmentController@index');

	Route::get('/deposit','AccountingDepartmentController@allDeposit');
	Route::get('/deposit/verify/{id}','AccountingDepartmentController@depositVerify');
	Route::get('/deposit/verify/approve/{id}','AccountingDepartmentController@depositVerifyApprove');
	Route::get('/deposit/verify/cancel/{id}','AccountingDepartmentController@depositCancel');

	Route::get('/withdrawal','AccountingDepartmentController@allWithdrawal');
	Route::get('/withdrawal/verify/approve/{id}','AccountingDepartmentController@withdrawalVerify');
	Route::get('/withdrawal/verify/cancel/{id}','AccountingDepartmentController@withdrawalCancel');

	/**
	 * Post request
	 */
	Route::post('/withdrawal/verify/approve/{id}','AccountingDepartmentController@withdrawalVerifyApprove');

});

/**
 * Customer service (CS)
 */

Route::group(['prefix' => 'cs'], function () {
	Route::get('home','CSController@ordersList');

	Route::get('/orders','CSController@ordersList');
	Route::get('/response-order/{order_id}','CSController@responseOrder');
	Route::get('/my-response-orders','CSController@orderManagementStaff');
	Route::get('/manage-orders/{po_id}','CSController@orderManagement');

});




/**
 * Pages content
 */
Route::get('/about-us', 'PagesContentController@aboutUS');
Route::get('/about-us/who-we-are', 'PagesContentController@whoWeAre');
Route::get('/about-us/vision', 'PagesContentController@vision');
Route::get('/about-us/mission', 'PagesContentController@mission');
Route::get('/about-us/core-competency', 'PagesContentController@coreCompetency');
Route::get('/about-us/our-services', 'PagesContentController@ourService');


Route::get('/import/by-truck', 'PagesContentController@byTruck');
Route::get('/import/by-ship', 'PagesContentController@byShip');
Route::get('/import/by-air-flight', 'PagesContentController@byAirFlight');
Route::get('/import/domestic-fee', 'PagesContentController@domesticFee');


Route::get('/export/information', 'PagesContentController@exportInfo');
Route::get('/export/transportation-fee', 'PagesContentController@transportationFee');
Route::get('/export/payment', 'PagesContentController@payment');

Route::get('/how-to-order', 'PagesContentController@howToOrder');
Route::get('/contact', 'PagesContentController@contact');



///**
// * =========================================================
// *  API
// * =========================================================
// */

Route::group(['prefix' => 'api/v1/'], function () {

	/**
	 * Setting Lang
	 */
	Route::post('/setting/lang', 'APIsController@langSetting');

});
