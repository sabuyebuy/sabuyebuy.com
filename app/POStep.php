<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class POStep extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'po_status';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
}
