var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir.config.sourcemaps = false;
elixir(function(mix) {
    mix.sass('app-frontend.scss','resources/assets/css/app-frontend.css');
    mix.sass('app-backend.scss','resources/assets/css/app-backend.css');


    /**
     * Front-end CSS
     */
    mix.styles([
        'front-end/jquery-ui.css',
        'front-end/bootstrap.min.css',
        'front-end/owl.carousel.css',
        'front-end/owl.theme.css',
        'front-end/owl.transitions.css',
        'front-end/font-awesome.min.css',
        'front-end/nivo-slider.css',
        'front-end/fancybox/jquery.fancybox.css',
        'front-end/animate.css',
        'front-end/normalize.css',
        'front-end/meanmenu.min.css',
        'front-end/main.css',
        'front-end/style.css',
        'front-end/flag-icon.css',
        'front-end/responsive.css',
        'back-end/libs/stroke-7/style.css',
        'app-frontend.css',
    ], 'public/css/front-end.css');

    /**
     * Front-end JS
     */
    mix.scripts([
        'front-end/vendor/modernizr-2.8.3.min.js',
        'front-end/vendor/jquery-1.11.3.min.js',
        'front-end/price-slider.js',
        'front-end/bootstrap.min.js',
        'front-end/jquery.nivo.slider.pack.js',
        'front-end/owl.carousel.min.js',
        'front-end/jquery.countdown.min.js',
        'front-end/jquery.elevatezoom.js',
        'front-end/wow.js',
        'front-end/jquery.meanmenu.js',
        'front-end/fancybox/jquery.fancybox.pack.js',
        'front-end/jquery.scrollUp.min.js',
        'front-end/jquery.mixitup.min.js',
        'front-end/plugins.js',
        'front-end/main.js',
    ],'public/js/front-end.js');



    /**
     * Back-end CSS
     */
    mix.styles([
        'back-end/libs/bootstrap/dist/css/bootstrap.min.css',
        'back-end/libs/animate/animate.css',
        'back-end/libs/stroke-7/style.css',
        'back-end/libs/datetimepicker/css/bootstrap-datetimepicker.min.css',
        'back-end/libs/jquery.niftymodals/css/component.css',
        'back-end/libs/select2/css/select2.min.css',
        'back-end/libs/bootstrap-slider/css/bootstrap-slider.css',
        'back-end/bootstrap-clockpicker.min.css',
        'back-end/libs/dropzone/dist/dropzone.css',
        'back-end/libs/daterangepicker/daterangepicker.css',
        'back-end/libs/datatable/dataTables.bootstrap.css',
        'back-end/sweetalert.css',
        'back-end/introjs.css',
        'back-end/style.css',
        'app-backend.css',
    ], 'public/css/back-end.css');

    /**
     * Back-end JS
     */
    mix.scripts([
        'back-end/libs/jquery/jquery.min.js',
        'back-end/libs/jquery.nanoscroller/javascripts/jquery.nanoscroller.min.js',
        'back-end/main.js',
        'back-end/libs/bootstrap/dist/js/bootstrap.min.js',
        'back-end/app-form-wizard.js',
        'back-end/libs/fuelux/js/wizard.js',
        'back-end/libs/parsley/parsley.min.js',
        'back-end/libs/jquery-ui/jquery-ui.min.js',
        'back-end/libs/jquery.nestable/jquery.nestable.js',
        'back-end/libs/moment.js/moment.js',
        'back-end/libs/datetimepicker/js/bootstrap-datetimepicker.min.js',
        'back-end/daterangepicker.js',
        'back-end/libs/dropzone/dist/dropzone.js',
        'back-end/libs/select2/js/select2.min.js',
        'back-end/libs/bootstrap-slider/js/bootstrap-slider.js',
        'back-end/bootstrap-clockpicker.min.js',
        'back-end/libs/jquery.niftymodals/js/jquery.modalEffects.js',
        'back-end/jquery.niftymodals.js',
        'back-end/libs/moment.js/min/moment.min.js',
        'back-end/libs/skycons/skycons.js',
        'back-end/app-form-elements.js',
        'back-end/libs/countup/countUp.min.js',
        'back-end/libs/jquery-flot/jquery.flot.js',
        'back-end/libs/jquery-flot/jquery.flot.pie.js',
        'back-end/libs/datatables/js/jquery.dataTables.min.js',
        'back-end/libs/datatables/js/dataTables.bootstrap.min.js',
        'back-end/libs/datatables/plugins/buttons/js/dataTables.buttons.js',
        'back-end/libs/datatables/plugins/buttons/js/buttons.html5.js',
        'back-end/libs/datatables/plugins/buttons/js/buttons.flash.js',
        'back-end/libs/datatables/plugins/buttons/js/buttons.print.js',
        'back-end/libs/datatables/plugins/buttons/js/buttons.colVis.js',
        'back-end/libs/datatables/plugins/buttons/js/buttons.bootstrap.js',
        'back-end/sweetalert.min.js',
        'back-end/intro.js'
    ],'public/js/back-end.js');
});
