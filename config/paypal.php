<?php
return array(
	/** set your paypal credential **/
	'client_id' =>'Ab5KT_t6UNDxw5PtR-AYy3KRalLyxPT67-toSEY9fP3JOYMQlg0FdojHXFJfOEIi1NUxs2wbipwyGNPh',
	'secret' => 'EBK0SOo5oay4vinVZWMkNIujoB9efgn2IPqieUqJA4zhD05R3kXr5d0Y9qissHCiYyqpgacFmyt15mO7',
	/**
	 * SDK configuration
	 */
	'settings' => array(
		/**
		 * Available option 'sandbox' or 'live'
		 */
		'mode' => 'sandbox',
		/**
		 * Specify the max request time in seconds
		 */
		'http.ConnectionTimeOut' => 1000,
		/**
		 * Whether want to log to a file
		 */
		'log.LogEnabled' => true,
		/**
		 * Specify the file that want to write on
		 */
		'log.FileName' => storage_path() . '/logs/paypal.log',
		/**
		 * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
		 *
		 * Logging is most verbose in the 'FINE' level and decreases as you
		 * proceed towards ERROR
		 */
		'log.LogLevel' => 'FINE'
	),
);