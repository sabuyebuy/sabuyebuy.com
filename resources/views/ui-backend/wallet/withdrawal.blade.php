@extends('ui-backend/partials.master')



@section('content')

    {{--@include('ui-backend/wallet/deposit/header')--}}

    <div class="main-content">

        @include('errors.error')
        <div class="row">
            {{--@include('ui-backend/users/sidebar')--}}
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="tools"><a data-modal="withdrawal" class="md-trigger" href="#"><button type="link" class="btn btn-space btn-success" data-step="4" data-intro="Add deposit" data-position='right'> {{trans('lang.menu_account_withdrawal')}}</button></a></div>
                        <div class="title">ข้อมูลการถอนเงิน</div>
                    </div>
                    <div class="panel-body">

                        <table id="table-deposit" class="table table-striped table-hover table-fw-widget">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>เข้าธนาคาร</th>
                                <th>จำนวนเงิน</th>
                                <th>วัน/เวลาที่แจ้ง</th>
                                <th>สถานะ</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0?>
                            @foreach($withdrawal as $with)

                                <tr>
                                    <td class="">{{$i+1}}</td>
                                    <td class="">{{$with->bank}}</td>
                                    <td class="">{{number_format($with->amount)}}</td>
                                    <td class="">{{$with->date_time}}</td>
                                    <td class="">{{$with->status}}</td>
                                    <td class="text-right">

                                        <div class="btn-group btn-space">
                                            <button type="button" data-toggle="dropdown" class="btn btn-primary">Actions <span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a data-modal="full-danger-edit-{{$with->id}}"  href="/users/wallet/withdrawal/{{$with->id}}">ดูข้อมูล</a></li>
                                                <li><a data-modal="full-danger-{{$with->id}}" class="md-trigger" href="#">{{trans('lang.btn_delete')}}</a></li>
                                            </ul>
                                        </div>

                                    </td>
                                </tr>

                                <?php $i++;?>
                            @endforeach

                            </tbody>
                        </table>

                        <div class="text-center"><h2>Total withdrawal : {{number_format($totalWithdrawal)}} THB</h2></div>

                    </div>
                </div>
            </div>

        </div>

    <!--Delete-->
    @foreach($withdrawal as $with)

        <!-- Nifty Modal-->
            <div id="full-danger-{{$with->id}}" class="md-modal full-color danger md-effect-8">
                <div class="md-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                        <h3 class="modal-title">{{trans('lang.btn_alert_danger')}} !</h3>
                    </div>
                    <form action="/users/wallet/withdrawal/{{$with->id}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" name="_method" value="DELETE" />
                        <div class="modal-body">
                            <div class="text-center">
                                <div class="i-circle success"><i class="icon s7-check"></i></div>
                                <h4>คุณต้องการที่จะลบรายการนี้ ?</h4>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                            <button type="submit" class="btn btn-success btn-primary">{{trans('lang.btn_confirm')}}</button>
                        </div>
                    </form>
                </div>
            </div>
    @endforeach

    <!--Edit-->
    @foreach($withdrawal as $with)

        <!-- Nifty Modal-->
            <div id="full-danger-{{$with->id}}" class="md-modal full-color danger md-effect-8">
                <div class="md-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                        <h3 class="modal-title">{{trans('lang.btn_alert_danger')}} !</h3>
                    </div>
                    <form action="/users/wallet/withdrawal/edit/1" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">{{trans('lang.deposit_to')}}</label>

                                <select class="form-control" name="bank">
                                    @foreach($banks as $bank)
                                        <option value="{{$bank->bank_code}}">{{$bank->bank_name}} {{$bank->bank_account}}</option>
                                    @endforeach
                                </select>

                            </div>

                            <div class="form-group">
                                <label>จำนวนเงิน</label>
                                <input type="number" name="amount"  placeholder="จำนวน" value="" class="form-control">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                            <button type="submit" class="btn btn-success btn-primary">{{trans('lang.btn_confirm')}}</button>
                        </div>
                    </form>
                </div>
            </div>
    @endforeach

    <!-- Nifty Modal add new bank-->
        <div id="withdrawal" class="md-modal modal-container modal-colored-header md-effect-8">
            <div class="md-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                    <h3 class="modal-title">{{trans('lang.add_deposit')}}</h3>
                    <hr>
                </div>
                <form action="/users/wallet/withdrawal" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="modal-body">

                        <div class="form-group">
                            <label>จำนวนเงินคงเหลือของคุณ</label>
                            <input readonly type="number" name=""  placeholder="จำนวน" value="{{Auth::user()->user_amount_balance}}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="control-label">{{trans('lang.deposit_to')}}</label>

                            <select class="form-control" name="bank">
                                @foreach($banks as $bank)
                                    <option value="{{$bank->bank_code}}">{{$bank->bank_name}} {{$bank->bank_account}}</option>
                                @endforeach
                            </select>

                        </div>

                        <div class="form-group">
                            <label>จำนวนเงิน</label>
                            <input type="number" name="amount"  placeholder="จำนวน" value="" class="form-control">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                        <button type="submit" class="btn btn-success btn-primary">{{trans('lang.btn_confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>


    </div>


@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
            $('.md-trigger').modalEffects();

            $('#table-deposit').dataTable( {
                "pageLength": 50
            } );

        });
    </script>

@endsection