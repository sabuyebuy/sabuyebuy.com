@extends('ui-backend/partials.master')



@section('content')

    {{--@include('ui-backend/wallet/deposit/header')--}}

    <div class="main-content">

        @include('errors.error')
        <div class="row">
            {{--@include('ui-backend/users/sidebar')--}}
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="tools"></div>
                        <div class="title">ข้อมูลการโอนเงิน</div>
                    </div>
                    <div class="panel-body">

                        <div class="panel-body">
                            <h1>ยอดโอน : {{$deposit->amount}}  เวลา : {{$deposit->date_time}}</h1>
                            <h1>โอนเข้าธนาคาร  : {{$deposit->bank}} </h1>

                            <hr>
                            <h1>Slip</h1>
                            @if($deposit->slip)
                                <img class="img-responsive" src="{{URL('/')}}/{{$deposit->slip}}" alt="">
                            @endif

                        </div>

                    </div>
                </div>
            </div>



        </div>
    </div>


@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
            $('.md-trigger').modalEffects();

            $('#table-deposit').dataTable( {
                "pageLength": 50
            } );

        });
    </script>

@endsection