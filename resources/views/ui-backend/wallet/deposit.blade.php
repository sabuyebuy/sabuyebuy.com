@extends('ui-backend/partials.master')



@section('content')

    {{--@include('ui-backend/wallet/deposit/header')--}}

    <div class="main-content">

        @include('errors.error')
        <div class="row">
            {{--@include('ui-backend/users/sidebar')--}}
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="tools"><a data-modal="deposit" class="md-trigger" href="#"><button type="link" class="btn btn-space btn-success" data-step="4" data-intro="Add deposit" data-position='right'> {{trans('lang.add_deposit')}}</button></a></div>
                        <div class="tools"><a data-modal="paypal" class="md-trigger" href="#"><button type="link" class="btn btn-space btn-info" data-step="4" data-intro="Add deposit" data-position='right'> {{trans('lang.add_paypal')}}</button></a></div>
                        <div class="title">ข้อมูลการโอนเงิน</div>
                    </div>
                    <div class="panel-body">

                        <table id="table-deposit" class="table table-striped table-hover table-fw-widget">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>ธนาคาร</th>
                                <th>จำนวนเงิน</th>
                                <th>วัน/เวลาที่โอน</th>
                                <th>หมายเหตุ    </th>
                                <th>สถานะ</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
							<?php $i = 0?>
                            @foreach($deposit as $dep)

                                <tr>
                                    <td class="">{{$i+1}}</td>
                                    <td class="">{{$dep->bank}}</td>
                                    <td class="">{{number_format($dep->amount)}}</td>
                                    <td class="">{{$dep->date_time}}</td>
                                    <td class="">{{$dep->note}}</td>
                                    <td class="">{{$dep->status}}</td>
                                    <td class="text-right">

                                        <div class="btn-group btn-space">
                                            <button type="button" data-toggle="dropdown" class="btn btn-primary">Actions <span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a data-modal="full-danger-edit-{{$dep->id}}"  href="/users/wallet/deposit/{{$dep->id}}">ดูข้อมูล</a></li>
                                                <li><a data-modal="full-danger-{{$dep->id}}" class="md-trigger" href="#">{{trans('lang.btn_delete')}}</a></li>
                                            </ul>
                                        </div>

                                    </td>
                                </tr>

								<?php $i++;?>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="text-center"><h2>Total deposit : {{number_format($totalDeposit)}} THB</h2></div>

                    </div>
                </div>
            </div>

        </div>

        <!--Delete-->
    @foreach($deposit as $dep)

        <!-- Nifty Modal-->
            <div id="full-danger-{{$dep->id}}" class="md-modal full-color danger md-effect-8">
                <div class="md-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                        <h3 class="modal-title">{{trans('lang.btn_alert_danger')}} !</h3>
                    </div>
                    <form action="/users/wallet/deposit/{{$dep->id}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" name="_method" value="DELETE" />
                        <div class="modal-body">
                            <div class="text-center">
                                <div class="i-circle success"><i class="icon s7-check"></i></div>
                                <h4>คุณต้องการที่จะลบรายการนี้ ?</h4>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                            <button type="submit" class="btn btn-success btn-primary">{{trans('lang.btn_confirm')}}</button>
                        </div>
                    </form>
                </div>
            </div>
    @endforeach

    <!--Edit-->
    @foreach($deposit as $dep)

        <!-- Nifty Modal-->
            <div id="full-danger-{{$dep->id}}" class="md-modal full-color danger md-effect-8">
                <div class="md-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                        <h3 class="modal-title">{{trans('lang.btn_alert_danger')}} !</h3>
                    </div>
                    <form action="/users/wallet/deposit/edit/1" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">{{trans('lang.deposit_to')}}</label>

                                <select class="form-control" name="bank">
                                    @foreach($cop_banks as $bank)
                                        <option value="{{$bank->bank_code}}">{{$bank->bank_name}} {{$bank->bank_account}}</option>
                                    @endforeach
                                </select>

                            </div>

                            <div class="form-group">
                                <label>จำนวนเงิน</label>
                                <input type="number" name="amount"  placeholder="จำนวน" value="" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>แนบ slip</label>
                                <input type="file" name="slip"  placeholder="Slip" value="" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>วันเวลาที่โอน</label>
                                <input type="text" name="date_time"  placeholder="วันเวลาที่โอน" value="" class="form-control datetimepicker">
                            </div>

                            <div class="form-group">
                                <label>หมายเหตุ</label>
                                <textarea name="note" id="" cols="30" rows="3" class="form-control"></textarea>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                            <button type="submit" class="btn btn-success btn-primary">{{trans('lang.btn_confirm')}}</button>
                        </div>
                    </form>
                </div>
            </div>
    @endforeach

    <!-- Nifty Modal add new bank-->
        <div id="deposit" class="md-modal modal-container modal-colored-header md-effect-8">
            <div class="md-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                    <h3 class="modal-title">{{trans('lang.add_deposit')}}</h3>
                    <hr>
                </div>
                <form action="/users/wallet/deposit" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">{{trans('lang.deposit_to')}}</label>

                            <select class="form-control" name="bank">
                                @foreach($cop_banks as $bank)
                                    <option value="{{$bank->bank_code}}">{{$bank->bank_name}} {{$bank->bank_account}}</option>
                                @endforeach
                            </select>

                        </div>

                        <div class="form-group">
                            <label>จำนวนเงิน</label>
                            <input type="number" name="amount"  placeholder="จำนวน" value="" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>แนบ slip</label>
                            <input type="file" name="slip"  placeholder="Slip" value="" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>วันเวลาที่โอน</label>
                            <input type="text" name="date_time"  placeholder="วันเวลาที่โอน" value="" class="form-control datetimepicker">
                        </div>

                        <div class="form-group">
                            <label>หมายเหตุ</label>
                            <textarea name="note" id="" cols="30" rows="3" class="form-control"></textarea>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                        <button type="submit" class="btn btn-success btn-primary">{{trans('lang.btn_confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>


        <div id="paypal" class="md-modal modal-container modal-colored-header md-effect-8">
            <div class="md-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                    <h3 class="modal-title">{{trans('lang.add_paypal')}}</h3>
                </div>
                <form action="{!! URL::route('addmoney.paypal') !!}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="modal-body">

                        <div class="form-group">
                            <label>จำนวนเงิน</label>
                            <input type="number" name="amount"  placeholder="จำนวน" value="" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                        <button type="submit" class="btn btn-success btn-primary">{{trans('lang.btn_confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>

    </div>


@endsection

@section('scripts')

    <script type="text/javascript">

		$(document).ready(function(){
			//initialize the javascript
			App.init();
			App.formElements();
			$('.md-trigger').modalEffects();

			$('#table-deposit').dataTable( {
				"pageLength": 50
			} );

		});
    </script>

@endsection