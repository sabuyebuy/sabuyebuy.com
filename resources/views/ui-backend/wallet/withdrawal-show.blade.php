@extends('ui-backend/partials.master')



@section('content')

    {{--@include('ui-backend/wallet/deposit/header')--}}

    <div class="main-content">

        @include('errors.error')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="tools"><a data-modal="withdrawal" class="md-trigger" href="#"><button type="link" class="btn btn-space btn-success" data-step="4" data-intro="Add deposit" data-position='right'> {{trans('lang.menu_account_withdrawal')}}</button></a></div>
                        <div class="title">ข้อมูลการถอนเงิน</div>
                    </div>
                    <div class="panel-body">

                        <h2>ยอดแจ้งถอน : {{$withdrawal->amount}}  เวลา : {{$withdrawal->date_time}}</h2>
                        <h2>ให้โอนเข้าธนาคาร  : {{$withdrawal->bank}} </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="tools"></div>
                        <div class="title">ข้อมูลการโอนเงินจากบริษัท</div>
                    </div>
                    <div class="panel-body">
                        <h2>สถนานะ : {{$withdrawal->status}} เวลา : {{$withdrawal->transfer_date}}</h2>
                        <h3>เจ้าหน้าที่ผู้ตรวจสอบ : {{$withdrawal->staff_name}}</h3>
                        <h3>หมายเหตุ : {{$withdrawal->note}}</h3>

                        <hr>
                        @if($withdrawal->slip)
                        <h2>Slip</h2>
                            <img class="img-responsive" src="{{URL('/')}}/{{$withdrawal->slip}}" alt="">
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
            $('.md-trigger').modalEffects();

            $('#table-deposit').dataTable( {
                "pageLength": 50
            } );

        });
    </script>

@endsection