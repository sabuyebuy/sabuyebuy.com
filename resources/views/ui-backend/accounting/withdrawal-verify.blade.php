@extends('ui-backend/partials.master')



@section('content')

    {{--@include('ui-backend/wallet/deposit/header')--}}

    <div class="main-content">

        @include('errors.error')
        <div class="row">
            {{--@include('ui-backend/users/sidebar')--}}
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="tools">
                            <a href="/accounting/withdrawal/verify/cancel/{{$withdrawal->id}}"><button type="link" class="btn btn-space btn-danger"   data-position='right'>ยกเลิกรายการนี้</button></a>
                        </div>
                        <div class="title">ข้อมูลการโอนเงิน</div>
                    </div>
                    <div class="panel-body">
                        <h1>ยอดเงินคงเหลือของ ลูกค้า: <span class="label label-success">{{number_format($userBalance)}}</span></h1>
                        <hr>

                        <h1>ยอดแจ้งถอน : <span class="label label-danger">{{$withdrawal->amount}}</span>  เวลา : {{$withdrawal->date_time}}</h1>
                        <h1>ให้โอนเข้าธนาคาร  : {{$withdrawal->bank}} </h1>

                        <hr>
                    </div>
                </div>
            </div>


            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="tools"></div>
                        <div class="title">แนบ Slip การโอน</div>
                    </div>
                    <div class="panel-body">

                       <form action="/accounting/withdrawal/verify/approve/{{$withdrawal->id}}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="control-label">{{trans('lang.deposit_to')}}</label>

                                    <select class="form-control" name="status">
                                            <option value="โอนแล้ว">โอนแล้ว</option>
                                            <option value="ไม่อนุมัติ">ไม่อนุมัติ</option>
                                    </select>

                                </div>

                                <div class="form-group">
                                    <label>จำนวนเงิน</label>
                                    <input type="number" name="amount"  placeholder="จำนวน" value="{{$withdrawal->amount}}" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label>แนบ slip</label>
                                    <input type="file" name="slip"  placeholder="Slip" value="" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label>วันเวลาที่โอน</label>
                                    <input type="text" name="transfer_date"  placeholder="วันเวลาที่โอน" value="" class="form-control datetimepicker">
                                </div>

                                <div class="form-group">
                                    <label>หมายเหตุ</label>
                                    <textarea name="note" id="" cols="30" rows="3" class="form-control"></textarea>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success btn-primary form-control">{{trans('lang.btn_confirm')}}</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>



    </div>


@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
            $('.md-trigger').modalEffects();

            $('#table-deposit').dataTable( {
                "pageLength": 50
            } );

        });
    </script>

@endsection