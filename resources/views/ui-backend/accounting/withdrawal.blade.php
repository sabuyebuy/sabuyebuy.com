@extends('ui-backend/partials.master')



@section('content')

    {{--@include('ui-backend/wallet/deposit/header')--}}

    <div class="main-content">

        @include('errors.error')
        <div class="row">
            {{--@include('ui-backend/users/sidebar')--}}
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="title">ข้อมูลการแจ้งถอนเงิน</div>
                    </div>
                    <div class="panel-body">

                        <table id="table-deposit" class="table table-striped table-hover table-fw-widget">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>รหัสลูกค้า</th>
                                <th>ธนาคาร</th>
                                <th>จำนวนเงิน</th>
                                <th>วัน/เวลาที่โอน</th>
                                <th>หมายเหตุ    </th>
                                <th>สถานะ</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0?>
                            @foreach($withdrawal as $withdraw)

                                <tr>
                                    <td class="">{{$i+1}}</td>
                                    <td class="">{{$withdraw['user_code']}}</td>
                                    <td class="">{{$withdraw['bank']}}</td>
                                    <td class="">{{$withdraw['amount']}}</td>
                                    <td class="">{{$withdraw['date_time']}}</td>
                                    <td class="">{{$withdraw['note']}}</td>
                                    <td class="">{{$withdraw['status']}} <span class="label label-success">{{$withdraw['transfer_date']}}</span></td>
                                    <td class="text-right">

                                        <div class="btn-group btn-space">
                                            <a href="/accounting/withdrawal/verify/approve/{{$withdraw['id']}}"><button type="button" class="btn btn-success">ตรวจสอบข้อมูล</button></a>
                                        </div>

                                    </td>
                                </tr>

                                <?php $i++;?>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>



    </div>


@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
            $('.md-trigger').modalEffects();

            $('#table-deposit').dataTable( {

            } );

        });
    </script>

@endsection