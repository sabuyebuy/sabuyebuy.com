@extends('ui-backend/partials.master')



@section('content')

    {{--@include('ui-backend/wallet/deposit/header')--}}

    <div class="main-content">

        @include('errors.error')
        <div class="row">
            {{--@include('ui-backend/users/sidebar')--}}
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="tools"></div>
                        <div class="title">ข้อมูลการโอนเงิน</div>
                    </div>
                    <div class="panel-body">

                        <table id="table-deposit" class="table table-striped table-hover table-fw-widget">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>รหัสลูกค้า</th>
                                <th>ธนาคาร</th>
                                <th>จำนวนเงิน</th>
                                <th>วัน/เวลาที่โอน</th>
                                <th>หมายเหตุ    </th>
                                <th>สถานะ</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0?>
                            @foreach($deposit as $dep)

                                <tr>
                                    <td class="">{{$i+1}}</td>
                                    <td class="">{{$dep['user_code']}}</td>
                                    <td class="">{{$dep['bank']}}</td>
                                    <td class="">{{$dep['amount']}}</td>
                                    <td class="">{{$dep['date_time']}}</td>
                                    <td class="">{{$dep['note']}}</td>
                                    <td class="">
                                        @if($dep['status'] == 'ตรวจสอบแล้ว')<span class="label label-success">{{$dep['status']}}</span>@endif
                                        @if($dep['status'] == 'รอการตรวจสอบ')<span class="label label-warning">{{$dep['status']}}</span>@endif
                                        @if($dep['status'] == 'ยกเลิก')<span class="label label-danger">{{$dep['status']}}</span>@endif
                                    </td>
                                    <td class="text-right">

                                        <div class="btn-group btn-space">
                                            <a href="/accounting/deposit/verify/{{$dep['id']}}"><button type="button" class="btn btn-success">ตรวจสอบข้อมูล</button></a>
                                        </div>

                                    </td>
                                </tr>

                                <?php $i++;?>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>



    </div>


@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
            $('.md-trigger').modalEffects();

            $('#table-deposit').dataTable( {
                "pageLength": 50
            } );

        });
    </script>

@endsection