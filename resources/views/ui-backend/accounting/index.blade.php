@extends('ui-backend/partials.master')



@section('content')

    {{--@include('ui-backend/wallet/deposit/header')--}}

    <div class="main-content">

        @include('errors.error')
        <div class="row">
            {{--@include('ui-backend/users/sidebar')--}}
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="tools"></div>
                        <div class="title">ข้อมูลการแจ้งโอนเงิน</div>
                    </div>
                    <div class="panel-body">

                        <table id="table-deposit" class="table table-striped table-hover table-fw-widget">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>ธนาคาร</th>
                                <th>จำนวนเงิน</th>
                                <th>วัน/เวลาที่โอน</th>
                                <th>หมายเหตุ    </th>
                                <th>สถานะ</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0?>
                            @foreach($deposit as $dep)

                                <tr>
                                    <td class="">{{$i+1}}</td>
                                    <td class="">{{$dep->bank}}</td>
                                    <td class="">{{$dep->amount}}</td>
                                    <td class="">{{$dep->date_time}}</td>
                                    <td class="">{{$dep->note}}</td>
                                    <td class="">{{$dep->status}}</td>
                                    <td class="text-right">

                                        {{--<div class="btn-group btn-space">--}}
                                            {{--<button type="button" data-toggle="dropdown" class="btn btn-primary">Actions <span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>--}}
                                            {{--<ul role="menu" class="dropdown-menu">--}}
                                                {{--<li><a data-modal="full-danger-edit-{{$dep->id}}" class="md-trigger" href="#">{{trans('lang.btn_edit')}}</a></li>--}}
                                                {{--<li><a data-modal="full-danger-{{$dep->id}}" class="md-trigger" href="#">{{trans('lang.btn_delete')}}</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}

                                        <div class="btn-group btn-space">
                                            <a href="/accounting/deposit/verify/{{$dep->id}}"><button type="button" class="btn btn-success">ตรวจสอบข้อมูล</button></a>
                                        </div>

                                    </td>
                                </tr>

                                <?php $i++;?>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="tools"></div>
                        <div class="title">ข้อมูลการถอนเงิน</div>
                    </div>
                    <div class="panel-body">

                        <table id="table-withdrawal" class="table table-striped table-hover table-fw-widget">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>เข้าธนาคาร</th>
                                <th>จำนวนเงิน</th>
                                <th>วัน/เวลาที่แจ้ง</th>
                                <th>สถานะ</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0?>
                            @foreach($withdrawal as $dep)

                                <tr>
                                    <td class="">{{$i+1}}</td>
                                    <td class="">{{$dep->bank}}</td>
                                    <td class="">{{$dep->amount}}</td>
                                    <td class="">{{$dep->date_time}}</td>
                                    <td class="">{{$dep->status}}</td>
                                    <td class="text-right">

                                        {{--<div class="btn-group btn-space">--}}
                                            {{--<button type="button" data-toggle="dropdown" class="btn btn-primary">Actions <span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>--}}
                                            {{--<ul role="menu" class="dropdown-menu">--}}
                                                {{--<li><a data-modal="full-danger-edit-{{$dep->id}}" class="md-trigger" href="#">{{trans('lang.btn_edit')}}</a></li>--}}
                                                {{--<li><a data-modal="full-danger-{{$dep->id}}" class="md-trigger" href="#">{{trans('lang.btn_delete')}}</a></li>--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}

                                        <div class="btn-group btn-space">
                                            <a href="/accounting/withdrawal/verify/{{$dep->id}}"><button type="button" class="btn btn-success">ตรวจสอบข้อมูล</button></a>
                                        </div>

                                    </td>
                                </tr>

                                <?php $i++;?>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>

    <!-- Nifty Modal add new bank-->
        <div id="deposit" class="md-modal modal-container modal-colored-header md-effect-8">
            <div class="md-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                    <h3 class="modal-title">{{trans('lang.add_deposit')}}</h3>
                    <hr>
                </div>
                <form action="/users/wallet/deposit" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">{{trans('lang.deposit_to')}}</label>



                        </div>

                        <div class="form-group">
                            <label>จำนวนเงิน</label>
                            <input type="number" name="amount"  placeholder="จำนวน" value="" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>แนบ slip</label>
                            <input type="file" name="slip"  placeholder="Slip" value="" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>วันเวลาที่โอน</label>
                            <input type="text" name="date_time"  placeholder="วันเวลาที่โอน" value="" class="form-control datetimepicker">
                        </div>

                        <div class="form-group">
                            <label>หมายเหตุ</label>
                            <textarea name="note" id="" cols="30" rows="3" class="form-control"></textarea>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                        <button type="submit" class="btn btn-success btn-primary">{{trans('lang.btn_confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>


    </div>


@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
            $('.md-trigger').modalEffects();

            $('#table-deposit').dataTable( {

            } );
            $('#table-withdrawal').dataTable( {

            } );

        });
    </script>

@endsection