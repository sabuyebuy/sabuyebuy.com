@extends('ui-backend/partials.master')



@section('content')

    @include('ui-backend/users/header')

    <div class="main-content">

        @include('errors.error')
        <div class="row">

            @include('ui-backend/users/sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="tools"><a data-modal="add-favorite-shop" class="md-trigger" href="#"><button type="link" class="btn btn-space btn-primary" data-step="4" data-intro="Add your address" data-position='right'>{{trans('lang.btn_add_new')}}</button></a></div>
                        <div class="title">{{trans('lang.profile_header_favorites')}}</div>
                    </div>
                    <div class="panel-body">
                        <table id="table-fav-shop" class="table table-striped table-hover table-fw-widget">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>{{trans('lang.fav_shop_name')}}</th>
                                <th>{{trans('lang.fav_shop_url')}}</th>
                                <th>{{trans('lang.fav_shop_note')}}</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0?>
                            @foreach($favorites as $fav)

                                <tr>
                                    <td class="">{{$i+1}}</td>
                                    <td class="">{{$fav->shop_name}}</td>
                                    <td class=""><a href="{{$fav->shop_url}}" target="_blank">{{$fav->shop_url}}</a></td>
                                    <td class="">{{$fav->shop_note}}</td>
                                    <td class="text-right">

                                        <div class="btn-group btn-space">
                                            <button type="button" data-toggle="dropdown" class="btn btn-primary">Actions <span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a data-modal="full-danger-edit-{{$fav->id}}" class="md-trigger" href="#">{{trans('lang.btn_edit')}}</a></li>
                                                <li><a data-modal="full-danger-{{$fav->id}}" class="md-trigger" href="#">{{trans('lang.btn_delete')}}</a></li>
                                            </ul>
                                        </div>

                                        <!-- Nifty Modal delete-->
                                        <div id="full-danger-{{$fav->id}}" class="md-modal full-color danger md-effect-8">
                                            <div class="md-content">
                                                <div class="modal-header">
                                                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                                                    <h3 class="modal-title">{{trans('lang.btn_alert_danger')}} !</h3>
                                                </div>
                                                <form action="/users/delete-fav-shop/{{$fav->id}}" method="POST">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    <input type="hidden" name="_method" value="DELETE" />
                                                    <div class="modal-body">
                                                        <div class="text-center">
                                                            <div class="i-circle success"><i class="icon s7-check"></i></div>
                                                            <p>{{trans('lang.word_alert_danger')}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                                                        <button type="submit" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_confirm')}}</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </td>
                                </tr>

                                <?php $i++;?>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>




        <!-- Nifty Modal add new Favorite shop-->
        <div id="add-favorite-shop" class="md-modal modal-container modal-colored-header md-effect-8">
            <div class="md-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                    <h3 class="modal-title">Add new favorite shop</h3>
                    <hr>
                </div>
                <form action="/users/add-fav-shop" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Shop name</label>
                            <input type="text" name="shop_name"  placeholder="Shop name" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Shop URL</label>
                            <input type="text" name="shop_url"  placeholder="URL" value="" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>note</label>
                            <textarea name="note" id="" cols="30" rows="3" class="form-control"></textarea>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                        <button type="submit" class="btn btn-success btn-primary">{{trans('lang.btn_confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    </div>




@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
//            App.dataTables();
            $('.md-trigger').modalEffects();

            $("#table-fav-shop").dataTable();
        });
    </script>

@endsection