<div class="page-head">
    <h2>
        <span class="pull-right text-primary account-balance"> {{trans('lang.account_balance')}} {{number_format($user->user_amount_balance,2)}}  {{trans('lang.thb_currency')}}</span>
        {{trans('lang.greeting')}} <?php echo  strtoupper($user->name)?>  {{$user->user_code}}
    </h2>
    <ol class="breadcrumb">
        <li><a href="/users/profile">{{trans('lang.menu_home')}}</a></li>
        <li><a href="#">{{trans('lang.menu_profile')}}</a></li>
    </ol>

</div>

