@extends('ui-backend/partials.master')



@section('content')

    @include('ui-backend/users/header')

    <div class="main-content">

        @include('errors.error')
        <div class="row">

            @include('ui-backend/users/sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="tools"><a data-modal="add-address" class="md-trigger" href="#"><button type="link" class="btn btn-space btn-primary" data-step="4" data-intro="Add your address" data-position='right'>{{trans('lang.btn_add_new')}}</button></a></div>
                        <div class="title">{{trans('lang.profile_header_address')}}</div>
                    </div>
                    <div class="panel-body">
                        <table id="table-fav-shop" class="table table-striped table-hover table-fw-widget">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>{{trans('lang.user_receiver_name')}}</th>
                                <th>{{trans('lang.user_receiver_address')}}</th>
                                <th>{{trans('lang.user_receiver_district')}}</th>
                                <th>{{trans('lang.user_receiver_province')}}</th>
                                <th>{{trans('lang.user_receiver_tel')}}</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0?>
                            @foreach($address as $add)

                                <tr>
                                    <td class="">{{$i+1}}</td>
                                    <td class="">{{$add->receiver_name}}</td>
                                    <td class="">{{$add->receiver_address}}</td>
                                    <td class="">{{$add->receiver_district}}</td>
                                    <td class="">{{$add->receiver_province}}</td>
                                    <td class="">{{$add->receiver_tel}}</td>
                                    <td class="text-right">

                                        <div class="btn-group btn-space">
                                            <button type="button" data-toggle="dropdown" class="btn btn-primary">Actions <span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a data-modal="full-danger-edit-{{$add->id}}" class="md-trigger" href="#">{{trans('lang.btn_edit')}}</a></li>
                                                <li><a data-modal="full-danger-{{$add->id}}" class="md-trigger" href="#">{{trans('lang.btn_delete')}}</a></li>
                                            </ul>
                                        </div>

                                        <!-- Nifty Modal delete-->
                                        <div id="full-danger-{{$add->id}}" class="md-modal full-color danger md-effect-8">
                                            <div class="md-content">
                                                <div class="modal-header">
                                                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                                                    <h3 class="modal-title">{{trans('lang.btn_alert_danger')}} !</h3>
                                                </div>
                                                <form action="/users/delete-address/{{$add->id}}" method="POST">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    <input type="hidden" name="_method" value="DELETE" />
                                                    <div class="modal-body">
                                                        <div class="text-center">
                                                            <div class="i-circle success"><i class="icon s7-check"></i></div>
                                                            <p>{{trans('lang.word_alert_danger')}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                                                        <button type="submit" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_confirm')}}</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </td>
                                </tr>

                                <?php $i++;?>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>




        <!-- Nifty Modal add new address-->
        <div id="add-address" class="md-modal modal-container modal-colored-header md-effect-8">
            <div class="md-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                    <h3 class="modal-title">Add new address</h3>
                    <hr>
                </div>
                <form action="/users/add-address" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Recipient</label>
                            <input type="text" name="receiver_name"  placeholder="Recipient" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" name="receiver_address"  placeholder="Address" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>District</label>
                            <input type="text" name="receiver_district"  placeholder="District" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Province</label>
                            <input type="text" name="receiver_province"  placeholder="Province" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Zip</label>
                            <input type="text" name="receiver_zip_code"  placeholder="Zip" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Tel</label>
                            <input type="text" name="receiver_tel"  placeholder="Tel" value="" class="form-control">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                        <button type="submit" class="btn btn-success btn-primary">{{trans('lang.btn_confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    </div>




@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
//            App.dataTables();
            $('.md-trigger').modalEffects();

            $("#table-fav-shop").dataTable();
        });
    </script>

@endsection