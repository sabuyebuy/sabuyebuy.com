@extends('ui-backend/partials.master')



@section('content')

    @include('ui-backend/users/header')

    <div class="main-content">

        @include('errors.error')
        <div class="row">
            @include('ui-backend/users/sidebar')
            <div class="col-md-9">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="tools"><a class="btn btn-large btn-success" href="javascript:void(0);" onclick="javascript:introJs().start();">How to use ?</a></div>
                        <div class="title">{{trans('lang.menu_profile')}}</div>
                    </div>
                    <div class="panel-body">

                        <form  role="form" method="POST" action="/users/update-profile/{{$user->id}}" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <input type="hidden" name="_method" value="PUT" />

                            <div data-step="1" data-intro="Update Your profile" data-position='left'>
                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label>Full name</label>
                                            <input type="text" name="name"  placeholder="UserID" value="{{$user->name}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Email address</label>
                                            <input type="email" name="email" disabled="disabled" placeholder="Enter email" value="{{$user->email}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>User ID</label>
                                            <input type="text" name="user_code" disabled="disabled" placeholder="UserID" value="{{$user->user_code}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label class="">Gender</label>

                                            <div class="am-radio inline">
                                                <input type="radio" @if($user->gender == 1) checked="checked" @endif value="1" name="gender" id="male">
                                                <label for="male">Male</label>
                                            </div>

                                            <div class="am-radio inline">
                                                <input type="radio" @if($user->gender == 0) checked="checked" @endif  value="0" name="gender" id="female">
                                                <label for="female">Female</label>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class=""> Birthday </label>

                                            <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                                                <input id="birth_date" name="birthday_date" size="16" type="text" value="{{$user->birthday_date}}" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th s7-date"></i></span>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-md-6">


                                        <div class="form-group">
                                            <label>Website</label>
                                            <input type="text" name="owner_shop"  placeholder="web site" value="{{$user->owner_shop}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Mobile</label>
                                            <input type="text" name="contact_phone1"  placeholder="Mobile" value="{{$user->contact_phone1}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Home phone</label>
                                            <input type="text" name="contact_phone2"  placeholder="Home phone" value="{{$user->contact_phone2}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Line ID</label>
                                            <input type="text" name="contact_line"  placeholder="Line ID" value="{{$user->contact_line}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Avatar</label>
                                            <input type="file" name="user_image"  placeholder="Image" value="{{$user->user_image}}" class="form-control">
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="spacer text-right">
                                <button type="submit" class="btn btn-space btn-primary">Update</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>



        <div class="row">

            <!-- Nifty Modal add new address-->
            <div id="add-address" class="md-modal modal-container modal-colored-header md-effect-8">
                <div class="md-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                        <h3 class="modal-title">Add new address</h3>
                        <hr>
                    </div>
                    <form action="/users/add-address" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Recipient</label>
                                <input type="text" name="receiver_name"  placeholder="Recipient" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" name="receiver_address"  placeholder="Address" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>District</label>
                                <input type="text" name="receiver_district"  placeholder="District" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Province</label>
                                <input type="text" name="receiver_province"  placeholder="Province" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Zip</label>
                                <input type="text" name="receiver_zip_code"  placeholder="Zip" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Tel</label>
                                <input type="text" name="receiver_tel"  placeholder="Tel" value="" class="form-control">
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                            <button type="submit" class="btn btn-success btn-primary">{{trans('lang.btn_confirm')}}</button>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Nifty Modal add new bank-->
            <div id="add-bank" class="md-modal modal-container modal-colored-header md-effect-8">
                <div class="md-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                        <h3 class="modal-title">Add new bank</h3>
                        <hr>
                    </div>
                    <form action="/users/add-bank" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Bank code</label>
                                <input type="text" name="bank_code"  placeholder="eg. BBL,BAY,KBANK,KTC" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Account number</label>
                                <input type="text" name="account_number"  placeholder="Account number" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Account name</label>
                                <input type="text" name="account_name"  placeholder="Account name" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Account branch</label>
                                <input type="text" name="account_branch"  placeholder="Account branch" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>note</label>
                                <textarea name="note" id="" cols="30" rows="3" class="form-control"></textarea>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                            <button type="submit" class="btn btn-success btn-primary">{{trans('lang.btn_confirm')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>




@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
//            App.dataTables();
            $('.md-trigger').modalEffects();

            $("#table-fav-shop").dataTable();
        });
    </script>

@endsection