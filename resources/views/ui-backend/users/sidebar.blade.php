<?php $url = Route::getFacadeRoot()->current()->uri()?>

<div class="col-md-3">
    <div class="panel panel-default">

        <div class="panel-heading">
            {{--<div class="tools"><a class="btn btn-large btn-success" href="javascript:void(0);" onclick="javascript:introJs().start();">How to use ?</a></div>--}}
            <div class="title">{{trans('lang.menu_profile')}}</div>
        </div>
        <div class="list-group">

            <a href="/users/profile" class="list-group-item @if($url === 'users/profile') active @endif">{{trans('lang.menu_profile')}}</a>
            <a href="/users/address" class="list-group-item @if($url === 'users/address') active @endif">{{trans('lang.sidebar_address')}}</a>
            <a href="/users/banks" class="list-group-item @if($url === 'users/banks') active @endif">{{trans('lang.sidebar_banks')}}</a>
            <a href="/users/favorites" class="list-group-item @if($url === 'users/favorites') active @endif">{{trans('lang.profile_header_favorites')}}</a>
        </div>
    </div>
</div>