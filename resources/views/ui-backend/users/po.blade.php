@extends('ui-backend/partials.master')



@section('content')

    @include('ui-backend/users/header')

    <div class="main-content">

        @include('errors.error')
        <div class="row">

            {{--@include('ui-backend/users/sidebar')--}}

            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        {{--<div class="tools"><a data-modal="add-address" class="md-trigger" href="#"><button type="link" class="btn btn-space btn-primary" data-step="4" data-intro="Add your address" data-position='right'>{{trans('lang.btn_add_new')}}</button></a></div>--}}
                        <div class="title">ข้อมูลการสั่งซื้อ</div>
                    </div>
                    <div class="panel-body">
                        <table id="table-fav-shop" class="table table-striped table-hover table-fw-widget">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>หมายเลขการสั่งซื้อ</th>
                                <th>อยู่ระหว่างการ</th>
                                <th>สถาณะบิล</th>
                                <th>วันที่สั่งซื้อ</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0?>
                            @foreach($orders as $po)

                                <tr>
                                    <td class="">{{$i+1}}</td>
                                    <td class="">{{$po->po_id}}</td>
                                    <td class="">{{$po->po_step}}</td>
                                    <td class="">{{$po->po_status}}</td>
                                    <td class="">{{$po->created_at}}</td>
                                    <td class="text-right">
                                        <a href="/users/po/show/{{$po->po_id}}"> <button type="button"class="btn btn-primary">ดูข้อมูล</button></a>
                                    </td>
                                </tr>

                                <?php $i++;?>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>




        <!-- Nifty Modal add new address-->
        <div id="add-address" class="md-modal modal-container modal-colored-header md-effect-8">
            <div class="md-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                    <h3 class="modal-title">Add new address</h3>
                    <hr>
                </div>
                <form action="/users/add-address" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Recipient</label>
                            <input type="text" name="receiver_name"  placeholder="Recipient" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" name="receiver_address"  placeholder="Address" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>District</label>
                            <input type="text" name="receiver_district"  placeholder="District" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Province</label>
                            <input type="text" name="receiver_province"  placeholder="Province" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Zip</label>
                            <input type="text" name="receiver_zip_code"  placeholder="Zip" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Tel</label>
                            <input type="text" name="receiver_tel"  placeholder="Tel" value="" class="form-control">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                        <button type="submit" class="btn btn-success btn-primary">{{trans('lang.btn_confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    </div>




@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
//            App.dataTables();
            $('.md-trigger').modalEffects();

            $("#table-fav-shop").dataTable();
        });
    </script>

@endsection