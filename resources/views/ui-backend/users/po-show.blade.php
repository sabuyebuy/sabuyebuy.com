@extends('ui-backend/partials.master')



@section('content')

    @include('ui-backend/users/header')

    <div class="main-content">




        @include('errors.error')
        <div class="row">

            {{--@include('ui-backend/users/sidebar')--}}


            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        {{--<div class="tools"><a data-modal="add-address" class="md-trigger" href="#"><button type="link" class="btn btn-space btn-primary" data-step="4" data-intro="Add your address" data-position='right'>{{trans('lang.btn_add_new')}}</button></a></div>--}}
                        <div class="title">ข้อมูลการสั่งซื้อ หมายเลข : {{$po->po_id}}</div>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="wizard">
                                <div class="wizard-inner">
                                    <div class="connecting-line"></div>
                                    <ul class="nav nav-tabs" role="tablist">

                                        <li role="presentation" class="disabled @if($po->po_step == 1) active @endif">
                                            <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="ตรวจสอบ/ชำระเงิน">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-eye-open"></i>
                                            </span>
                                            </a>

                                        </li>

                                        <li role="presentation" class="disabled  @if($po->po_step == 2) active @endif">
                                            <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="กำลังสั่งซื้อ">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-eur"></i>
                                            </span>
                                            </a>
                                        </li>

                                        <li role="presentation" class="disabled @if($po->po_step == 3) active @endif">
                                            <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="รอร้านค้าจัดส่ง">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-shopping-cart"></i>
                                            </span>
                                            </a>
                                        </li>

                                        <li role="presentation" class="disabled @if($po->po_step == 4) active @endif">
                                            <a href="#step4" data-toggle="tab" aria-controls="step3" role="tab" title="รอเข้าโกดังไทย">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-plane"></i>
                                            </span>
                                            </a>
                                        </li>


                                        <li role="presentation" class="disabled @if($po->po_step == 5) active @endif">
                                            <a href="#step5" data-toggle="tab" aria-controls="step3" role="tab" title="ระหว่างการออกบิลขนส่ง">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-open-file"></i>
                                            </span>
                                            </a>
                                        </li>



                                        <li role="presentation" class="disabled @if($po->po_step == 6) active @endif">
                                            <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="ปิด order">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-ok"></i>
                                            </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>

                        <h4>สินค้า</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <td></td>
                                    <td class="text-center">ชื่อสินค้า </td>
                                    <td class="text-center">ราคา</td>
                                    <td class="text-center">จำนวน</td>
                                    <td class="text-center">ราคารวม</td>
                                    <td class="text-center">ข้อมูลเพิ่มติม</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td class="cart-td-img">
                                            <a href="{{$product->product_url}}" target="_blank">
                                                <img title="{{$product->product_title}}" alt="" src="{{$product->product_image_url}}" width="50" height="50"></a>
                                        </td>
                                        <td><a href="">{{$product->product_title}}</a></td>
                                        <td class="text-center">{{number_format($product->price)}}</td>
                                        <td class="text-center">{{$product->quantity}}</td>
                                        <td class="cart-total-price text-center">{{number_format($product->price * $product->quantity)}}  ¥</td>
                                        <td class="cart-total-price text-center"><small>{{$product->note}}</small></td>
                                    </tr>

                                    <input type="hidden" name="products_id[]" value="{{$product->id}}" />

                                @endforeach

                                </tbody>
                            </table>

                        </div>

                        <hr>

                        <div class="panel panel-danger">
                            <div class="panel-heading font-14"><strong><i class="glyphicon glyphicon-list-alt"></i> สรุปยอดค่าใช้จ่าย</strong></div>
                            <div class="panel-body">
                                <input type="hidden" name="po" id="po" value="TA165354">
                                <input type="hidden" name="ciid" id="iid" value="38144">
                                <table class="table table-striped table-hover mg-bottom-0">
                                    <tbody>
                                    <tr>
                                        <td class="col-md-5 text-center"><strong>รายการสรุปยอดค่าใช้จ่าย</strong></td>
                                        <td class="col-md-2 text-right">จำนวน</td>
                                        <td class="col-md-1">&nbsp;</td>
                                        <td class="col-md-2"><strong>หน่วย</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong>จำนวนรายการ :</strong></td>
                                        <td class="text-right text-red">1</td>
                                        <td></td>
                                        <td>รายการ</td>
                                    </tr>
                                    <tr>
                                        <td><strong>จำนวนชิ้น :</strong></td>
                                        <td class="text-right text-red">{{$dataSum['productTotal']}}</td>
                                        <td></td>
                                        <td>ชิ้น</td>
                                    </tr>
                                    <tr>
                                        <td><strong>ค่าสินค้า :</strong></td>
                                        <td class="text-right text-red">{{number_format($total)}}</td>
                                        <td></td>
                                        <td>หยวน</td>
                                    </tr>
                                    <tr>
                                        <td><strong>ค่าหิ้ว :</strong></td>
                                        <td class="text-right text-red">{{number_format($dataSum['order_fee'])}}</td>
                                        <td></td>
                                        <td>หยวน</td>
                                    </tr>
                                    <tr>
                                        <td><strong>รวม (ค่าสินค้า+ค่าขนส่งในจีน+ค่าหิ้ว) :</strong></td>
                                        <td class="text-right text-red">{{number_format($dataSum['totalCost'])}}</td>
                                        <td></td>
                                        <td>หยวน</td>
                                    </tr>
                                    <tr>
                                        <td><strong>เรท (อัตราแลกเปลี่ยน) :</strong></td>
                                        <td class="text-right text-red">{{$po->exchange_rate}}</td>
                                        <td></td>
                                        <td>บาท / หยวน</td>
                                    </tr>
                                    <tr>
                                        <th><strong>รวม (ค่าสินค้า+ค่าขนส่งในจีน+ค่าหิ้ว) x เรท :</strong></th>
                                        <th class="text-right text-red">{{number_format($dataSum['totalCost'])}}</th>
                                        <td></td>
                                        <th>บาท</th>
                                    </tr>
                                    <tr class="success">
                                        <td class="text-center"><strong class="">รวมยอดเงินที่ต้องชำระ :</strong></td>
                                        <td class="text-right text-red font-14"><strong>{{number_format($dataSum['totalCostWithRate'])}}</strong></td>
                                        <td></td>
                                        <td><strong class="font-14">บาท</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5"></td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>


                        <div class="panel panel-success">
                            <div class="panel-heading font-14"><strong><i class="glyphicon glyphicon-list-alt"></i> สรุปยอดค่าใช้จ่าย (เพิ่มเติม)</strong></div>
                            <div class="panel-body">
                                <table class="table table-striped table-hover mg-bottom-0">
                                    <tbody>
                                    <tr>
                                        <td class="col-md-5 text-center"><strong>ค่าใช้จ่ายเพิ่มเติม</strong></td>
                                        <td class="col-md-2 text-right">จำนวน</td>
                                        <td class="col-md-1">&nbsp;</td>
                                        <td class="col-md-2"><strong>หน่วย</strong></td>
                                    </tr>



                                    <tr>
                                        <td><strong>ค่าขนส่งในจีน :</strong></td>
                                        <td class="text-right text-red">{{$po->payment_add_ship_in_cn}}</td>
                                        <td></td>
                                        <td>บาท</td>
                                    </tr>

                                    <tr>
                                        <td><strong>ค่าบริการเพิ่มเติมกรณีสินค้าพิเศษ : </strong></td>
                                        <td class="text-right text-red">{{$po->payment_add_buy_for_other}}</td>
                                        <td></td>
                                        <td>บาท</td>
                                    </tr>
                                    <tr>
                                        <td><strong>ค่าขนส่งในไทย : </strong></td>
                                        <td class="text-right text-red">{{$po->payment_add_ship_in_th}}</td>
                                        <td></td>
                                        <td>บาท</td>
                                    </tr>
                                    <tr>
                                        <td><strong>ค่าใช้จ่ายอื่นๆ : </strong></td>
                                        <td class="text-right text-red">{{$po->payment_add_ship_in_th}}</td>
                                        <td></td>
                                        <td>บาท</td>
                                    </tr>
                                    <tr>
                                        <td><strong>ส่วนลดพิเศษ : </strong></td>
                                        <td class="text-right text-red">{{$po->discount}}</td>
                                        <td></td>
                                        <td>บาท</td>
                                    </tr>

                                    <tr>
                                        <td colspan="5"></td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>




@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
//            App.dataTables();
            $('.md-trigger').modalEffects();

            $("#table-fav-shop").dataTable();
        });
    </script>

@endsection