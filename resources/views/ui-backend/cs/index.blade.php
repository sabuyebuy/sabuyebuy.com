@extends('ui-backend/partials.master')



@section('content')

    {{--@include('ui-backend/wallet/deposit/header')--}}

    <div class="main-content">

        @include('errors.error')
        <div class="row">
            {{--@include('ui-backend/users/sidebar')--}}
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="title">รายการสั่งซื้อ</div>
                    </div>
                    <div class="panel-body">

                        <table id="table-deposit" class="table table-striped table-hover table-fw-widget">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>รหัสลูกค้า</th>
                                <th>หมายเลขการสั่งซื้อ</th>
                                <th>สถานะ</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0?>
                            @foreach($orders as $order)

                                <tr>
                                    <td class="">{{$i+1}}</td>
                                    <td class="">{{$order['user_code']}}</td>
                                    <td class="">{{$order['po_id']}}</td>
                                    <td class="">{{$order['po_step']}}</td>
                                    <td class="text-right">
                                        <div class="btn-group btn-space">
                                            <a href="/cs/response-order/{{$order['id']}}"><button type="button" class="btn btn-success">ดำเนินการ</button></a>
                                        </div>

                                    </td>
                                </tr>

                                <?php $i++;?>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>



    </div>


@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
            $('.md-trigger').modalEffects();

            $('#table-deposit').dataTable( {

            } );

        });
    </script>

@endsection