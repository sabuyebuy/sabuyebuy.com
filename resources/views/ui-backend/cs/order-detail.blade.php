@extends('ui-backend/partials.master')



@section('content')

    {{--@include('ui-backend/users/header')--}}

    <div class="main-content">




        @include('errors.error')
        <div class="row">

            {{--@include('ui-backend/users/sidebar')--}}


            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="tools">
                            <a data-modal="add-address" class="md-trigger" href="#">
                                <button type="link" class="btn btn-space btn-success" data-step="4" data-intro="Add your address" data-position='right'> ยอดเงินคงเหลือของผู้สั่งซื้อ : {{number_format($user->user_amount_balance)}} บาท</button>
                                @if($user->user_amount_balance < $dataSum['totalCostWithRate'])
                                <button type="link" class="btn btn-space btn-primary" data-step="4" data-intro="Add your address" data-position='right'> ตัดเงินค่าสินค้าและค่าหิ้ว : {{number_format($dataSum['totalCostWithRate'])}} บาท</button>
                                    @else
                                    <button type="link" class="btn btn-space btn-primary" data-step="4" data-intro="Add your address" data-position='right'> ยอดเงินคงเหลือไม่เพียงพอ</button>
                                @endif
                            </a>
                        </div>
                        <div class="title">ข้อมูลการสั่งซื้อ หมายเลข : {{$po->po_id}}</div>
                    </div>
                    <div class="panel-body">
                        <h4>สินค้า</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <td></td>
                                    <td class="text-center">ชื่อสินค้า </td>
                                    <td class="text-center">ราคา</td>
                                    <td class="text-center">จำนวน</td>
                                    <td class="text-center">ราคารวม</td>
                                    <td class="text-center">ข้อมูลเพิ่มติม</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td class="cart-td-img">
                                            <a href="{{$product->product_url}}" target="_blank">
                                                <img title="{{$product->product_title}}" alt="" src="{{$product->product_image_url}}" width="50" height="50"></a>
                                        </td>
                                        <td><a href="">{{$product->product_title}}</a></td>
                                        <td class="text-center">{{number_format($product->price)}}</td>
                                        <td class="text-center">{{$product->quantity}}</td>
                                        <td class="cart-total-price text-center">{{number_format($product->price * $product->quantity)}}  ¥</td>
                                        <td class="cart-total-price text-center"><small>{{$product->note}}</small></td>
                                    </tr>

                                    <input type="hidden" name="products_id[]" value="{{$product->id}}" />

                                @endforeach

                                </tbody>
                            </table>

                        </div>

                        <hr>

                        <div class="panel panel-danger">
                            <div class="panel-heading font-14"><strong><i class="glyphicon glyphicon-list-alt"></i> สรุปยอดค่าใช้จ่าย</strong></div>
                            <div class="panel-body">
                                <table class="table table-striped table-hover mg-bottom-0">
                                    <tbody>
                                    <tr>
                                        <td class="col-md-5 text-center"><strong>รายการสรุปยอดค่าใช้จ่าย</strong></td>
                                        <td class="col-md-2 text-right">จำนวน</td>
                                        <td class="col-md-1">&nbsp;</td>
                                        <td class="col-md-2"><strong>หน่วย</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong>จำนวนรายการ :</strong></td>
                                        <td class="text-right text-red">1</td>
                                        <td></td>
                                        <td>รายการ</td>
                                    </tr>
                                    <tr>
                                        <td><strong>จำนวนชิ้น :</strong></td>
                                        <td class="text-right text-red">{{$dataSum['productTotal']}}</td>
                                        <td></td>
                                        <td>ชิ้น</td>
                                    </tr>
                                    <tr>
                                        <td><strong>ค่าสินค้า :</strong></td>
                                        <td class="text-right text-red">{{number_format($total)}}</td>
                                        <td></td>
                                        <td>หยวน</td>
                                    </tr>
                                    <tr>
                                        <td><strong>ค่าหิ้ว :</strong></td>
                                        <td class="text-right text-red">{{number_format($dataSum['order_fee'])}}</td>
                                        <td></td>
                                        <td>หยวน</td>
                                    </tr>
                                    <tr>
                                        <td><strong>รวม (ค่าสินค้า+ค่าขนส่งในจีน+ค่าหิ้ว) :</strong></td>
                                        <td class="text-right text-red">{{number_format($dataSum['totalCost'])}}</td>
                                        <td></td>
                                        <td>หยวน</td>
                                    </tr>
                                    <tr>
                                        <td><strong>เรท (อัตราแลกเปลี่ยน) :</strong></td>
                                        <td class="text-right text-red">{{$po->exchange_rate}}</td>
                                        <td></td>
                                        <td>บาท / หยวน</td>
                                    </tr>
                                    <tr>
                                        <th><strong>รวม (ค่าสินค้า+ค่าขนส่งในจีน+ค่าหิ้ว) x เรท :</strong></th>
                                        <th class="text-right text-red">{{number_format($dataSum['totalCostWithRate'])}}</th>
                                        <td></td>
                                        <th>บาท</th>
                                    </tr>

                                    <tr class="success">
                                        <td class="text-center"><strong class="">รวมยอดเงินที่ต้องชำระ :</strong></td>
                                        <td class="text-right text-red font-14"><strong>{{number_format($dataSum['totalCostWithRate'])}}</strong></td>
                                        <td></td>
                                        <td><strong class="font-14">บาท</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5"></td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>

                        <div class="panel-heading">
                            <div class="tools">
                                <a data-modal="add-address" class="md-trigger" href="#">
                                    <button type="link" class="btn btn-space btn-success" data-step="4" data-intro="Add your address" data-position='right'> ยอดเงินคงเหลือของผู้สั่งซื้อ : {{number_format($user->user_amount_balance)}} บาท</button>
                                    @if($user->user_amount_balance < $dataSum['totalCostWithRate'])
                                        <button type="link" class="btn btn-space btn-primary" data-step="4" data-intro="Add your address" data-position='right'> ตัดเงินค่าสินค้าและค่าหิ้ว : {{number_format($dataSum['totalCostWithRate'])}} บาท</button>
                                    @else
                                        <button type="link" class="btn btn-space btn-primary" data-step="4" data-intro="Add your address" data-position='right'> ยอดเงินคงเหลือไม่เพียงพอ</button>
                                    @endif
                                </a>
                            </div>
                            <div class="title">.</div>
                        </div>

                        <div class="panel panel-success" id="app">
                            <div class="panel-heading font-14"><strong><i class="glyphicon glyphicon-list-alt"></i> สรุปยอดค่าใช้จ่าย (เพิ่มเติม)</strong></div>
                            <div class="panel-body">
                                <table class="table table-striped table-hover mg-bottom-0">
                                    <tbody>
                                    <tr>
                                        <td class="col-md-5 text-center"><strong>ค่าใช้จ่ายเพิ่มเติม</strong></td>
                                        <td class="col-md-2 text-right">จำนวน</td>
                                        <td class="col-md-1">&nbsp;</td>
                                        <td class="col-md-2"><strong>หน่วย</strong></td>
                                    </tr>



                                    <tr>
                                        <td><strong>ค่าขนส่งในจีน :</strong></td>
                                        <td class="text-right text-red"><input type="number" name="payment_add_ship_in_cn" value="{{$po->payment_add_ship_in_cn}}"></td>
                                        <td></td>
                                        <td>บาท</td>
                                    </tr>

                                    <tr>
                                        <td><strong>ค่าบริการเพิ่มเติมกรณีสินค้าพิเศษ : </strong></td>
                                        <td class="text-right text-red"><input type="number" name="payment_add_buy_fee" value="{{$po->payment_add_buy_fee}}"></td>
                                        <td></td>
                                        <td>บาท</td>
                                    </tr>
                                    <tr>
                                        <td><strong>ค่าขนส่งในไทย : </strong></td>
                                        <td class="text-right text-red"><input type="number" name="payment_add_ship_in_th" value="{{$po->payment_add_ship_in_th}}"></td>
                                        <td></td>
                                        <td>บาท</td>
                                    </tr>
                                    <tr>
                                        <td><strong>ค่าใช้จ่ายอื่นๆ : </strong></td>
                                        <td class="text-right text-red"><input type="number" name="payment_add_buy_for_other" value="{{$po->payment_add_buy_for_other}}"></td>
                                        <td></td>
                                        <td>บาท</td>
                                    </tr>
                                    <tr>
                                        <td><strong>ส่วนลดพิเศษ : </strong></td>
                                        <td class="text-right text-red"><input type="number" name="discount" value="{{$po->discount}}"></td>
                                        <td></td>
                                        <td>บาท</td>
                                    </tr>

                                    <tr class="" >
                                        <td class="text-center"><strong class="">รวมยอดเงินที่ต้องชำระเพิ่มเติม :</strong></td>
                                        <td class="text-right text-red font-14"><strong>{{$totalAddPay}}</strong></td>
                                        <td></td>
                                        <td><strong class="font-14">บาท</strong></td>
                                    </tr>

                                    <tr>
                                        <td colspan="5"></td>
                                    </tr>
                                    </tbody>
                                </table>

                                <button type="button" class="btn btn-space btn-success pull-right" data-step="4" data-intro="Add your address" data-position='right'>Update ข้อมูล</button>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection

@section('scripts')

    <script src="https://unpkg.com/vue"></script>

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();

        });
    </script>

    <script>
        var app = new Vue({
            el: '#app',
            data: {

            },
            computed: {
                result: function(){
                    var payment_add_ship_in_cn = this.payment_add_ship_in_cn;
                    var payment_add_ship_in_th = this.payment_add_ship_in_th;
                    var payment_add_buy_fee = this.payment_add_buy_fee;
                    var payment_add_buy_for_other = this.payment_add_buy_for_other;
                    var discount = this.discount;

                    return this.result = (payment_add_ship_in_cn + payment_add_ship_in_th + payment_add_buy_fee + payment_add_buy_for_other) - discount;
                }
            }
        })
    </script>

@endsection