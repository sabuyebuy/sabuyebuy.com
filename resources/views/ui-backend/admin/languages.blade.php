@extends('ui-backend/partials.master')



@section('content')

    @include('ui-backend/admin/header')

    <div class="main-content">

        @include('errors.error')

        <div class="row">


            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="title">Languages</div>
                    </div>
                    <div class="panel-body">

                        <form class="form-horizontal" style="border-radius: 0px;" role="form" method="POST" action="{{url('admin/languages')}}" enctype="multipart/form-data" >
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />

                            @foreach($langData as $value)

                                <label class=" control-label">{{$value->key}}</label>

                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <input type="text" id="{{$value->value_th}}" name="{{$value->id}}[]" class="form-control" value="{{$value->value_th}}">
                                    </div>

                                    <div class="col-sm-4">
                                        <input type="text" id="{{$value->value_en}}" name="{{$value->id}}[]" class="form-control" value="{{$value->value_en}}">
                                    </div>

                                    <div class="col-sm-4">
                                        <input type="text" id="{{$value->value_cn}}" name="{{$value->id}}[]" class="form-control" value="{{$value->value_cn}}">
                                    </div>
                                </div>

                                <hr>


                            @endforeach


                            <div class="col-sm-6 col-md-offset-3">
                                <button type="submit" class="btn btn-block btn-primary btn-lg">{{trans('lang.btn_confirm')}}</button>
                            </div>


                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>


    <script src="{{asset('js/tinymce/tinymce.min.js')}}"></script>

@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
//            App.dataTables();
            $('.md-trigger').modalEffects();

            $("#table-fav-shop").dataTable();

            tinymce.init({
                selector:'textarea',
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools '
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',

                image_advtab: true,
                images_upload_base_path: '{{asset('/uploads/images/contents')}}',
                moxiemanager_image_settings : {
                    view : 'thumbs',
                    extensions : 'jpg,png,gif'
                }
            });
        });
    </script>

@endsection