@extends('ui-backend/partials.master')



@section('content')

    @include('ui-backend/admin/header')

    <div class="main-content">

        @include('errors.error')

        <div class="row">


            <div class="col-sm-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="title">อัตราแลกเปลี่ยน</div>
                    </div>
                    <div class="panel-body">

                        <form class="form-horizontal" style="border-radius: 0px;" role="form" method="POST" action="{{url('admin/exchange-rate')}}" enctype="multipart/form-data" >
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />

                            <div class="form-group">

                                <div class="col-sm-6">
                                    <label class=" control-label"> (บาท) ต่อ 1 ¥</label>
                                    <div class="input-group">
                                        <input type="text" name="thai_baht" value="{{$exchangeRate->thai_baht}}" class="form-control">
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">

                                <div class="col-sm-6">
                                    <label class=" control-label"> เริ่มใช้งาน</label>
                                    <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                                        <input type="text" name="start_date" value="{{$exchangeRate->start_date}}" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th s7-date"></i></span>
                                    </div>
                                </div>


                            </div>

                            <div class="form-group">
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-block btn-primary btn-lg">{{trans('lang.btn_confirm')}}</button>
                                </div>
                            </div>


                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>


    <script src="{{asset('js/tinymce/tinymce.min.js')}}"></script>

@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
//            App.dataTables();
            $('.md-trigger').modalEffects();

            $("#table-fav-shop").dataTable();

            tinymce.init({
                selector:'textarea',
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools '
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',

                image_advtab: true,
                images_upload_base_path: '{{asset('/uploads/images/contents')}}',
                moxiemanager_image_settings : {
                    view : 'thumbs',
                    extensions : 'jpg,png,gif'
                }
            });
        });
    </script>

@endsection