@extends('ui-backend/partials.master')



@section('content')

    @include('ui-backend/admin/header')

    <div class="main-content">

        @include('errors.error')

        <div class="row">

            @include('ui-backend/admin/cms/sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="title">Content</div>
                    </div>
                    <div class="panel-body">



                    </div>
                </div>
            </div>

        </div>
    </div>


    <script src="{{asset('js/tinymce/tinymce.min.js')}}"></script>

@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
//            App.dataTables();
            $('.md-trigger').modalEffects();

            $("#table-fav-shop").dataTable();

            tinymce.init({
                selector:'textarea',
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools '
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',

                image_advtab: true,
                images_upload_base_path: '{{asset('/uploads/images/contents')}}',
                moxiemanager_image_settings : {
                    view : 'thumbs',
                    extensions : 'jpg,png,gif'
                }
            });
        });
    </script>

@endsection