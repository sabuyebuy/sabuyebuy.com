@extends('ui-backend/partials.master')



@section('content')

    @include('ui-backend/admin/cms/header')

    <div class="main-content">

        @include('errors.error')

        <div class="row">

            @include('ui-backend/admin/cms/sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="title">Content</div>
                    </div>
                    <div class="panel-body">

                        <form  role="form" method="POST" action="/admin/cms/{{$content->id}}>" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <input type="hidden" name="_method" value="PUT" />


                            <div class="form-group">
                                <h5>Title English</h5>
                                <input type="text" name="title_en" value="{{$content->title_en}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <h5>Content Eng</h5>
                                <textarea name="body_en" id="body_en" cols="30" rows="15" class="form-control">{{$content->body_en}}</textarea>
                            </div>


                            <div class="form-group">
                                <h5>Title Thai</h5>
                                <input type="text" name="title_thai" value="{{$content->title_th}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <h5>Content Thai</h5>
                                <textarea name="body_th" id="body_th" cols="30" rows="15" class="form-control">{{$content->body_th}}</textarea>

                            </div>

                            <div class="form-group">
                                <h5>Title China</h5>
                                <input type="text" name="title_cn" value="{{$content->title_cn}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <h5>Content China</h5>
                                <textarea name="body_cn" id="body_cn" cols="30" rows="15" class="form-control">{{$content->body_cn}}</textarea>

                            </div>

                                <div class="spacer text-right">
                                    <button type="submit" class="btn btn-space btn-primary">Update</button>
                                </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>


    <script src="{{asset('js/tinymce/tinymce.min.js')}}"></script>

@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
//            App.dataTables();
            $('.md-trigger').modalEffects();

            $("#table-fav-shop").dataTable();

            tinymce.init({
                selector:'textarea',
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools '
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',

                image_advtab: true,
                images_upload_base_path: '{{asset('/uploads/images/contents')}}',
                moxiemanager_image_settings : {
                    view : 'thumbs',
                    extensions : 'jpg,png,gif'
                }
            });
        });
    </script>

@endsection