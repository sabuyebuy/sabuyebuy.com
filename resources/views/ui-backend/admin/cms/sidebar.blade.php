<?php  $url = Route::getFacadeRoot()->current()->uri()?>
<div class="col-md-3">
    <div class="panel panel-default">

        <div class="panel-heading">
            {{--<div class="tools"><a class="btn btn-large btn-success" href="javascript:void(0);" onclick="javascript:introJs().start();">How to use ?</a></div>--}}
            <div class="title">About us</div>
        </div>
        <div class="list-group">
            <a href="/admin/cms/page/1" class="list-group-item @if($url === 'users/profile') active @endif">{{trans('lang.cms_who_we_are')}}</a>
            <a href="/admin/cms/page/2" class="list-group-item @if($url === 'users/address') active @endif">{{trans('lang.cms_vision')}}</a>
            <a href="/admin/cms/page/3" class="list-group-item @if($url === 'users/banks') active @endif">{{trans('lang.cms_mission')}}</a>
            <a href="/admin/cms/page/4" class="list-group-item @if($url === 'users/favorites') active @endif">{{trans('lang.cms_core_competency')}}</a>
            <a href="/admin/cms/page/5" class="list-group-item @if($url === 'users/favorites') active @endif">{{trans('lang.cms_our_services')}}</a>
        </div>
    </div>


    <div class="panel panel-default">

        <div class="panel-heading">
            {{--<div class="tools"><a class="btn btn-large btn-success" href="javascript:void(0);" onclick="javascript:introJs().start();">How to use ?</a></div>--}}
            <div class="title">How to order</div>
        </div>
        <div class="list-group">
            <a href="/admin/cms/page/6" class="list-group-item @if($url === 'users/profile') active @endif">{{trans('lang.cms_how_to_order')}}</a>
        </div>
    </div>


    <div class="panel panel-default">

        <div class="panel-heading">
            {{--<div class="tools"><a class="btn btn-large btn-success" href="javascript:void(0);" onclick="javascript:introJs().start();">How to use ?</a></div>--}}
            <div class="title">Import</div>
        </div>
        <div class="list-group">
            <a href="/admin/cms/page/7" class="list-group-item @if($url === 'users/favorites') active @endif">{{trans('lang.cms_by_truck')}}</a>
            <a href="/admin/cms/page/8" class="list-group-item @if($url === 'users/favorites') active @endif">{{trans('lang.cms_by_ship')}}</a>
            <a href="/admin/cms/page/9" class="list-group-item @if($url === 'users/favorites') active @endif">{{trans('lang.cms_by_air_flight')}}</a>
            <a href="/admin/cms/page/10" class="list-group-item @if($url === 'users/favorites') active @endif">{{trans('lang.cms_domestic_fee')}}</a>
        </div>
    </div>

    <div class="panel panel-default">

        <div class="panel-heading">
            {{--<div class="tools"><a class="btn btn-large btn-success" href="javascript:void(0);" onclick="javascript:introJs().start();">How to use ?</a></div>--}}
            <div class="title">Export</div>
        </div>
        <div class="list-group">
            <a href="/admin/cms/page/11" class="list-group-item @if($url === 'users/favorites') active @endif">{{trans('lang.cms_information')}}</a>
            <a href="/admin/cms/page/12" class="list-group-item @if($url === 'users/favorites') active @endif">{{trans('lang.cms_transportation_fee')}}</a>
            <a href="/admin/cms/page/13" class="list-group-item @if($url === 'users/favorites') active @endif">{{trans('lang.cms_payment')}}</a>
        </div>
    </div>
</div>