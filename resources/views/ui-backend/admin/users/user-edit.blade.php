@extends('ui-backend/partials.master')

@section('content')

    @include('ui-backend/admin/users/header')

    <div class="main-content">

        @include('errors.error')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    {{--<div class="panel-heading">--}}
                        {{--<div class="tools"><a data-modal="add-bank" class="md-trigger" href="#"><button type="link" class="btn btn-space btn-primary" data-step="4" data-intro="Add your address" data-position='right'>{{trans('lang.btn_add_new')}}</button></a></div>--}}
                        {{--<div class="title">{{trans('lang.users')}}</div>--}}
                    {{--</div>--}}
                    <div class="panel-body">
                        <form  role="form" method="POST" action="/admin/users/edit/{{$user->id}}" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            {{--<input type="hidden" name="_method" value="PUT" />--}}

                            <div data-step="1" data-intro="Update Your profile" data-position='left'>
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-2">

                                        <div class="form-group">
                                            <label>User ID</label>
                                            <input type="text" name="user_code"  placeholder="UserID" value="{{$user->user_code}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Full name</label>
                                            <input type="text" name="name"  placeholder="UserID" value="{{$user->name}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Email address</label>
                                            <input type="email" name="email"  placeholder="Enter email" value="{{$user->email}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>ยอดเงินคงเหลือ</label>
                                            <input type="text" name="user_amount_balance"  placeholder="Enter email" value="{{$user->user_amount_balance}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>สิทธิ์ การใช้งาน</label>
                                            <select class="form-control" name="role">
                                                <option @if($user->role == 'admin')selected @endif value="admin">ผู้ดูแลระบบ</option>
                                                <option @if($user->role == 'accounting')selected @endif value="accounting">แผนกบัญชี</option>
                                                <option @if($user->role == 'operator')selected @endif value="operator">ผู้ตรวจบิล/สั่งซื้อสิ้นค้าจากจีน</option>
                                                <option @if($user->role == 'user')selected @endif value="user">ผู้ใช้งานทั่วไป</option>
                                            </select>
                                        </div>


                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" name="password"  placeholder="Password" value="" class="form-control">
                                        </div>


                                        <div class="form-group">
                                            <button type="submit" class="btn btn-space btn-primary form-control">Update</button>
                                        </div>

                                    </div>
                                </div>

                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>


@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();

        });
    </script>

@endsection