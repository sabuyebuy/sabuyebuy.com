@extends('ui-backend/partials.master')

@section('content')

    @include('ui-backend/admin/users/header')

    <div class="main-content">

        @include('errors.error')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    {{--<div class="panel-heading">--}}
                    {{--<div class="tools"><a data-modal="add-bank" class="md-trigger" href="#"><button type="link" class="btn btn-space btn-primary" data-step="4" data-intro="Add your address" data-position='right'>{{trans('lang.btn_add_new')}}</button></a></div>--}}
                    {{--<div class="title">{{trans('lang.users')}}</div>--}}
                    {{--</div>--}}
                    <div class="panel-body">
                        <form  role="form" method="POST" action="/admin/products" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            {{--<input type="hidden" name="_method" value="PUT" />--}}

                            <div data-step="1" data-intro="Update Your profile" data-position='left'>
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-2">

                                        <div class="form-group">
                                            <label>หมวดหมู่</label>
                                            <select class="form-control" name="category">
                                                @foreach($category as $cat)
                                                    <option value="{{$cat->id}}">{{$cat->category_name}}</option>
                                            @endforeach
                                            </select>
                                        </div>


                                        <div class="form-group">
                                            <label>ชื่อสินค้า</label>
                                            <input type="text" name="title"   value="{{old('title')}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>ราคาเต็ม</label>
                                            <input type="text" name="full_price"   value="{{old('full_price')}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>ราคาลดแล้ว </label>
                                            <input type="text" name="price"  value="{{old('price')}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Link สินค้า</label>
                                            <input type="text" name="external_link"  value="{{old('external_link')}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Link ภาพสินค้า</label>
                                            <input type="text" name="image" value="{{old('image')}}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>ร้านค้า</label>
                                            <input type="text" name="shop" value="{{old('shop')}}" class="form-control">
                                        </div>


                                        <div class="form-group">
                                            <button type="submit" class="btn btn-space btn-primary form-control">Create</button>
                                        </div>

                                    </div>
                                </div>

                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>


@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();

        });
    </script>

@endsection