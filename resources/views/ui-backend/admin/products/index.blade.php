@extends('ui-backend/partials.master')

@section('content')

    @include('ui-backend/admin/products/header')

    <div class="main-content">

        @include('errors.error')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                    <div class="tools"><a href="/admin/products/create"><button type="link" class="btn btn-space btn-primary pull-right">{{trans('lang.btn_add_new')}}</button></a></div>
                        <div class="title">{{trans('lang.products')}}</div>
                    </div>
                    <div class="panel-body">
                        <table id="table-users" class="table table-striped table-hover table-fw-widget">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>ภาพสินค้า</th>
                                <th>ชื่อสินค้า</th>
                                <th>ร้าน</th>
                                <th>ราคา</th>
                                <th>ไปหน้าร้านค้า</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0?>
                            @foreach($products as $product)

                                <tr>
                                    <td class="">{{$i+1}}</td>
                                    <td class=""><img src="{{$product->image}}" width="80" height="80"></td>
                                    <td class="">{{$product->title}}</td>
                                    <td class="">{{$product->shop}}</td>
                                    <td class="">{{$product->price}}</td>
                                    <td class=""><a href="{{$product->external_link}}" target="_blank">ดูหน้าสินค้า</a></td>
                                    <td class="text-right">

                                        <div class="btn-group btn-space">
                                            <button type="button" data-toggle="dropdown" class="btn btn-primary">Actions <span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="/admin/products/edit/{{$product->id}}">{{trans('lang.btn_edit')}}</a></li>
                                                <li><a data-modal="full-danger-{{$product->id}}" class="md-trigger" href="#">{{trans('lang.btn_delete')}}</a></li>
                                            </ul>
                                        </div>


                                        <!-- Nifty Modal delete-->
                                        <div id="full-danger-{{$product->id}}" class="md-modal modal-container modal-colored-header md-effect-8">
                                            <div class="md-content">
                                                <div class="modal-header">
                                                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><i class="icon s7-close"></i></button>
                                                    <h3 class="modal-title">{{trans('lang.btn_alert_danger')}} !</h3>
                                                </div>
                                                <form action="/admin/products/delete/{{$product->id}}" method="POST">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    {{--<input type="hidden" name="_method" value="DELETE" />--}}
                                                    <div class="modal-body">
                                                        <div class="text-center">
                                                            <div class="i-circle success"><i class="icon s7-check"></i></div>
                                                            <p>{{trans('lang.word_alert_danger')}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_cancel')}}</button>
                                                        <button type="submit" data-dismiss="modal" class="btn btn-danger btn-shade1 md-close">{{trans('lang.btn_confirm')}}</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </td>
                                </tr>

                                <?php $i++;?>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function(){
            //initialize the javascript
            App.init();
            App.formElements();
//            App.dataTables();
            $('.md-trigger').modalEffects();

            $("#table-users").dataTable();
        });
    </script>

@endsection