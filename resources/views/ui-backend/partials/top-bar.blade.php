<div class="container-fluid">
    <div class="navbar-header">
        <div class="page-title"><span>{{trans('lang.home')}}</span></div>
        <a href="#" class="am-toggle-left-sidebar navbar-toggle collapsed">
            <span class="icon-bar">
                <span></span>
                <span></span>
                <span></span></span></a>
        <a href="/" class="navbar-brand"> <img src="{{asset('/images/logo.png')}}" alt=""></a>
    </div>

    <a href="#" data-toggle="collapse" data-target="#am-navbar-collapse" class="am-toggle-top-header-menu collapsed">
        <span class="icon s7-angle-down"></span>
    </a>

    <div id="am-navbar-collapse" class="collapse navbar-collapse">

        <?php $user = Auth::user();?>
        <ul class="nav navbar-nav navbar-right am-user-nav">
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
                    @if(@$user->user_image != '')
                        <img src="{{asset('/uploads/images/'.@$user->user_image)}}">
                    @else
                        <img src="{{asset('/uploads/images/photo.jpg')}}">
                    @endif
                    <span class="user-name">{{@$user->user_image}}</span>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="/users/profile"> <span class="icon s7-user"></span>{{trans('lang.menu_profile')}}</a></li>
                    <li><a href="/logout"> <span class="icon s7-power"></span>Sign Out</a></li>
                </ul>
            </li>
        </ul>
        @if(@$user->role == 'user')

            <ul class="nav navbar-nav am-nav-right">
                <li><a href="/users/dashboard"><i class="icon s7-graph3"></i>{{trans('lang.menu_dashboard')}}</a></li>
                <li><a href="/cart"><i class="icon s7-cart"></i>{{trans('lang.menu_cart')}}</a></li>

                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"  >
                        <i class="s7-cash"></i>{{trans('lang.menu_account')}} <span class="angle-down s7-angle-down"></span>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li ><a href="/users/wallet/deposit">{{trans('lang.menu_account_deposit')}}</a></li>
                        <li><a href="/users/wallet/withdrawal">{{trans('lang.menu_account_withdrawal')}}</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
                        <i class="s7-gift"></i>{{trans('lang.menu_privilege')}} <span class="angle-down s7-angle-down"></span>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="#">{{trans('lang.menu_privilege_coupon')}}</a></li>
                        <li><a href="#">{{trans('lang.menu_privilege_point')}}</a></li>
                    </ul>
                </li>
            </ul>
        @elseif(@$user->role == 'accounting')
            <ul class="nav navbar-nav am-nav-right">

                <li class="dropdown">
                    <a href="/accounting/deposit" role="button" aria-expanded="false" class="dropdown-toggle"  >
                        <i class="s7-download"></i>{{trans('lang.deposit_transaction')}}
                    </a>
                </li>
                <li class="dropdown">
                    <a href="/accounting/withdrawal" role="button" aria-expanded="false" class="dropdown-toggle">
                        <i class="s7-upload"></i>{{trans('lang.withdrawal_transaction')}}
                    </a>
                </li>
            </ul>
        @elseif(@$user->role == 'cs')
            <ul class="nav navbar-nav am-nav-right">

                <li class="dropdown">
                    <a href="/cs/my-response-orders" role="button" aria-expanded="false" class="dropdown-toggle"  >
                        <i class="s7-download"></i>{{trans('lang.cs_orders_list')}}
                    </a>
                </li>

            </ul>
        @elseif(@$user->role == 'admin')
            <ul class="nav navbar-nav am-nav-right">

                <li class="dropdown">
                    <a href="/admin/exchange-rate" role="button" aria-expanded="false" class="dropdown-toggle"  >
                        <i class="s7-repeat"></i>Exchange Rate
                    </a>
                </li>

                <li class="dropdown">
                    <a href="/admin/products" role="button" aria-expanded="false" class="dropdown-toggle"  >
                        <i class="s7-shopbag"></i>Products
                    </a>
                </li>

                <li class="dropdown">
                    <a href="/admin/users" role="button" aria-expanded="false" class="dropdown-toggle"  >
                        <i class="s7-users"></i>Users
                    </a>
                </li>

                <li class="dropdown">
                    <a href="/admin/cms/page/1" role="button" aria-expanded="false" class="dropdown-toggle"  >
                        <i class="s7-note2"></i>CMS
                    </a>
                </li>

                <li class="dropdown">
                    <a href="/admin/languages" role="button" aria-expanded="false" class="dropdown-toggle"  >
                        <i class="s7-global"></i>Languages
                    </a>
                </li>
            </ul>
        @endif

    </div>
</div>