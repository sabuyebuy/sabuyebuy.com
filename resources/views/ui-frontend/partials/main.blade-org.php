<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="en" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>SabuyEbuy.com Chaina product import to Thailand</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon
    ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">

    <!-- CSS  -->

    <!-- Google Fonts CSS
    ============================================ -->
    {{--<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>--}}
    <link href="https://fonts.googleapis.com/css?family=Kanit&subset=thai" rel="stylesheet">

    <link rel="stylesheet" href="css/front-end.css">

    <!-- Mordernizr JS
    ============================================ -->
    {{--<script src="js/vendor/modernizr-2.8.3.min.js"></script>--}}

</head>
<body class="home-1">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!--HEADER AREA START-->
<section class="header-area">

    <div class="header-middle">
        <div class="container-fluid">
            <div class="row">
                <div class=" col-lg-2">
                    <div class="left-category-menu hidden-sm  hidden-xs hidden-md">
                        <div class="left-product-cat">
                            <div class="category-heading">
                                <span>category</span>
                                <div class="cat-align">
                                    <i class="fa fa-align-left"></i>
                                </div>
                            </div>
                            <!-- CATEGORY-MENU-LIST START -->
                            <div class="category-menu-list">
                                <ul>
                                    <!--SINGLE MENU START-->
                                    <li class="arrow-plus">
                                                            <span class="cat-thumb">
                                                                <i class="fa fa-television"></i>
                                                            </span>
                                        <a  href="#">electronic</a>
                                        <!-- MEGA MENU START -->
                                        <div class="cat-left-drop-menu  layer-one">
                                            <div class="cat-left-drop-menu-left">
                                                <ul>
                                                    <li class="arrow-plus"><a href="#">television</a>
                                                        <div class="cat-left-drop-menu  layer-two">
                                                            <div class="cat-left-drop-menu-left">
                                                                <ul>
                                                                    <li><a href="#">LCD TV</a></li>
                                                                    <li><a href="#">LED TV</a></li>
                                                                    <li><a href="#">curved TV</a></li>
                                                                    <li><a href="#">plazma TV</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="arrow-plus"><a href="#">refrigeretor</a>
                                                        <div class="cat-left-drop-menu  layer-two">
                                                            <div class="cat-left-drop-menu-left">
                                                                <ul>
                                                                    <li><a href="#">LG</a></li>
                                                                    <li><a href="#">toshiba</a></li>
                                                                    <li><a href="#">panasonic</a></li>
                                                                    <li><a href="#">samsung</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="arrow-plus"><a href="#">air conditioners</a>
                                                        <div class="cat-left-drop-menu  layer-two">
                                                            <div class="cat-left-drop-menu-left">
                                                                <ul>
                                                                    <li><a href="#">samsung</a></li>
                                                                    <li><a href="#">sanaky</a></li>
                                                                    <li><a href="#">panasonic</a></li>
                                                                    <li><a href="#">samsung</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--  MEGA MENU END -->
                                    </li>
                                    <!--SINGLE MENU END-->
                                    <!--SINGLE MENU START-->
                                    <li class="arrow-plus">
                                                            <span class="cat-thumb">
                                                                <i class="fa fa-female"></i>
                                                            </span>
                                        <a  href="#">fashion &amp; beauty</a>
                                        <!-- MEGA MENU START -->
                                        <div class="cat-left-drop-menu layer-one">
                                            <div class="cat-left-drop-menu-left">
                                                <ul>
                                                    <li><a href="#">health &amp; beauty</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--  MEGA MENU END -->
                                    </li>
                                    <!--SINGLE MENU END-->
                                    <!--SINGLE MENU START-->
                                    <li class="arrow-plus">
                                                            <span class="cat-thumb">
                                                                <i class="fa fa-camera"></i>
                                                            </span>
                                        <a  href="#">Camera &amp; Photo</a>
                                        <!-- MEGA MENU START -->
                                        <div class="cat-left-drop-menu layer-one">
                                            <div class="cat-left-drop-menu-left">
                                                <ul>
                                                    <li><a href="#">apple</a></li>
                                                    <li><a href="#">lg</a></li>
                                                    <li><a href="#">samsung</a></li>
                                                    <li><a href="#">sony</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--  MEGA MENU END -->
                                    </li>
                                    <!--SINGLE MENU END-->
                                    <!--SINGLE MENU START-->
                                    <li class="arrow-plus">
                                                            <span class="cat-thumb">
                                                                <i class="fa fa-laptop"></i>
                                                            </span>
                                        <a  href="shop.html">Smartphone &amp; laptop</a>
                                        <!-- MEGA MENU START -->
                                        <div class="cat-left-drop-menu layer-one">
                                            <div class="cat-left-drop-menu-left">
                                                <ul>
                                                    <li><a href="#">apple</a></li>
                                                    <li><a href="#">lg</a></li>
                                                    <li><a href="#">samsung</a></li>
                                                    <li><a href="#">sony</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--  MEGA MENU END -->
                                    </li>
                                    <!--SINGLE MENU END-->

                                    <!--SINGLE MENU START-->
                                    <li>
                                                            <span class="cat-thumb">
                                                                <i class="fa fa-futbol-o"></i>
                                                            </span>
                                        <a  href="#">sports &amp; outdoor</a>
                                    </li>
                                    <!--SINGLE MENU END-->
                                    <!--SINGLE MENU START-->
                                    <li>
                                                            <span class="cat-thumb">
                                                                <i class="fa fa-motorcycle"></i>
                                                            </span>
                                        <a  href="#">automotive &amp; motorcycle</a>
                                    </li>
                                    <!--SINGLE MENU END-->
                                    <!--SINGLE MENU START-->
                                    <li>
                                                            <span class="cat-thumb">
                                                                <i class="fa fa-cogs"></i>
                                                            </span>
                                        <a  href="#">washing machine</a>
                                    </li>
                                    <!--SINGLE MENU END-->
                                    <!--SINGLE MENU START-->
                                    <li>
                                                            <span class="cat-thumb">
                                                                <i class="fa fa-laptop"></i>
                                                            </span>
                                        <a  href="#">toshiba</a>
                                    </li>
                                    <!--SINGLE MENU END-->
                                    <!--SINGLE MENU START-->
                                    <li>
                                                            <span class="cat-thumb">
                                                                <i class="fa fa-sun-o"></i>
                                                            </span>
                                        <a  href="#">samsung</a>
                                    </li>
                                    <!--SINGLE MENU END-->
                                    <!--SINGLE MENU START-->
                                    <li>
                                                            <span class="cat-thumb">
                                                                <i class="fa fa-cube"></i>
                                                            </span>
                                        <a  href="#">sanyo</a>
                                    </li>
                                    <!--SINGLE MENU END-->
                                    <!--SINGLE MENU START-->
                                    <li>
                                                            <span class="cat-thumb">
                                                                <i class="fa fa-hdd-o"></i>
                                                            </span>
                                        <a  href="#">LG</a>
                                    </li>
                                    <!--SINGLE MENU END-->
                                    <!--SINGLE MENU START-->
                                    <li>
                                                            <span class="cat-thumb">
                                                                <i class="fa fa-clock-o"></i>
                                                            </span>
                                        <a  href="#">jewellery &amp; watches</a>
                                    </li>
                                    <!--SINGLE MENU END-->
                                    <!--MENU ACCORDION START-->
                                    <li class="rx-child">
                                        <a href="shop.html">
                                            <span class="cat-thumb"></span>
                                            phones &amp; pades
                                        </a>
                                    </li>
                                    <li class=" rx-child">
                                        <a href="shop.html">
                                            <span class="cat-thumb"></span>
                                            clothing
                                        </a>
                                    </li>
                                    <li class=" rx-parent">
                                        <a class="rx-default">
                                            <span class="cat-thumb  fa fa-plus"></span>
                                            More categories
                                        </a>
                                        <a class="rx-show">
                                            <span class="cat-thumb  fa fa-minus"></span>
                                            close menu
                                        </a>
                                    </li>
                                    <!--MENU ACCORDION END-->
                                </ul>
                            </div>
                            <!-- CATEGORY-MENU-LIST END -->
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-8 col-lg-7 no-padding">
                    <!--Logo start-->
                    <div class="logo">
                        <a href="#"><img src="images/logo.png" alt="logo"></a>
                    </div>
                    <!--MAIN MENU START-->
                    <div class="main-menu hidden-sm hidden-xs hidden-md">
                        <nav>
                            <ul>
                                <li><a href="#">Home</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">Grab URL</a></li>
                                        <li><a href="#">China Website</a></li>
                                        <li><a href="#">Exchange Rate</a></li>
                                        <li><a href="#">Promotions</a></li>
                                        <li><a href="#">Live Chat</a></li>
                                        <li><a href="#">Calculate Fee</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">About us</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">Who we are ?</a></li>
                                        <li><a href="#">Vision</a></li>
                                        <li><a href="#">Mission</a></li>
                                        <li><a href="#">Core Competency</a></li>
                                        <li><a href="#">Our Services</a></li>
                                    </ul>
                                </li>

                                <li><a href="#">How to Order</a>
                                </li>
                                <li><a href="#">Import</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">By Truck</a></li>
                                        <li><a href="#">By Ship</a></li>
                                        <li><a href="#">By Air Freight</a></li>
                                        <li><a href="#">Domestic Fee</a></li>
                                    </ul>
                                </li>


                                <li><a href="#">Export</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">Information</a></li>
                                        <li><a href="#">Transportation Fee</a></li>
                                        <li><a href="#">Payment</a></li>
                                    </ul>
                                </li>


                                {{--<li><a href="#">Contact us</a></li>--}}
                            </ul>
                        </nav>
                    </div>
                    <!--MAIN MENU END-->
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 no-padding-left">
                    <div class="top-right-menu-wrapper">

                        <div class="plus-account">
                            <div class="plus-icon">
                                <a href="#"><span class="flag-icon flag-icon-th"></span></a>
                                <a href="#"><span class="flag-icon flag-icon-cn"></span></a>
                                <a href="#"><span class="flag-icon flag-icon-gb"></span></a>
                            </div>
                        </div>

                        <div class="cart-wrapper">
                            <div class="cart">
                                <i class="fa fa-cart-plus"></i>
                            </div>

                        </div>

                        <div class="plus-account">
                            <div class="plus-icon">
                                {{--<a href="#"><i class="fa fa-user"></i></a>--}}
                                @if(Auth::check())<a href="/users/profile"><small style="
    color: #fff;
">{{Auth::user()->name}}</small>
                                    @if(Auth::user()->user_image != '')
                                        <img src="{{asset('/uploads/images/'.Auth::user()->user_image)}}" width="30" height="30" class="img-circle">
                                    @else
                                        <img src="{{asset('/uploads/images/photo.jpg')}}" width="30" height="30" class="img-circle">
                                    @endif
                                    @endif
                                    @if(!Auth::check())<a href="/login">Login</a>@endif
                            </div>
                            {{--<div class="plus-menu">--}}
                            {{--<ul>--}}
                            {{--<li><a href="/users/profile">My account</a></li>--}}
                            {{--@if(Auth::check())<li><a href="/logout">Logout</a></li>@endif--}}
                            {{--@if(!Auth::check())<li><a href="/login">Login</a></li>@endif--}}
                            {{--</ul>--}}
                            {{--</div>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--MOBILE MENU START-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 ">
                <div class="mobile-menu ">
                    <nav class="mobile-menu-start">
                        <ul>
                            <li><a href="#">Home</a>

                            <li>
                                <a href="">About us</a>
                                <ul>
                                    <a href="#">Who we are ?</a>
                                    <a href="#">Vision</a>
                                    <a href="#">Mission</a>
                                    <a href="#">Core Competency</a>
                                    <a href="#">Our Services</a>
                                </ul>
                            </li>
                            <li>
                                <a href="">Import</a>
                                <ul>
                                    <li><a href="#">By Truck</a></li>
                                    <li><a href="#">By Ship</a></li>
                                    <li><a href="#">By Air Freight</a></li>
                                    <li><a href="#">Domestic Fee</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="">Export</a>
                                <ul>
                                    <li><a href="#">Information</a></li>
                                    <li><a href="#">Transportation Fee</a></li>
                                    <li><a href="#">Payment</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">contact us</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!--MOBILE MENU END-->
    <div class="header-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class=" col-xs-12 col-sm-12 col-lg-offset-2 col-md-6 col-lg-6">

                    <div class="webgrab"><img src="/images/webgrab.png" alt=""></div>

                    <div class="categorys-product-search">
                        <form action="#" method="get" class="search-form-cat">
                            <div class="search-product form-group">
                                <input type="text" class="form-control search-form " placeholder="วาง Link หน้าสินค้า"/>
                                <button class="search-button" value="Search" name="s" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                    <div class="header-shipping">
                        <ul>
                            <li><i class="fa fa-envelope-o"></i><strong>Email:</strong>admin@bootexperts.com</li>
                            <li><i class="fa fa-phone"></i><strong>Hotline:</strong>+001 555 801</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--HEADER AREA END-->

@yield('content')



<!-- main js
============================================ -->
<script src="js/front-end.js"></script>

@yield('scripts')

</body>
</html>
