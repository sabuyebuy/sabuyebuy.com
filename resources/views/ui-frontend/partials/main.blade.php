<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="en" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>SabuyEbuy.com Chaina product import to Thailand</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon
    ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">

    <!-- CSS  -->

    <!-- Google Fonts CSS
    ============================================ -->
    {{--<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>--}}
    <link href="https://fonts.googleapis.com/css?family=Kanit&subset=thai" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('css/front-end.css')}}">

    <!-- Mordernizr JS
    ============================================ -->
    {{--<script src="js/vendor/modernizr-2.8.3.min.js"></script>--}}

</head>
<body class="home-1">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<?php
    $user = Auth::user();
    $isLoggedIn = Auth::check();
?>

<!--HEADER AREA START-->
<section class="header-area">
    <div class="header-middle  hidden-sm hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding">
                    <!--Logo start-->
                    <div class="logo">
                        <a href="{{url('/')}}"><img src="{{asset('images/logo.png')}}" alt="logo"></a>
                    </div>
                    <!--MAIN MENU START-->
                    <div class="main-menu hidden-sm hidden-xs hidden-md">
                        <nav>
                            <ul>
                                <li><a href="{{url('/')}}">{{trans('lang.menu_home')}}</a>
                                    {{--<ul class="sub-menu">--}}
                                        {{--<li><a href="#">Grab URL</a></li>--}}
                                        {{--<li><a href="#">China Website</a></li>--}}
                                        {{--<li><a href="#">Exchange Rate</a></li>--}}
                                        {{--<li><a href="#">Promotions</a></li>--}}
                                        {{--<li><a href="#">Live Chat</a></li>--}}
                                        {{--<li><a href="#">Calculate Fee</a></li>--}}
                                    {{--</ul>--}}
                                </li>
                                <li><a href="#">{{trans('lang.menu_about')}}</a>
                                    <ul class="sub-menu">
                                        <li><a href="/about-us/who-we-are">{{trans('lang.menu_about_who_we_are')}}</a></li>
                                        <li><a href="/about-us/vision">{{trans('lang.menu_about_vision')}}</a></li>
                                        <li><a href="/about-us/mission">{{trans('lang.menu_about_mission')}}</a></li>
                                        <li><a href="/about-us/core-competency">{{trans('lang.menu_about_core_competency')}}</a></li>
                                        <li><a href="/about-us/our-services">{{trans('lang.menu_about_our_service')}}</a></li>
                                    </ul>
                                </li>

                                <li><a href="/how-to-order">{{trans('lang.menu_how_to_order')}}</a>
                                </li>
                                <li><a href="#">{{trans('lang.menu_import')}}</a>
                                    <ul class="sub-menu">
                                        <li><a href="/import/by-truck">{{trans('lang.menu_import_by_truck')}}</a></li>
                                        <li><a href="/import/by-ship">{{trans('lang.menu_import_by_ship')}}</a></li>
                                        <li><a href="/import/by-air-flight">{{trans('lang.menu_import_by_air_freight')}}</a></li>
                                        <li><a href="/import/domestic-fee">{{trans('lang.menu_import_domestic_fee')}}</a></li>
                                    </ul>
                                </li>


                                <li><a href="#">{{trans('lang.menu_export')}}</a>
                                    <ul class="sub-menu">
                                        <li><a href="/export/information">{{trans('lang.menu_export_information')}}</a></li>
                                        <li><a href="/export/transportation-fee">{{trans('lang.menu_export_transportation')}}</a></li>
                                        <li><a href="/export/payment">{{trans('lang.menu_export_payment')}}</a></li>
                                    </ul>
                                </li>


                                {{--<li><a href="#">Contact us</a></li>--}}
                            </ul>
                        </nav>
                    </div>
                    <!--MAIN MENU END-->
                </div>

                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding-left">
                    <div class="top-right-menu-wrapper">

                        <div class="plus-account">
                            <div class="plus-icon">
                                <a href="/lang-switch/th"><span class="flag-icon flag-icon-th"></span></a>
                                <a href="/lang-switch/cn"><span class="flag-icon flag-icon-cn"></span></a>
                                <a href="/lang-switch/en"><span class="flag-icon flag-icon-gb"></span></a>
                            </div>
                        </div>
                        @if($isLoggedIn && $user->role == 'user')
                        <div class="cart-wrapper">
                            <div class="cart">
                                <a href="/cart"><i class="fa fa-cart-plus"></i></a>
                            </div>
                        </div>
                        @else
                            <div class="cart-wrapper">
                                <div class="cart">
                                    {{--<a href="/cart"><i class="fa fa-cart-plus"></i></a>--}}
                                </div>
                            </div>
                        @endif

                        <div class="plus-account">
                            <div class="plus-icon">
                                {{--<a href="#"><i class="fa fa-user"></i></a>--}}
                                @if($isLoggedIn)

                                    @if($user->role == 'user')

                                    <a href="/users/profile">
                                        <small style="color: #fff;">{{$user->name}}</small>
                                    </a>
                                    @elseif($user->role == 'accounting')
                                        <a href="/accounting/home">
                                            <small style="color: #fff;">{{$user->name}}</small>
                                        </a>
                                    @elseif($user->role == 'cs')
                                        <a href="/cs/home">
                                            <small style="color: #fff;">{{$user->name}}</small>
                                        </a>
                                    @else
                                        <a href="/admin/home">
                                            <small style="color: #fff;">{{$user->name}}</small>
                                        </a>
                                    @endif

                                    @if($user->user_image != '')
                                        <img src="{{asset('/uploads/images/'.$user->user_image)}}" width="30" height="30" class="img-circle">
                                    @else
                                        <img src="{{asset('/uploads/images/photo.jpg')}}" width="30" height="30" class="img-circle">
                                    @endif
                                @else
                                    <a href="/login">Login</a>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--MOBILE MENU START-->
    <div class="">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 ">
                <div class="mobile-menu ">
                    <nav class="mobile-menu-start">
                        <ul>
                            <li><a href="#">{{trans('lang.menu_home')}}</a>

                            <li>
                                <a href="#">{{trans('lang.')}}</a>
                                <ul>
                                    <a href="/about-us/who-we-are">{{trans('lang.menu_about_who_we_are')}}</a>
                                    <a href="/about-us/vision">{{trans('lang.menu_about_vision')}}</a>
                                    <a href="/about-us/mission">{{trans('lang.menu_about_mission')}}</a>
                                    <a href="/about-us/core-competency">{{trans('lang.menu_about_core_competency')}}</a>
                                    <a href="/about-us/our-services">{{trans('lang.menu_about_our_service')}}</a>
                                </ul>
                            </li>

                            <li><a href="#">{{trans('lang.menu_home')}}</a>
                            <li>
                                <a href="">{{trans('lang.menu_import')}}</a>
                                <ul>
                                    <li><a href="/import/by-truck">{{trans('lang.menu_import_by_truck')}}</a></li>
                                    <li><a href="/import/by-ship">{{trans('lang.menu_import_by_ship')}}</a></li>
                                    <li><a href="/import/by-air-flight">{{trans('lang.menu_import_by_air_freight')}}</a></li>
                                    <li><a href="/import/domestic-fee">{{trans('lang.menu_import_domestic_fee')}}</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="">Export</a>
                                <ul>
                                    <li><a href="/export/information">{{trans('lang.menu_export_information')}}</a></li>
                                    <li><a href="/export/transportation-fee">{{trans('lang.menu_export_transportation')}}</a></li>
                                    <li><a href="/export/payment">{{trans('lang.menu_export_payment')}}</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="/contact">{{trans('lang.menu_contact')}}</a>
                            </li>
                            <li>
                                @if($isLoggedIn)
                                    <a href="/users/profile">
                                        {{$user->name}}

                                    @if($user->user_image != '')
                                        <img src="{{asset('/uploads/images/'.Auth::user()->user_image)}}" width="30" height="30" class="img-circle pull-right">
                                    @else
                                        <img src="{{asset('/uploads/images/photo.jpg')}}" width="30" height="30" class="img-circle pull-right">
                                    @endif
                                    </a>
                                @endif
                                @if(!$isLoggedIn)<a href="/login">Login</a>@endif
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!--MOBILE MENU END-->
</section>
<!--HEADER AREA END-->

<div class="container">
    <div class="categorys-product-search">
        <form action="#" method="get" class="search-form-cat">
            <div class="search-product form-group">
                <input type="text" class="form-control search-form " placeholder="วาง Link หน้าสินค้า">
                <button class="search-button" value="Search" name="s" type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </div>
        </form>
    </div>
</div>


@yield('content')

<!-- main js
============================================ -->
<script src="{{asset('js/front-end.js')}}"></script>

@yield('scripts')


<!--FOOTER AREA START-->
<section class="footer-area">
    {{--<img class="add" src="{{asset('img/ads.jpg')}}" alt="">--}}
    <div class="container-fluid">
        <div class="row">
            <div class="footer-top">
                <div class="col-sm-6 col-md-2 border-right">
                    <div class="single-footer">
                        <h3 class="footer-top-heading">About Us</h3>
                        <div class="footer-list">
                            <ul>
                                <li class="footer-list-item"><a href="#">Company History</a></li>
                                <li class="footer-list-item" ><a href="#">Social Responsibility</a></li>
                                <li class="footer-list-item"><a href="#">Investor Relations</a></li>
                                <li class="footer-list-item" ><a href="#">Healijy on Papers</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-2 border-right">
                    <div class="single-footer">
                        <h3 class="footer-top-heading">Information</h3>
                        <div class="footer-list">
                            <ul>
                                <li class="footer-list-item"><a href="#">our blog</a></li>
                                <li class="footer-list-item" ><a href="#">about our shop</a></li>
                                <li class="footer-list-item"><a href="#">secure shopping</a></li>
                                <li class="footer-list-item" ><a href="#">privacy policy</a></li>
                                <li class="footer-list-item" ><a href="#">dekivery information</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-2 border-right ">
                    <div class="single-footer">
                        <h3 class="footer-top-heading">my account</h3>
                        <div class="footer-list">
                            <ul>
                                <li class="footer-list-item"><a href="#">my account</a></li>
                                <li class="footer-list-item" ><a href="#">shopping cart</a></li>
                                <li class="footer-list-item"><a href="#">custom link</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3 border-right">
                    <div class="contact-us">
                        <ul>
                            <li><i class="fa fa-phone"></i>PHONE: (012) 345 6789</li>
                            <li><i class="fa fa-truck"></i>FREE SHIPPING ANYWHERE WORLDWIDE</li>
                            <li><i class="fa fa-unlock-alt"></i>100% SECURE FOR PAYMENT</li>
                            <li><i class="fa fa-headphones"></i>24/24 ONLINE SUPPORT CUSTOME</li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="news-letter">
                        <h3 class="footer-top-heading">send newsletter</h3>
                        <div class="newsletter-wrapper">
                            <div class="subscribe-inner">
                                <input type="text" id="subscribe_email">
                                <a class="sub-button" href="">subscribe</a>
                            </div>
                        </div>
                        <div class="payment">
                            <img src="{{asset('img/payment.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-bottom">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
                    <div class="logo">
                        <img src="{{asset('images/logo.png')}}" class="img-responsive" alt="">
                    </div>
                    <div class="footer-copyright ">
                        Copyright &copy; 2016 <a href="#">CTTLogistics</a>. All rights reserved
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-6">
                    <div class="social-icon-footer">
                        <ul class="social-icons">
                            <li><a data-toggle="tooltip" data-placement="top" title="Facebook" href="#" ><i class="fa fa-facebook"></i></a></li>
                            <li><a data-toggle="tooltip" title="Twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a data-toggle="tooltip" title="Pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a data-toggle="tooltip" title="Google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>                                                                 <li><a data-toggle="tooltip" title="Dribbble" href=""><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--FOOTER AREA END-->
<!-- QUICKVIEW PRODUCT -->
<div id="quickview-wrapper">
    <!-- Modal -->
    <div class="modal fade" id="productModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="modal-product">
                        <div class="product-images">
                            <div class="main-image images">
                                <img alt="" src="{{asset('img/products/prod-10b.jpg')}}">
                            </div>
                        </div><!-- .product-images -->

                        <div class="product-info">
                            <h1>Diam quis cursus</h1>
                            <div class="price-box">
                                <p class="price"><span class="special-price"><span class="amount">$132.00</span></span></p>
                            </div>
                            <a href="shop.html" class="see-all">See all features</a>
                            <div class="quick-add-to-cart">
                                <form method="post" class="cart-qv">
                                    <div class="numbers-row">
                                        <input type="number" id="french-hens" value="3">
                                    </div>
                                    <button class="single_add_to_cart_button" type="submit">Add to cart</button>
                                </form>
                            </div>
                            <div class="quick-desc">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla.
                            </div>
                            <div class="social-sharing">
                                <div class="widget widget_socialsharing_widget">
                                    <h3 class="widget-title-modal">Share this product</h3>
                                    <ul class="social-icons">
                                        <li><a target="_blank" title="Facebook" href="#" class="facebook social-icon"><i class="fa fa-facebook"></i></a></li>
                                        <li><a target="_blank" title="Twitter" href="#" class="twitter social-icon"><i class="fa fa-twitter"></i></a></li>
                                        <li><a target="_blank" title="Pinterest" href="#" class="pinterest social-icon"><i class="fa fa-pinterest"></i></a></li>
                                        <li><a target="_blank" title="Google +" href="#" class="gplus social-icon"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a target="_blank" title="LinkedIn" href="#" class="linkedin social-icon"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .product-info -->
                    </div><!-- .modal-product -->
                </div><!-- .modal-body -->
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div>
    <!-- END Modal -->
</div>
<!-- END QUICKVIEW PRODUCT -->

</body>
</html>
