@extends('ui-frontend/partials.main')

@section('content')

    <!--ABOUT AREA START-->
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <!--FEATURED PRODUCT-->
                <div class="shop-featured-product">
                    <div class="area-heading">
                        <h3>{{$product->title}}</h3>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{$product->image}}" alt="" class="img-responsive">
                        </div>
                        <div class="col-md-8">

                            <h4>ร้านค้า : {{$product->shop}}</h4>
                            <h4>ราคาสินค้า : <span class="text-success">{{$product->price}}</span>  หยวน</h4>

                            <form  role="form" method="POST" action="/cart-add" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}" />

                                <div class="form-group">
                                    <label>จำนวนที่ต้องการ</label>
                                    <span class="plus-minus-cart"> <input type="number" name="quantity" value="1"> </span>
                                </div>

                                <div class="form-group">
                                    <label>หมายเหตุ</label>
                                    <textarea name="note" id="note" cols="30" rows="10">

                                    </textarea>
                                </div>

                                <!-- Indicates a successful or positive action -->
                                <button type="submit" class="btn btn-success">เพิ่มลงตระกร้า</button>

                                <input type="hidden" name="product_title" value="{{$product->title}}" />
                                <input type="hidden" name="product_url" value="{{$product->external_link}}" />
                                <input type="hidden" name="product_image_url" value="{{$product->image}}" />
                                <input type="hidden" name="product_shop" value="{{$product->shop}}" />
                                <input type="hidden" name="price" value="{{$product->price}}" />
                                <input type="hidden" name="user_id" value="{{$user}}" />
                            </form>

                        </div>
                    </div>

                    <p>
                        {!!html_entity_decode($product->body)!!}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!--ABOUT AREA END-->

@endsection