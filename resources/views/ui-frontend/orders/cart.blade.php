@extends('ui-frontend/partials.main')

@section('content')


    <!--ABOUT AREA START-->
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">

                <form  role="form" method="POST" action="/create-po" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <!--FEATURED PRODUCT-->
                    <div class="shop-featured-product">

                        @include('errors/error')

                        <div class="area-heading">
                            <h3>ตระกร้าสินค้า</h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <td></td>
                                    <td class="text-center">ชื่อสินค้า </td>
                                    <td class="text-center">ราคา</td>
                                    <td class="text-center">จำนวน</td>
                                    <td class="text-center">ราคารวม</td>
                                    <td class="text-center">ข้อมูลเพิ่มติม</td>
                                    <td class="text-center"><i class="fa fa-trash"></i></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td class="cart-td-img">
                                            <a href="{{$product->product_url}}" target="_blank">
                                                <img title="blandit blandit" alt="" src="{{$product->product_image_url}}"></a>
                                        </td>
                                        <td><a href="">{{$product->product_title}}</a></td>
                                        <td class="text-center">{{$product->price}}</td>
                                        <td class="text-center">
                                            <span class="plus-minus-cart"> <input type="number" value="{{$product->quantity}}"> </span>
                                        </td>
                                        <td class="cart-total-price text-center">{{$product->price * $product->quantity}}  ¥</td>
                                        <td class="cart-total-price text-center"><small>{{$product->note}}</small></td>
                                        <td class="cross text-center"><a href="/remove-from-cart/{{$product->id}}"><i class="fa fa-times-circle"></i></a></td>
                                    </tr>

                                    <input type="hidden" name="product_id[]" value="{{$product->id}}" />

                                @endforeach

                                </tbody>
                            </table>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="buttons-cart">
                                    {{--<input type="submit" value="ปรับปรุงตระกร้า" name="update_cart" class="button">--}}
                                    <a href="/" class="button-link">ดูสินค้าอื่นเพิ่ม</a>
                                    <input type="submit" value="สั่งซื้อสินค้าในตระกร้า" name="order" class="button">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h2 class="pull-right text-success">รวม : {{number_format($total)}} ¥ <small class="text-info">ประมาณ : {{number_format($thaiTotal)}} บาท</small></h2>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--ABOUT AREA END-->

@endsection