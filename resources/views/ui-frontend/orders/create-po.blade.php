@extends('ui-frontend/partials.main')

@section('content')
    <form  role="form" method="POST" action="/create-po-confirm" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{csrf_token()}}" />

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="shop-featured-product">

                        @include('errors/error')

                        <div class="area-heading">
                            <h3>การจัดส่ง</h3>
                        </div>


                        <!--FEATURED PRODUCT-->
                        <div class="form-group">
                            <label for="shipping_cn_to_th_by">จัดส่งสินค้าจาก จีนมาไทย</label>
                            <select name="shipping_cn_to_th_by" id="shipping_cn_to_th_by" class="">
                                <option value="car">ทางรถ</option>
                                <option value="ship" disabled>ทางเรือ</option>
                                <option value="fight" disabled>ทางอากาศ</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="shipping_th_to_user_by">จัดส่งสินค้าจากในไทย</label>
                            <select name="shipping_th_to_user_by" id="shipping_th_to_user_by" class="">
                                <option value="pick_at_ctt">รับสินค้าเองที่ CTT Logistics</option>
                                <option value="ctt_logistic">บริการขนส่งสินค้าจาก CTT Logistics</option>
                                <option value="kerry">Kerry Express</option>
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="wooden_box">ต้องการให้ตีลังไม้</label>
                            <select name="wooden_box" id="wooden_box" class="">
                                <option value="0">ไม่ ตีลังไม้</option>
                                <option value="1">ต้องการตีลังไม้</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                    <div class="shop-featured-product">

                        <div class="area-heading">
                            <h3>รายการสินค้า</h3>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <td></td>
                                    <td class="text-center">ชื่อสินค้า </td>
                                    <td class="text-center">ราคา</td>
                                    <td class="text-center">จำนวน</td>
                                    <td class="text-center">ราคารวม</td>
                                    <td class="text-center">ข้อมูลเพิ่มติม</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td class="cart-td-img">
                                            <a href="{{$product->product_url}}" target="_blank">
                                                <img title="blandit blandit" alt="" src="{{$product->product_image_url}}"></a>
                                        </td>
                                        <td><a href="">{{$product->product_title}}</a></td>
                                        <td class="text-center">{{$product->price}}</td>
                                        <td class="text-center">{{$product->quantity}}</td>
                                        <td class="cart-total-price text-center">{{$product->price * $product->quantity}}  ¥</td>
                                        <td class="cart-total-price text-center"><small>{{$product->note}}</small></td>
                                    </tr>

                                    <input type="hidden" name="products_id[]" value="{{$product->id}}" />

                                @endforeach

                                </tbody>
                            </table>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="buttons-cart">
                                    <h2 class="text-center text-success">รวม : {{number_format($total)}} ¥ <small class="text-info">ประมาณ : {{number_format($thaiTotal)}} บาท</small></h2>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="shop-featured-product">
                        <div class="area-heading">
                            <h3><i class="fa fa-info" aria-hidden="true"></i>  เงื่อนใขการให้บริการ</h3>
                        </div>

                        <p>
                            <span class="text-danger">**ทางบริษัทฯขอสงวนสิทธิ์ในการยกเลิกรายการหากราคาสินค้ามีการปรับเพิ่มขึ้นและคุณลูกค้าไม่ได้ระบุเงื่อนไขพิเศษด้านล่างนี้**</span>
                            <br>
                        <div class="well well-sm">
                            <div class="text-center">
                                <label class="">
                                    <input type="checkbox" name="assent_buy" id="assent_buy" value="1">
                                    <strong>ยอมรับเงื่อนไข </strong>
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="#" class="text-danger" data-toggle="modal" data-target="#service-condition"> <i class="glyphicon glyphicon-info-sign"></i> อ่านเงื่อนไขการใช้บริการ</a>
                                </label>
                            </div>
                        </div>
                        </p>

                        <br>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="shop-featured-product">
                        <div class="area-heading">
                            <h3>ยืนยันการสั่งซือ</h3>
                        </div>

                        <div class="buttons-cart text-center">
                            <input type="submit" value="ยืนยันการสั่งซื้อ" name="order" class="button">
                        </div>

                        <br>
                    </div>
                </div>
            </div>

        </div>

    </form>

    <!-- Modal -->
    <div class="modal fade" id="service-condition" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-info" aria-hidden="true"></i> เงื่อนไขการใช้บริการ</h4>
                </div>
                <div class="modal-body">
                    <ul>
                        <small>
                        <li> 1.ทางบริษัทเป็นเพียงคนกลางในการประสานงานสั่งซื้อสินค้าให้กับลูกค้าเท่านั้น ดังนั้นทางบริษัทไม่สามารถรับเปลี่ยน ชดใช้ หรือซื้อคืนสินค้าในกรณีทีร้านค้าจีนส่งมาผิดแบบ ผิดสี ผิดไซส์ ผิดจำนวน หรือมีตำหนิ ทั้งนี้เพราะทางบริษัทให้ลูกค้าเป็นผู้เลือกร้านค้าเอง ไม่มีส่วนร่วมในการตัดสินใจเลือกร้านค้าแต่อย่างใด แต่ทางบริษัทยินดีช่วยประสานกับร้านค้าเพื่อทวงสิทธิ์และผลประโยชน์ของลูกค้าให้อย่างเต็มความสามารถ แต่หากเป็นกรณีที่เป็นความผิดของทางบริษัทเอง เช่น สั่งผิดสี ผิดแบบ ผิดไซส์ ผิดจำนวน ไปจากที่ลูกค้าระบุมา ทางบริษัทยินดีรับผิดชอบให้ 100% ของมูลค่าสินค้าค่ะ</li>

                        <li> 2. ทางบริษัทไม่มีนโยบายแกะพัสดุสินค้าของลูกค้าทุก ๆ ท่านและจะใช้การอ้างอิงแยกสินค้าแต่ละ Order จากข้อมูล Tracking no. ตามที่ร้านค้าแจ้งมาเท่านั้น เว้นแต่ทางศุลกากรจีนและไทยจะเป็นผู้แกะสินค้าเพื่อตรวจสอบในพิธีการส่งออกหรือนำเข้าเท่านั้น</li>

                        <li> 3. การส่งสินค้าจากร้านค้าจีนมาที่โกดังจีนของเราจะเร็วหรือช้า ขึ้นอยู่หับความพร้อมของร้านค้านั้น ๆ และระยะห่างระหว่างร้านค้ากับโกดังที่กวางเจาของบริษัทเรา ซึ่งในส่วนนี้อยู่นอกเหลือความควบคุมของทางบริษัทค่ะ แต่โดยปกติทุกร้านจะส่งของถึงโกดังจีนของเราภายใน 3-5 วันนับจากวันที่เราชำระเงินให้แก่ร้านค้า และหากช้ากว่านั้นบริษัทจะช่วยเร่งและติดตามให้ร้านค้าจัดส่งมาโดยเร็วที่สุดค่ะ</li>

                        <li> 4.ร้านค้าในจีนส่วนใหญ่จะใช้เทคนิคในการปรับแต่ง สี แสง ในส่วนของรูปของสินค้าที่ขายบนหน้าเว็บไซต์ซึ่งอาจทำให้สินค้าจริงที่ได้รับสีเพี้ยนไปจากรูป</li>

                        <li> 5. บรรจุภัณฑ์สินค้าที่เป็นกล่องอาจมีสภาพบุบบ้างอันเนื่องมาจากการขนส่ง ซึ่งในส่วนนี้อยู่นอกเหนือความควบคุมของทางบริษัทค่ะ เพราะการขนส่งทางรถจะต้องมาเปลี่ยนถ่ายรถที่ประเทศลาวต่างจากการขนส่งทางเรือที่พอใส่ตู้คอนเทนเนอร์แล้วจะไม่ต้องขนถ่ายใด ๆ จนถึงโกดังของบริษัทที่ไทย ทำให้สินค้าหรือแพ็คเกจสินค้าไม่ชำรุดค่ะ ดังนั้น หากสินค้าสูญหายหรือชำรุด บริษัทจะชดใช้ให้ไม่เกิน 3 เท่าของค่าขนส่งเท่านั้น</li>

                        <li> 6. หากสินค้าหมดสต๊อกหรือร้านค้าขอยกเลิก ทางบริษัทจะโอนเงินคืนให้กับลูกค้าค่ะ ในกรณีที่ลูกค้าไม่ชำระค่าสินค้าภายใน 7 วัน หลังจากที่บริษัทแจ้งยอดชำระ บริษัทขอสงวนสิทธิ์นำสินค้าไปขายทอดตลอด</li>

                        <li> 7. หากมีข้อสงสัยเพิ่มเติมรบกวนติดต่อบริษัทเราได้ค่ะ เปิดทำการจันทร์-อาทิตย์ 09.00-18.00 น.</li>

                        <li> 8. หากสินค้าส่งใบสั่งซื้อมาให้กับทางบริษัทเราแล้วถือว่าลูกค้ายินยอมและยอมรับในข้อตกลงข้างต้นทั้งหมด</li>
                        </small>
                    </ul>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection