@extends('ui-frontend/partials.main')

@section('content')

    <div class="container">
        <!--TAB AND BANNER AREA START-->
        <div class="row">
            <div class="col-md-8">
                <section class="tab-and-banner-wrapper">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="https://ae01.alicdn.com/kf/HTB1DfDMPXXXXXXWaXXXq6xXFXXXN.jpg" alt="...">
                            </div>
                            <div class="item">
                                <img src="https://ae01.alicdn.com/kf/HTB15CjfPXXXXXa2apXXq6xXFXXXP.jpg" alt="...">
                            </div>
                            <div class="item">
                                <img src="https://ae01.alicdn.com/kf/HTB1nEHIPXXXXXa4apXXq6xXFXXX2.jpg" alt="...">
                            </div>
                            <div class="item">
                                <img src="https://ae01.alicdn.com/kf/HTB1GNrCPXXXXXXpapXXq6xXFXXX7.jpg" alt="...">
                            </div>
                        </div>

                    </div>
                </section>
            </div>
            <div class="col-md-4">
                <section class="tab-and-banner-wrapper">
                    {{--<div id="carousel-right" class="carousel slide" data-ride="carousel">--}}
                    {{--<!-- Indicators -->--}}
                    {{--<ol class="carousel-indicators">--}}
                    {{--<li data-target="#carousel-right" data-slide-to="0" class="active"></li>--}}
                    {{--<li data-target="#carousel-right" data-slide-to="1"></li>--}}
                    {{--</ol>--}}

                    {{--<!-- Wrapper for slides -->--}}
                    {{--<div class="carousel-inner" role="listbox">--}}
                    {{--<div class="item active">--}}
                    {{--<img src="https://ae01.alicdn.com/kf/HTB1A16VPXXXXXX_XVXXq6xXFXXXL.jpg" alt="">--}}
                    {{--</div>--}}
                    {{--<div class="item">--}}
                    {{--<img src="http://ae01.alicdn.com/kf/HTB1sdZ0MVXXXXXtXpXXq6xXFXXX3.jpg" alt="...">--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    {{--</div>--}}

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="s7-repeat"></i> อัตราแลกเปลี่ยน <small><span class="pull-right">เริ่มใช้งาน {{$data['start_date']}}</span></small></h3>
                        </div>
                        <div class="panel-body">

                            <h1 class="text-center text-danger" style="font-size: 8em;">{{$data['rate']}}</h1>
                        </div>

                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="s7-repeat"></i> ธนาคารกรุงเทพ <small><span class="pull-right">ข้อมูลเมื่อ 20-01-2017 08:30</span></small></h3>
                        </div>
                        <div class="panel-body">
                            <table style='width:300px;'>
                                <tr>
                                    <td style='width:70%;'>China</td>
                                    <td style='width:30%;'></td>
                                </tr>
                                <tr>
                                    <td style='width:70%;'>Bank Notes: Buying Rates</td>
                                    <td style='width:30%;'>4.88 </td>
                                </tr>
                                <tr>
                                    <td style='width:70%;'>Bank Notes: Selling Rates</td>
                                    <td style='width:30%;'>5.39 </td>
                                </tr>
                                <tr>
                                    <td style='width:70%;'>Buying Rates: Sight Bill</td>
                                    <td style='width:30%;'>5.08250 </td>
                                </tr>
                                <tr>
                                    <td style='width:70%;'>Buying Rates: TT</td>
                                    <td style='width:30%;'>5.12750 </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>


    {{--<div class="row">--}}
    {{--<div class="col-md-4">--}}

    {{--<div class="panel panel-primary">--}}
    {{--<div class="panel-heading">--}}
    {{--<h3 class="panel-title"><i class="s7-repeat"></i> อัตราแลกเปลี่ยน</h3>--}}
    {{--</div>--}}
    {{--<div class="panel-body">--}}

    {{--<h1 class="text-center" style="font-size: 6em;">{{$data['rate']}}</h1>--}}
    {{--</div>--}}
    {{--<div class="panel-footer">--}}
    {{--<h6 class="text-center"><i class="s7-date"></i> เริ่มใช้งาน {{$data['start_date']}}</h6>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--</div>--}}
    {{--<div class="col-md-4">--}}

    {{--</div>--}}
    {{--<div class="col-md-4">--}}

    {{--</div>--}}
    {{--</div>--}}


    <!--CATEGORY TWO AREA START-->
        <section class="category-area-two control-car mar-bottom">
            <div class="container-fluid">
                <div class="row">
                    <div class="cat-area-heading">
                        <h4>สินค้าแนะนำ</h4>
                    </div>
                </div>
                <div class="row">
                    <?php $i = 0;?>
                    @foreach($products as $product)
                        <div class="col-md-3">
                            {{--<a href="#">{{$product->title}}</a>--}}
                            <div class="single-product">
                                <div class="product-image">
                                    <div class="show-img">
                                        <a href="/product-view/{{$product->id}}"><img src="{{$product->image}}" width="250" height="250" style="min-height: 250px" class="img-responsive"></a>
                                    </div>
                                </div>
                                <div class="prod-info">
                                    <h2 class="pro-name">
                                        {{--<a href="#">{{$product->title}}</a>--}}
                                    </h2>
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                    <div class="price-box">
                                        <div class="price">
                                            <span>{{$product->price}} ¥</span>
                                        </div>
                                    </div>
                                    <div class="actions text-center">
                                            <span class="add-to-cart">
                                                <a href="/product-view/{{$product->id}}" data-toggle="tooltip" title="Add to cart">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    <span>add to cart</span>
                                                </a>
                                            </span>
                                    </div>
                                </div>
                            </div>



                        </div>



                        @if(($i%4) == 3)

                </div>
                <br>
                <div class="row ">

                        @endif

                        <?php //echo ($i%4) ?>

                        <?php $i++ ?>

                    @endforeach

                    @if($i%4 == 0)

                </div>

                @endif


            </div>
    </div>

    </div>
    </section>
    <!--CATEGORY TWO AREA END-->
    </div>
@endsection
