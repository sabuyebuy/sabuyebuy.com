@extends('ui-frontend/partials.main')

@section('content')

    <!--ABOUT AREA START-->
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <!--FEATURED PRODUCT-->
                <div class="shop-featured-product">
                    <div class="area-heading">
                        <h3>{{$title}}</h3>
                        <p>
                            {!!html_entity_decode($body)!!}
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--ABOUT AREA END-->



@endsection