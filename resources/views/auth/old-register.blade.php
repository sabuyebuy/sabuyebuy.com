<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/favicon.png">
    <title>Laravel workshop</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Raleway:300,200,100" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{     asset('/css/back-end.css') }}" type="text/css"/>
</head>
<body class="texture">
<div id="cl-wrapper" class="login-container">
    <div class="middle-login">
        <div class="block-flat">

            @include('errors.error')

            <div class="header">
                <h3 class="text-center">Dmeet Login</h3>
            </div>
            <div>
                <form style="margin-bottom: 0px !important;" method="POST" action="/auth/register" class="form-horizontal">

                    {!! csrf_field() !!}

                    <div class="content">
                        <h4 class="title">Login Access</h4>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" name="name" data-parsley-trigger="change" data-parsley-errors-container="#nick-error" required="" placeholder="Username" class="form-control">
                                </div>
                                <div id="nick-error"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="email" name="email" data-parsley-trigger="change" data-parsley-errors-container="#email-error" required="" placeholder="E-mail" class="form-control">
                                </div>
                                <div id="email-error"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 col-xs-6">
                                <div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input id="pass1" name="password" type="password" data-parsley-errors-container="#password-error" placeholder="Password" required="" class="form-control">
                                </div>
                                <div id="password-error"></div>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input parsley-equalto="#pass1" name="password_confirmation" type="password" data-parsley-errors-container="#confirmation-error" required="" placeholder="Confirm Password" class="form-control">
                                </div>
                                <div id="confirmation-error"></div>
                            </div>
                        </div>
                    </div>
                    <div class="foot">
                        <button type="submit" class="btn btn-block btn-success btn-rad btn-lg">Sign Up</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="text-center out-links"><a href="#">© 2016 SabuyEbuy.com</a></div>
    </div>
</div>
<script src="{{ asset('/js/back-end.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
    });

</script>
</body>
</html>
