<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}">
    <title>SabuyEbuy.com | Login</title>
    <![endif]-->
    <link rel="stylesheet" href="{{     asset('/css/back-end.css') }}" type="text/css"/>
</head>
<body class="am-splash-screen">
<div class="am-wrapper am-login">
    <div class="am-content">
        <div class="main-content">
            <div class="login-container">
                @include('errors.error')
                <div class="panel panel-default">
                    <div class="panel-heading"><img src="{{ asset('images/logo.png') }}" alt="logo"  class="logo-img"></div>
                    <div class="panel-body">
                        <form action="/login" method="POST" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="login-form">
                                <div class="form-group">
                                    <div class="input-group"><span class="input-group-addon"><i class="icon s7-user"></i></span>
                                        <input id="email" name="email" type="text" placeholder="Email" autocomplete="off" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group"><span class="input-group-addon"><i class="icon s7-lock"></i></span>
                                        <input id="password" name="password" type="password" placeholder="Password" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group login-submit">
                                    <button data-dismiss="modal" type="submit" class="btn btn-primary btn-lg">Log me in</button>
                                </div>
                                <div class="form-group footer row">
                                    <div class="col-xs-6"><a href="#">Forgot Password?</a></div>
                                    <div class="col-xs-6 remember">
                                        <label for="remember">Remember Me</label>
                                        <div class="am-checkbox">
                                            <input type="checkbox" id="remember">
                                            <label for="remember"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="form-group login-submit">
                            <a href="/register"><button class="btn btn-success btn-lg">Register</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('/js/back-end.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
    });

</script>
</body>
</html>