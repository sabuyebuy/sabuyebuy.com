app = angular.module('DmeetApp',['ngRoute','pascalprecht.translate']);


app.controller('HomeController', function($scope) {
    $scope.controllerName = 'HomeController';
});

app.controller('BookingController', function($scope) {

  $scope.rooms = [
              {
                    "id": "1",
                    "room_id": "room01",
                    "room_name": "Roome01",

                },
              {
                    "id": "2",
                    "room_id": "room02",
                    "room_name": "Roome02",

                },
              {
                    "id": "3",
                    "room_id": "room03",
                    "room_name": "Roome03",

                },
              {
                    "id": "4",
                    "room_id": "room04",
                    "room_name": "Roome04",

                },
              ];

  $scope.controllerName = 'BookingController';


});


app.config(['$routeProvider', '$translateProvider', '$locationProvider',
  function($routeProvider, $translateProvider, $locationProvider) {

    $routeProvider
    .when('/',{
      templateUrl: 'views/home.html',
             controller: 'HomeController'
    })
    .when('/booking',{
      templateUrl: 'views/booking/create.html',
       controller: 'BookingController'
    })
    .otherwise({
      redirectTo: '/'
    });

    $translateProvider
  .translations('th', {
    'HELLO': 'สวัสดี'
  })
  .translations('en', {
    'HELLO': 'Hello'
  })
  .preferredLanguage('ar')

}]);


app.directive('validFile',function(){
  return {
    require:'ngModel',
    link:function(scope,el,attrs,ngModel){
      el.bind('change',function(){
        scope.$apply(function(){
          ngModel.$setViewValue(el.val());
          ngModel.$render();
        });
      });
    }
  }
});
