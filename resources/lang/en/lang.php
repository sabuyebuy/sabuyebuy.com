<?php
    /**
     * Created by PhpStorm.
     * User: kokarat
     * Date: 11/22/2015 AD
     * Time: 23:12
     */
    
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Session;
    
    $txt = DB::select("SELECT * FROM `languages`");
    
    $lang = Session::get ('lang');
    //Log::info($lang);
    
    if($lang == 'th'){
        
        $data = [];
        foreach ($txt as $key => $valueTH){
            $data[$valueTH->key] =  $valueTH->value_th;
        }
        //Log::info($data);
        return $data;
    }elseif ($lang == 'en'){
        
        $data = [];
        foreach ($txt as $key => $valueEN){
            $data[$valueEN->key] =  $valueEN->value_en;
        }
        //Log::info($data);
        return $data;
        
    }else{
        
        $data = [];
        foreach ($txt as $key => $valueJP){
            $data[$valueJP->key] =  $valueJP->value_cn;
        }
        //Log::info($data);
        return $data;
        
    }